# Ports Libraries

RS232 and RS485 communication and protocols. 
* [Runtime Pre-Requisites](#Runtime-Pre-Requisites)
* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Runtime-Pre-Requisites"></a>
## Runtime Pre-Requisites

### .Net Framework 4.7.2
[Microsoft /.NET Framework] must installed before proceeding with this installation.

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [Enums Libraries] - .NET Standard libraries
* [Json Libraries] - .NET Standard libraries
* [Logging Libraries] - .NET Standard libraries
* [Std Libraries] - .NET Standard libraries
* [Tracing Libraries] - .NET Standard libraries
* [Units Amounts] - Units and amounts Libraries
* [IDE Repository] - IDE support files.
* [Ports Libraries] - Ports Libraries.

```
git clone git@bitbucket.org:davidhary/dn.enums.git
git clone git@bitbucket.org:davidhary/dn.json.git
git clone git@bitbucket.org:davidhary/dn.logging.git
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.tracing.git
git clone git@bitbucket.org:davidhary/Arebis.UnitsAmounts
git clone git@bitbucket.org:davidhary/vs.ide.git
git clone git@bitbucket.org:davidhary/dn.ports.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%dnlib%\core\enums
%dnlib%\core\json
%dnlib%\core\logging
%dnlib%\core\std
%dnlib%\core\tracing
%dnlib%\core\units.amounts
%vslib%\core\ide
%dnlib%\io\ports
```

where %dnlib% and %vslib% are  the root folders of the .NET libraries, e.g., %my%\lib\vs 
and %my%\libraries\vs, respectively, and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

#### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Funduc Search and Replace]
* [Serial Port Sample in VB.NET] -- Serial Port Sample in VB.NET
* [Serial Port Windows Forms Application] -- Serial Port Windows Forms Application
* [Extended Serial Port Windows Forms Sample] -- Extended Serial Port Windows Forms Sample

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix] -- we are but a spec on the shoulders of giants  
* [John Simmons] - outlaw programmer  
* [Stack overflow] - Joel Spolsky  
* [.Net Foundation] - The .NET Foundation

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Std Libraries]  
[Units Amounts]  
[Serial Port Sample in VB.NET C\#]
[Serial Port Windows Forms Application II]
[Extended Serial Port Windows Forms Sample]
[Circular Collection]

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Std Libraries]

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Enums Libraries]: https://bitbucket.org/davidhary/vs.enums
[Json Libraries]: https://bitbucket.org/davidhary/vs.json
[Logging Libraries]: https://bitbucket.org/davidhary/vs.logging
[Std Libraries]: https://bitbucket.org/davidhary/vs.std
[Tracing Libraries]: https://bitbucket.org/davidhary/vs.tracing
[Units Amounts]: https://bitbucket.org/davidhary/Arebis.UnitsAmounts
[Ports Libraries]: https://www.bitbucket.org/davidhary/dn.ports

[Microsoft /.NET Framework]: https://dotnet.microsoft.com/download
[Serial Port Sample in VB.NET]: https://code.MSDN.microsoft.com/SerialPort-Sample-in-VBNET-fb040fb2
[Serial Port Windows Forms Application]: https://code.MSDN.microsoft.com/SerialPort-Windows-Forms-a43f208e
[Extended Serial Port Windows Forms Sample]: https://code.MSDN.microsoft.com/Extended-SerialPort-10107e37
[Circular Collection]: http://www.INAV.NET

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[WiX Repository]: https://www.bitbucket.org/davidhary/vs.wix

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
[.Net Foundation]: https://source.dot.net



