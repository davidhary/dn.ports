using System;
using System.Windows.Forms;

namespace isr.Ports.Serial.Console.UI
{

    /// <summary>   Form for viewing the serial port user control. </summary>
    /// <remarks>   David, 2021-07-17. </remarks>
    public partial class SerialPortForm : Form
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-07-16. </remarks>
        public SerialPortForm()
        {
            this.InitializeComponent();
            this.Size = new System.Drawing.Size( 850, 600 );
            this.Icon = isr.Ports.Serial.Console.Properties.Resources.favicon;
        }

        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );
            _ = isr.Logging.TraceLog.TraceLogger.LogInformation( "Serial Port Form loaded" );
        }

        protected override void OnShown( EventArgs e )
        {
            base.OnShown( e );
            _ = isr.Logging.TraceLog.TraceLogger.LogInformation( "Serial Port Form shown" );
        }

    }
}
