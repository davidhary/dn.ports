
namespace isr.Ports.Serial.Console.UI
{
    partial class SerialPortForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing )
            {
                components?.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SerialPortControl = new SerialPortControl();
            this.SuspendLayout();
            // 
            // SerialPortControl
            // 
            this.SerialPortControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SerialPortControl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SerialPortControl.Location = new System.Drawing.Point(0, 0);
            this.SerialPortControl.Name = "SerialPortControl";
            this.SerialPortControl.Size = new System.Drawing.Size(596, 459);
            this.SerialPortControl.TabIndex = 0;
            // 
            // SerialPortForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 459);
            this.Controls.Add(this.SerialPortControl);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "SerialPortForm";
            this.Text = "Serial Port Interactive IO";
            this.ResumeLayout(false);

        }

        #endregion

        private SerialPortControl SerialPortControl;
    }
}

