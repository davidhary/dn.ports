using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Ports.Serial.Console.UI
{
    /// <summary>   A Serial Port control. </summary>
    /// <remarks>   David, 2021-07-26. </remarks>
    public partial class SerialPortControl : UserControl
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-07-26. </remarks>
        public SerialPortControl()
        {
            this.InitializeComponent();
        }

        private isr.Logging.TraceLog.WinForms.MessagesBox _MessagesBox;

        private isr.Tracing.WinForms.TextBoxTraceEventWriter TextBoxTextWriter { get; set; }

        /// <summary>   Event handler. Called by SerialPortControl for load events. </summary>
        /// <remarks>   David, 2021-07-26. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SerialPortControl_Load( object sender, EventArgs e )
        {
            this.TreePanel.Title = "Serial Port";
            Properties.Settings settings = new();
            isr.Ports.Serial.Forms.Properties.Settings serialConsoleSettings = new();

            settings.ReadSettings();
            _ = this.TreePanel.AddNode( "SerialConsole", "Console", new isr.Ports.Serial.Forms.PortConsole() );
            _ = this.TreePanel.AddNode( "ConsoleSettings", "Console Settings", new isr.Json.JsonSettingsEditorControl() { Settings = serialConsoleSettings } );
            _ = this.TreePanel.AddNode( "Settings", "Settings", new isr.Json.JsonSettingsEditorControl() { Settings = settings } );
            this.Size = new System.Drawing.Size( 850, 600 );
            _ = this.TreePanel.SplitterDistance = 150;

            this._MessagesBox = new();
            TreeNode messagesNode = this.TreePanel.AddNode( "Message Box", "Log", this._MessagesBox );

            this.TextBoxTextWriter = new( this._MessagesBox );
            this.TextBoxTextWriter.ContainerTreeNode = messagesNode;
            this.TextBoxTextWriter.HandleCreatedCheckEnabled = false;
            this.TextBoxTextWriter.TabCaption = "Log";
            this.TextBoxTextWriter.CaptionFormat = "{0} " + Convert.ToChar( 0x1C2 );
            this.TextBoxTextWriter.ResetCount = 1000;
            this.TextBoxTextWriter.PresetCount = 500;
            this.TextBoxTextWriter.TraceLevel =  Properties.Settings.Instance.MessageDisplayLevel;
            isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.TextBoxTextWriter );

            _= isr.Logging.TraceLog.TraceLogger.LogCallerMessage( Microsoft.Extensions.Logging.LogLevel.Information,
                                                               "Serial Port Console loaded",
                                                               Properties.Settings.Instance.AssemblyLogLevel );

        }
    }
}
