# About

isr.Ports.Teleport.Forms is a .Net library providing Windows Controls for the teleport communication protocol.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Ports.Teleport.Forms is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Ports Repository].

[Ports Repository]: https://bitbucket.org/davidhary/dn.ports

