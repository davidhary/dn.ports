using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

using isr.Ports.Serial;

namespace isr.Ports.Teleport
{

    /// <summary>
    /// Defines the interface for the <see cref="MessengerBase">Teleport Messenger</see>
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public interface IMessenger : IDisposable, INotifyPropertyChanged, IMessageParser
    {

        #region " CONNECTION MANAGEMENT "

        /// <summary> Gets or sets a value indicating whether the port is open. </summary>
        /// <value> The is port open. </value>
        bool IsPortOpen { get; }

        #endregion

        #region " COMMUNICATION "

        /// <summary> Sends byte data. </summary>
        /// <param name="data"> The Byte data. </param>
        void SendData( IEnumerable<byte> data );

        /// <summary> Sends the specified message and returns a reply. </summary>
        /// <param name="hexMessage">     The hex message. </param>
        /// <param name="turnaroundTime"> The time it takes from the receipt of the command to when the
        /// module starts to transmit a response. </param>
        /// <param name="readTimeout">    The read timeout. </param>
        /// <returns> An ITransport. </returns>
        ITransport Query( string hexMessage, TimeSpan turnaroundTime, TimeSpan readTimeout );

        /// <summary> Sends the specified data and returns a reply. </summary>
        /// <param name="data">           The data. </param>
        /// <param name="turnaroundTime"> The time it takes from the receipt of the command to when the
        /// module starts to transmit a response. </param>
        /// <param name="readTimeout">    The read timeout. </param>
        /// <returns> An ITransport. </returns>
        ITransport Query( IEnumerable<byte> data, TimeSpan turnaroundTime, TimeSpan readTimeout );

        /// <summary> Sends the specified message and returns a reply. </summary>
        /// <param name="message">     The protocol message. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <returns> An ITransport. </returns>
        ITransport Query( IProtocolMessage message, TimeSpan readTimeout );

        #endregion

        #region " TRANSMIT/RECEIVE LOG "

        /// <summary> Gets or sets a value indicating whether [log events]. </summary>
        /// <value> <c>true</c> if [log events]; otherwise, <c>false</c>. </value>
        bool IsLogEvents { get; set; }

        /// <summary> Gets or sets the name of the event log file. </summary>
        /// <value> The name of the event log file. </value>
        string EventLogFileName { get; set; }

        #endregion

        #region " DATA MANAGEMENT "

        /// <summary> Gets or sets the input data buffering option. </summary>
        /// <value> The data buffering option. </value>
        DataBufferingOption InputBufferingOption { get; set; }

        /// <summary> The received message. </summary>
        /// <value> A message describing the input. </value>
        IProtocolMessage InputMessage { get; set; }

        /// <summary> The sent message. </summary>
        /// <value> A message describing the output. </value>
        IProtocolMessage OutputMessage { get; set; }

        /// <summary> Gets or sets the <see cref="ITransport">Transport</see>. </summary>
        /// <value> The transport. </value>
        ITransport Transport { get; }

        #endregion

        #region " PORT MANAGEMENT "

        /// <summary> Gets or sets the port. </summary>
        /// <value> The port. </value>
        IPort Port { get; set; }

        /// <summary> Gets or sets the is port that owns this item. </summary>
        /// <value> The is port owner. </value>
        bool IsPortOwner { get; }

        /// <summary> Assigns a Port. </summary>
        /// <param name="value"> True to show or False to hide the control. </param>
        void AssignPort( IPort value );

        /// <summary>   Process the received message. </summary>
        /// <param name="receivedStatus">   The received status. </param>
        /// <param name="receivedData">     Information describing the received. </param>
        /// <param name="newData">          Information describing the new. </param>
        /// <returns>   A StatusCode. </returns>
        StatusCode ProcessReceivedMessage( Indicator receivedStatus, IEnumerable<byte> receivedData, IEnumerable<byte> newData );

        /// <summary> Process the sent message. </summary>
        /// <param name="sentStatus"> The sent status. </param>
        /// <param name="sentData">   Information describing the sent. </param>
        /// <returns> A StatusCode. </returns>
        StatusCode ProcessSentMessage( Indicator sentStatus, IEnumerable<byte> sentData );

        #endregion

        #region " MESSENGER MODULE MANAGEMENT "

        /// <summary> Gets or sets the module address for this messenger. </summary>
        /// <value> The module address. </value>
        IEnumerable<byte> ModuleAddress { get; set; }

        /// <summary> Services an error. </summary>
        /// <param name="status"> The status. </param>
        /// <returns> A StatusCode. </returns>
        StatusCode ServiceError( StatusCode status );

        /// <summary> Services the request. </summary>
        /// <remarks>
        /// This is the message handler routine for the RS485 interface which is a default communication
        /// channel for Teleport modules.
        /// </remarks>
        /// <returns> A StatusCode. </returns>
        StatusCode ServiceRequest();

        /// <summary>
        /// Gets the messenger role. The messenger could be a talker (emitter or controller),
        /// controlling a module or a listener (collector or receiver) emulating a module.
        /// </summary>
        /// <value> The messenger role. </value>
        MessengerRole MessengerRole { get; }

        #endregion

        #region " EVENT MANAGEMENT "

        /// <summary>
        /// Occurs when connection changed.
        /// Connection status is reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
        /// </summary>
        event EventHandler<ConnectionEventArgs> ConnectionChanged;

        /// <summary>
        /// Occurs when data is received.
        /// Receipt status is report along with the received data in the receive buffer using the <see cref="ReceiveDataEventArgs">port event arguments.</see>
        /// </summary>
        event EventHandler<ReceiveDataEventArgs> DataReceived;

        /// <summary>
        /// Occurs when data is Sent.
        /// Send status is reported along with the Sent data in the receive buffer using the <see cref="SentDataEventArgs">port event arguments.</see>
        /// </summary>
        event EventHandler<SentDataEventArgs> DataSent;

        /// <summary>
        /// Occurs when Message is received.
        /// The received message is included in the <see cref="ProtocolEventArgs">protocol event arguments.</see>
        /// </summary>
        event EventHandler<ProtocolEventArgs> MessageReceived;

        /// <summary>
        /// Occurs when Message is sent.
        /// The sent message is included in the <see cref="ProtocolEventArgs">protocol event arguments.</see>
        /// </summary>
        event EventHandler<ProtocolEventArgs> MessageSent;

        /// <summary>
        /// Occurs when Serial Port Error Received.
        /// SerialPortErrorReceived status is reported with the
        /// <see cref="System.IO.Ports.SerialErrorReceivedEventArgs">SerialPortErrorReceived event arguments.</see>
        /// </summary>
        event EventHandler<System.IO.Ports.SerialErrorReceivedEventArgs> SerialPortErrorReceived;

        /// <summary>
        /// Occurs when Serial Port Disposed.
        /// </summary>
        event EventHandler<EventArgs> SerialPortDisposed;

        /// <summary>
        /// Occurs when timeout.
        /// </summary>
        event EventHandler<ProtocolEventArgs> Timeout;

        /// <summary>   Event queue for all listeners interested in ExceptionEventHandler events. </summary>
        event System.Threading.ThreadExceptionEventHandler ExceptionEventHandler;

        #endregion

    }

    /// <summary> Values that represent messenger roles. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public enum MessengerRole
    {

        /// <summary> An enum constant representing the emitter (controller/talker) option. </summary>
        [Description( "Emitter" )]
        Emitter,

        /// <summary> An enum constant representing the collector (receiver/listener) option. </summary>
        [Description( "Collector" )]
        Collector
    }
}
