using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;

using isr.Ports.Serial;
using isr.Ports.Serial.PayloadExtensions;

namespace isr.Ports.Teleport
{

    /// <summary> The Scribe class writes and reads information to and from the module. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Scribe : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected Scribe() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="Scribe" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress">           The module address. </param>
        /// <param name="role">                    The role. </param>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        public Scribe( IEnumerable<byte> moduleAddress, MessengerRole role, IProtocolMessage templateProtocolMessage ) : this( role == MessengerRole.Emitter ? Emitter.Create( moduleAddress, templateProtocolMessage ) : Collector.Create( moduleAddress, templateProtocolMessage ) )
        {
            this.IsMessengerOwner = true;
        }

        /// <summary> Initializes a new instance of the <see cref="Scribe" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="messenger"> The Messenger. </param>
        public Scribe( IMessenger messenger ) : base()
        {
            this.AssignMessengerThis( messenger );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets the is disposed. </summary>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    if ( disposing )
                    {
                        this._Messenger?.Dispose();
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        ~Scribe()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " CONNECTION MANAGEMENT "

        /// <summary> Gets the is port open. </summary>
        /// <value> The is port open. </value>
        public virtual bool IsPortOpen => this.Messenger is object && this.Messenger.IsPortOpen;

        #endregion

        #region " MESSENGER MANAGEMENT "

        /// <summary> The messenger. </summary>
        private IMessenger _Messenger;

        /// <summary> Gets or sets the Messenger. </summary>
        /// <value> The Messenger. </value>
        public IMessenger Messenger
        {
            get => this._Messenger;

            set => this.AssignMessengerThis( value );
        }

        /// <summary> Assigns the Messenger. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignMessengerThis( IMessenger value )
        {
            if ( this._Messenger is object )
            {
                this._Messenger.ConnectionChanged -= this.MessengerConnectionChanged;
                this._Messenger.DataReceived -= this.MessengerDataReceived;
                this._Messenger.DataSent -= this.MessengerDataSent;
                this._Messenger.MessageReceived -= this.MessengerMessageReceived;
                this._Messenger.MessageSent -= this.MessengerMessageSent;
                this._Messenger.SerialPortDisposed -= this.MessengerSerialPortDisposed;
                this._Messenger.SerialPortErrorReceived -= this.MessengerSerialPortErrorReceived;
                this._Messenger.Timeout -= this.MessengerTimeout;
                // this also closes the session. 

                if ( value is null && this.IsMessengerOwner )
                    this._Messenger.Dispose();
            }

            this._Messenger = value;
            if ( this._Messenger is object )
            {
                this._Messenger.ConnectionChanged += this.MessengerConnectionChanged;
                this._Messenger.DataReceived += this.MessengerDataReceived;
                this._Messenger.DataSent += this.MessengerDataSent;
                this._Messenger.MessageReceived += this.MessengerMessageReceived;
                this._Messenger.MessageSent += this.MessengerMessageSent;
                this._Messenger.SerialPortDisposed += this.MessengerSerialPortDisposed;
                this._Messenger.SerialPortErrorReceived += this.MessengerSerialPortErrorReceived;
                this._Messenger.Timeout += this.MessengerTimeout;
            }
        }

        /// <summary> Gets the is Messenger that owns this item. </summary>
        /// <value> The is Messenger owner. </value>
        public bool IsMessengerOwner { get; private set; }

        /// <summary> Assigns a Messenger. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public virtual void AssignMessenger( IMessenger value )
        {
            this.IsMessengerOwner = false;
            this.Messenger = value;
        }

        /// <summary> Releases the Messenger. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected virtual void ReleaseMessenger()
        {
            this._Messenger = null;
        }

        #endregion

        #region " TRANSPORT "

        /// <summary> Gets the <see cref="Transport">Transport</see>. </summary>
        /// <value> The transport. </value>
        public ITransport Transport => this.Messenger.Transport;

        /// <summary> Sends an hexadecimal message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hexMessage"> The message hex characters. </param>
        public void SendMessage( string hexMessage )
        {
            if ( string.IsNullOrEmpty( hexMessage ) )
            {
                this.TryNotifyDataSent( new SentDataEventArgs( SendDataStatus.None ) );
            }
            else
            {
                this.SendMessage( hexMessage.ToHexBytes() );
            }
        }

        /// <summary> Sends the data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="message"> The message. </param>
        public void SendMessage( IEnumerable<byte> message )
        {
            if ( message is null )
            {
                this.TryNotifyDataSent( new SentDataEventArgs( SendDataStatus.None ) );
            }
            else
            {
                this.Messenger.SendData( message );
            }
        }

        #endregion

        #region " TRANSMIT/RECEIVE LOG "

        /// <summary> Gets or sets a value indicating whether [log events]. </summary>
        /// <value> <c>true</c> if [log events]; otherwise, <c>false</c>. </value>
        public bool IsLogEvents
        {
            get => this._Messenger.IsLogEvents;

            set => this._Messenger.IsLogEvents = value;
        }

        /// <summary> Gets or sets the name of the event log file. </summary>
        /// <value> The name of the event log file. </value>
        public string EventLogFileName
        {
            get => this._Messenger.EventLogFileName;

            set => this._Messenger.EventLogFileName = value;
        }

        #endregion

        #region " QUERY COMMANDS: GENERIC "

        /// <summary> Issues a command. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="message">     The message. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <returns> A Tuple( bool Success, String Details, ITransport Transport ) 
        ///           ( <c>true</c> if it succeeds; otherwise <c>false</c> , failure details, The <see cref="ITransport"/> ) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, String Details, ITransport Transport) TryQuery( IProtocolMessage message, TimeSpan readTimeout )
        {
            if ( message is null )
                throw new ArgumentNullException( nameof( message ) );
            string activity = "issuing command";
            ITransport t = null;
            try
            {
                activity = $"issuing {message.CommandAscii}";
                t = this.Query( message, readTimeout );
                return t.IsSuccess() ? (true, string.Empty, t) : (false, $"failed {activity}: {t.FailureMessage()}", t);
            }
            catch ( Exception ex )
            {
                return (false, $"Exception {activity};. {ex}", t);
            }

        }

        /// <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="hexMessage">     The hex message. </param>
        /// <param name="turnaroundTime"> The turnaround time. </param>
        /// <param name="readTimeout">    The read timeout. </param>
        /// <returns> The <see cref="ITransport">transport</see> </returns>
        public ITransport Query( string hexMessage, TimeSpan turnaroundTime, TimeSpan readTimeout )
        {
            return hexMessage is null
                ? throw new ArgumentNullException( nameof( hexMessage ) )
                : this.Query( hexMessage.ToHexBytes(), turnaroundTime, readTimeout );
        }

        /// <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="data">           The data. </param>
        /// <param name="turnaroundTime"> The turnaround time. </param>
        /// <param name="readTimeout">    The read timeout. </param>
        /// <returns> The <see cref="ITransport">transport</see> </returns>
        public ITransport Query( IEnumerable<byte> data, TimeSpan turnaroundTime, TimeSpan readTimeout )
        {
            return data is null ? throw new ArgumentNullException( nameof( data ) ) : this.Messenger.Query( data, turnaroundTime, readTimeout );
        }

        /// <summary> Queries the module and returns the <see cref="ITransport">transport</see>. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="message">     The message. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <returns> The <see cref="ITransport">transport</see> </returns>
        public ITransport Query( IProtocolMessage message, TimeSpan readTimeout )
        {
            return this.Messenger.Query( message, readTimeout );
        }

        #endregion

        #region " RETRY COMMANDS "

        /// <summary> Gets or sets the retry count. </summary>
        /// <value> The retry count. </value>
        public int RetryCount { get; set; }

        /// <summary> Echo command. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="message">     The message. </param>
        /// <param name="repeatCount"> Number of repeats. </param>
        /// <param name="pauseTime">   The pause time. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <returns> A Tuple( bool Success, String Details, ITransport Transport ) 
        ///           ( <c>true</c> if it succeeds; otherwise <c>false</c> , failure details, The <see cref="ITransport"/> ) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, String Details, ITransport Transport) EchoCommand( IProtocolMessage message, int repeatCount, TimeSpan pauseTime, TimeSpan readTimeout )
        {
            if ( message is null )
                throw new ArgumentNullException( nameof( message ) );
            string activity = "Echoing command";
            ITransport t = null;
            (bool Success, String Details, ITransport Transport) result;
            try
            {
                this.RetryCount = 0;
                if ( !this.IsPortOpen )
                {
                    activity = $"Open port {this.Messenger.Port.PortParameters.PortName}";
                    _ = this.Messenger.Port.TryOpen();
                }

                if ( this.IsPortOpen )
                {
                    int countDown = repeatCount;
                    do
                    {
                        countDown -= 1;
                        activity = "applying address mode";
                        activity = $"querying {message.ModuleAddress} w/ {message.CommandAscii}";
                        t = this.Messenger.Query( message, readTimeout );
                        result = t.IsSuccess() ? (true, string.Empty, t) : (false, $"failed {this.RetryCount} {activity}: {t.FailureMessage()}", t);

                        if ( !result.Success )
                        {
                            this.RetryCount += 1;
                            _ = Serial.Port.Wait( pauseTime, TimeSpan.Zero, Serial.Port.DoEventsAction );
                        }
                    }
                    while ( countDown != 0 && !result.Success );
                    if ( result.Success )
                    {
                        activity = $"comparing TX:{t.SentHexMessage} to RX:{t.ReceivedHexMessage} of {message.ModuleAddress} w/ {message.CommandAscii}";
                        if ( !t.SentHexMessage.Equals( t.ReceivedHexMessage ) )
                        {
                            result = (false, $"failed {activity}", t);
                        }
                    }
                }
                else
                {
                    result = (false, $"failed {activity}", t);
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception {activity};. {ex}", t);
            }

            return result;
        }

        /// <summary> Queries until success or count down. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="message">     The message. </param>
        /// <param name="repeatCount"> Number of repeats. </param>
        /// <param name="pauseTime">   The pause time. </param>
        /// <param name="readTimeout"> The read timeout. </param>
        /// <returns> A Tuple( bool Success, String Details, ITransport Transport ) 
        ///           ( <c>true</c> if it succeeds; otherwise <c>false</c> , failure details, The <see cref="ITransport"/> ) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, String Details, ITransport Transport) Query( IProtocolMessage message, int repeatCount, TimeSpan pauseTime, TimeSpan readTimeout )
        {
            if ( message is null )
                throw new ArgumentNullException( nameof( message ) );
            string activity = "Issuing command";
            ITransport t = null;
            (bool Success, String Details, ITransport Transport) result;
            try
            {
                this.RetryCount = 0;
                if ( !this.IsPortOpen )
                {
                    activity = $"Opening port {this.Messenger.Port.PortParameters.PortName}";
                    _ = this.Messenger.Port.TryOpen();
                }

                if ( this.IsPortOpen )
                {
                    int countDown = repeatCount;
                    do
                    {
                        countDown -= 1;
                        activity = "applying address mode";
                        activity = $"querying {message.ModuleAddress} w/ {message.CommandAscii}";
                        t = this.Messenger.Query( message, readTimeout );
                        result = t.IsSuccess() ? (true, string.Empty, t) : (false, $"failed {this.RetryCount} {activity}: {t.FailureMessage()}", t);

                        if ( !result.Success )
                        {
                            this.RetryCount += 1;
                            _ = Serial.Port.Wait( pauseTime, TimeSpan.Zero, Serial.Port.DoEventsAction );
                        }
                    }
                    while ( countDown != 0 && !result.Success );
                }
                else
                {
                    result = (false, $"failed {activity}", t);
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception {activity};. {ex}", t);
            }

            return result;
        }

        #endregion

        #region " EVENT: CONNECTION CHANGED "

        /// <summary>
        /// Event queue for all listeners interested in Connection Changed events. Connection status is
        /// reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
        /// </summary>
        public event EventHandler<ConnectionEventArgs> ConnectionChanged;

        /// <summary>   Raises the connection changed event. </summary>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnConnectionChanged( object sender, ConnectionEventArgs e )
        {
            string activity = $"notifying port {(e.IsPortOpen ? "open" : "closed")}";
            _ = TraceMethods.TraceInformation( activity );
            ConnectionChanged?.Invoke( sender, e );
        }


        /// <summary>   Raises the connection changed event. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void NotifyConnectionChanged()
        {
            this.OnConnectionChanged( this, new ConnectionEventArgs( this.IsPortOpen ) );
        }

        /// <summary> Raises the connection event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void NotifyConnectionChanged( ConnectionEventArgs e )
        {
            this.OnConnectionChanged( this, e );
        }

        /// <summary> Raises the connection event within a try catch clause. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifyConnectionChanged( ConnectionEventArgs e )
        {
            string activity = "notifying connection changed";
            try
            {
                this.NotifyConnectionChanged( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Messenger connection changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Connection event information. </param>
        private void MessengerConnectionChanged( object sender, ConnectionEventArgs e )
        {
            this.TryNotifyConnectionChanged( e );
        }


        #endregion

        #region " EVENT: DATA RECEIVED "

        /// <summary>
        /// Event queue for all listeners interested in <see cref="DataReceived">data received</see>/&gt;
        /// events. Receipt status is reported along with the received data in the receive buffer using
        /// the <see cref="ReceiveDataEventArgs">receive data event arguments.</see>
        /// </summary>
        public event EventHandler<ReceiveDataEventArgs> DataReceived;

        /// <summary>   Raises the <see cref="DataReceived"/> event. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDataReceived( object sender, ReceiveDataEventArgs e )
        {
            this.DataReceived?.Invoke( sender, e );
        }

        /// <summary>   Notifies a <see cref="DataReceived"/> event. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        private void NotifyDataReceived( ReceiveDataEventArgs e )
        {
            this.OnDataReceived( this, e );
        }

        /// <summary> Try notify a <see cref="DataReceived"/> event. </summary>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyDataReceived( ReceiveDataEventArgs e )
        {
            string activity = "notifying data received";
            try
            {
                this.NotifyDataReceived( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Messenger data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Port event information. </param>
        private void MessengerDataReceived( object sender, ReceiveDataEventArgs e )
        {
            this.TryNotifyDataReceived( e );
        }

        #endregion

        #region " EVENT: DATA SENT "

        /// <summary> Event queue for all listeners interested in <see cref="DataSent">Data Sent events</see>.
        /// Sent status is reported along with the Sent data in the buffer
        /// using the <see cref="SentDataEventArgs">send data event arguments.</see> </summary>
        public event EventHandler<SentDataEventArgs> DataSent;

        /// <summary>   Raises the <see cref="DataSent"/> event. </summary>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDataSent( object sender, SentDataEventArgs e )
        {
            this.DataSent?.Invoke( sender, e );
        }

        /// <summary>   Notifies a <see cref="DataSent"/> event. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        private void NotifyDataSent( SentDataEventArgs e )
        {
            this.OnDataSent( this, e );
        }

        /// <summary> Try notify a <see cref="DataSent"/> event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyDataSent( SentDataEventArgs e )
        {
            string activity = "notifying data sent";
            try
            {
                this.NotifyDataSent( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>   Messenger data sent. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Send data event information. </param>
        private void MessengerDataSent( object sender, SentDataEventArgs e )
        {
            this.TryNotifyDataSent( e );
        }

        #endregion

        #region " EVENT: MESSAGE RECEIVED "

        /// <summary>
        /// Event queue for all listeners interested in <see cref="MessageReceived">data received</see>/&gt;
        /// events. Receipt status is reported along with the received data in the receive buffer using
        /// the <see cref="ProtocolEventArgs">receive data event arguments.</see>
        /// </summary>
        public event EventHandler<ProtocolEventArgs> MessageReceived;

        /// <summary>   Raises the <see cref="MessageReceived"/> event. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnMessageReceived( object sender, ProtocolEventArgs e )
        {
            this.MessageReceived?.Invoke( sender, e );
        }

        /// <summary>   Notifies a <see cref="MessageReceived"/> event. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void NotifyMessageReceived( ProtocolEventArgs e )
        {
            this.OnMessageReceived( this, e );
        }

        /// <summary> Try notify a <see cref="MessageReceived"/> event. </summary>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyMessageReceived( ProtocolEventArgs e )
        {
            string activity = "notifying message received";
            try
            {
                this.NotifyMessageReceived( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Messenger message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Protocol event information. </param>
        private void MessengerMessageReceived( object sender, ProtocolEventArgs e )
        {
            this.TryNotifyMessageReceived( e );
        }

        #endregion

        #region " EVENT: MESSAGE SENT "

        /// <summary>
        /// Event queue for all listeners interested in <see cref="MessageSent">data Sent</see>/&gt;
        /// events. Receipt status is reported along with the Sent data in the receive buffer using
        /// the <see cref="ProtocolEventArgs">receive data event arguments.</see>
        /// </summary>
        public event EventHandler<ProtocolEventArgs> MessageSent;

        /// <summary>   Raises the <see cref="MessageSent"/> event. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnMessageSent( object sender, ProtocolEventArgs e )
        {
            this.MessageSent?.Invoke( sender, e );
        }

        /// <summary>   Notifies a <see cref="MessageSent"/> event. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        private void NotifyMessageSent( ProtocolEventArgs e )
        {
            this.OnMessageSent( this, e );
        }

        /// <summary> Try notify a <see cref="MessageSent"/> event. </summary>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyMessageSent( ProtocolEventArgs e )
        {
            string activity = "notifying message sent";
            try
            {
                this.NotifyMessageSent( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Messenger message sent. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Protocol event information. </param>
        private void MessengerMessageSent( object sender, ProtocolEventArgs e )
        {
            this.TryNotifyMessageSent( e );
        }

        #endregion

        #region " EVENT: SERIAL PORT DISPOSED "

        /// <summary>
        /// Event queue for all listeners interested in the <see cref="SerialPortDisposed">Serial Port
        /// Disposed</see> events.
        /// </summary>
        public event EventHandler<EventArgs> SerialPortDisposed;

        /// <summary>   Raises the serial port disposed event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnSerialPortDisposed( object sender, EventArgs e )
        {
            this.SerialPortDisposed?.Invoke( sender, e );
        }

        /// <summary>
        /// Notifies the <see cref="SerialPortDisposed">Serial Port Disposed</see> event.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifySerialPortDisposed( EventArgs e )
        {
            this.OnSerialPortDisposed( this, e );
        }

        /// <summary>   Try notify serial port disposed. </summary>
        /// <remarks>   David, 2021-08-05. </remarks>
        /// <param name="e">    Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void TryNotifySerialPortDisposed( EventArgs e )
        {
            string activity = "notifying serial port disposed";
            try
            {
                _ = TraceMethods.TraceInformation( activity );
                this.NotifySerialPortDisposed( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>
        /// Handles the Disposed event of the _serialPort control. Propagates the events to the calling
        /// controls.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void MessengerSerialPortDisposed( object sender, EventArgs e )
        {
            this.TryNotifySerialPortDisposed( e );
        }

        #endregion

        #region " EVENT: SERIAL PORT ERROR RECEIVED "

        /// <summary> Event queue for all listeners interested in
        /// <see cref="SerialPortErrorReceived">Serial port error receivd </see>> events.
        /// Serial Port Error Received status is reported with the
        /// <see cref="SerialErrorReceivedEventArgs">Serial Port Error Received event arguments.</see>
        /// </summary>
        public event EventHandler<SerialErrorReceivedEventArgs> SerialPortErrorReceived;

        /// <summary>   Raises the serial error received event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnSerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            this.SerialPortErrorReceived?.Invoke( sender, e );
        }

        /// <summary> Raises the serial port error received event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void NotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            _ = TraceMethods.TraceWarning( $"Error {e.EventType} occurred in serial port" );
            this.OnSerialPortErrorReceived( this, e );
        }

        /// <summary> Try notify serial port error received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Serial error received event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void TryNotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            string activity = "notifying serial port error received";
            try
            {
                this.NotifySerialPortErrorReceived( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
            }
        }

        private void MessengerSerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            this.TryNotifySerialPortErrorReceived( e );
        }

        #endregion

        #region " EVENT: TIMEOUT "

        /// <summary>   Event queue for all listeners interested in <see cref="Timeout"/> events. </summary>
        public event EventHandler<ProtocolEventArgs> Timeout;

        /// <summary>   Raises the <see cref="Timeout"/> event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnTimeout( object sender, ProtocolEventArgs e )
        {
            if ( e is object && this.Transport is object )
                this.Transport.TransportStatus = StatusCode.ReceiveTimeout;
            this.Timeout?.Invoke( sender, e );
        }

        /// <summary> Notifies a <see cref="Timeout"/> event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifyTimeout( ProtocolEventArgs e )
        {
            this.OnTimeout( this, e );
        }

        /// <summary> Try notify a <see cref="Timeout"/> event. </summary>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyTimeout( ProtocolEventArgs e )
        {
            string activity = $"notifying port timeout";
            try
            {
                _ = TraceMethods.TraceInformation( activity );
                this.NotifyTimeout( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Talker timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Protocol event information. </param>
        private void MessengerTimeout( object sender, ProtocolEventArgs e )
        {
            this.TryNotifyTimeout( e );
        }

        #endregion

        #region " EVENT: EVENT HANDLER ERROR "

        /// <summary>   Event queue for all listeners interested in <see cref="System.Exception"/> events. </summary>
        public event System.Threading.ThreadExceptionEventHandler ExceptionEventHandler;

        /// <summary>
        /// Raises the  <see cref="System.Threading.ThreadExceptionEventHandler"/> event.
        /// </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnEventHandlerError( System.Threading.ThreadExceptionEventArgs e )
        {
            this.ExceptionEventHandler?.Invoke( this, e );
        }

        /// <summary>
        /// Raises the  <see cref="System.Threading.ThreadExceptionEventHandler"/> event.
        /// </summary>
        /// <param name="exception">    The exception. </param>
        protected virtual void OnEventHandlerError( System.Exception exception )
        {
            this.OnEventHandlerError( new System.Threading.ThreadExceptionEventArgs( exception ) );
        }

        #endregion

        #region " EVENT: PROPERTY CHANGED "

        /// <summary> Event queue for all listeners interested in property changed events. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Raises the property changed event . </summary>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            PropertyChanged?.Invoke( sender, e );
        }

        /// <summary>   Notifies a property changed. </summary>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

    }
}
