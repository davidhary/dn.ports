using System;
using System.Diagnostics;
using System.IO;

using FastEnums;

namespace isr.Ports.Teleport
{

    /// <summary>
    /// Includes <see cref="IProtocolMessage">protocol messages</see> that are sent and received from
    /// the module and their status values.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Transport : ITransport
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Transport" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="templateProtocolMessage"> Message describing the template protocol. </param>
        public Transport( IProtocolMessage templateProtocolMessage ) : base()
        {
            _ = ProtocolMessageBase.ValidateInstance( templateProtocolMessage );
            this.TransportProtocolMessage = ( IProtocolMessage ) templateProtocolMessage.Clone();
            this.SentProtocolMessage = ( IProtocolMessage ) templateProtocolMessage.Clone();
            this.ReceivedProtocolMessage = ( IProtocolMessage ) templateProtocolMessage.Clone();
            this.ClearThis();
        }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transportProtocolMessage"> A message describing the transport protocol. </param>
        /// <param name="sentProtocolMessage">      A message describing the sent protocol. </param>
        /// <param name="receivedProtocolMessage">  A message describing the received protocol. </param>
        public Transport( IProtocolMessage transportProtocolMessage, IProtocolMessage sentProtocolMessage, IProtocolMessage receivedProtocolMessage ) : base()
        {
            this.TransportProtocolMessage = transportProtocolMessage;
            this.SentProtocolMessage = sentProtocolMessage;
            this.ReceivedProtocolMessage = receivedProtocolMessage;
            this.ClearThis();
        }

        /// <summary> Initializes a new instance of the <see cref="Transport" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transport"> The transport message. </param>
        public Transport( ITransport transport ) : base()
        {
            this.CopyFromThis( transport );
        }

        /// <summary> Validated the given transport. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="transport"> The transport. </param>
        /// <returns> An ITransport. </returns>
        public static ITransport Validated( ITransport transport )
        {
            return transport is null ? throw new ArgumentNullException( nameof( transport ) ) : transport;
        }

        /// <summary> Clears the transport protocol message t his. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ClearTransportProtocolMessageTHis()
        {
            this.TransportProtocolMessage.Clear();
            this.TransportStatus = StatusCode.ValueNotSet;
        }

        /// <summary> Clears the sent protocol message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ClearSentProtocolMessageThis()
        {
            this.SentProtocolMessage.Clear();
            this.SendStatus = StatusCode.ValueNotSet;
        }

        /// <summary> Clears the received protocol message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ClearReceivedProtocolMessageThis()
        {
            this.ReceivedProtocolMessage.Clear();
            this.ReceiveStatus = StatusCode.ValueNotSet;
        }

        /// <summary> Clears this instance. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ClearThis()
        {
            this.ItemNumber = 0;
            this.ClearTransportProtocolMessageTHis();
            this.ClearSentProtocolMessageThis();
            this.ClearReceivedProtocolMessageThis();
        }

        /// <summary> Clears this instance. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Clear()
        {
            this.ClearThis();
        }

        #region " CLONE "

        /// <summary> Creates a new object that is a copy of the current instance. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A new object that is a copy of this instance. </returns>
        public object Clone()
        {
            return Clone( this );
        }

        /// <summary>
        /// Creates a new <see cref="Transport">transport</see> that is a copy of the specified instance.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A new object that is a copy of this instance. </returns>
        public static ITransport Clone( ITransport value )
        {
            return new Transport( value );
        }

        /// <summary> Copies the contents of an existing <see cref="ITransport" />. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transport"> The transport. </param>
        private void CopyFromThis( ITransport transport )
        {
            this.ItemNumber = transport.ItemNumber;
            this.TransportProtocolMessage = ( IProtocolMessage ) transport.TransportProtocolMessage.Clone();
            this.TransportStatus = transport.TransportStatus;
            this.SentProtocolMessage = ( IProtocolMessage ) transport.SentProtocolMessage.Clone();
            this.SendStatus = transport.SendStatus;
            this.ReceivedProtocolMessage = transport.ReceivedProtocolMessage;
            this.ReceiveStatus = transport.ReceiveStatus;
            this.Commands = new ProtocolCommandCollection( transport.Commands );
        }

        /// <summary> Copies the contents of an existing <see cref="ITransport" />. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="transport"> The transport. </param>
        public void CopyFrom( ITransport transport )
        {
            if ( transport is null )
                throw new ArgumentNullException( nameof( transport ) );
            this.CopyFromThis( transport );
        }

        #endregion

        #region " Disposable Support "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        public bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.TransportProtocolMessage?.Dispose();
                    this.SentProtocolMessage?.Dispose();
                    this.ReceivedProtocolMessage?.Dispose();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        ~Transport()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " COMMANDS "

        /// <summary> Populate the protocol commands. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="collection"> The collection. </param>
        public void PopulateCommands( ProtocolCommandCollection collection )
        {
            this.Commands = new ProtocolCommandCollection( collection );
        }

        /// <summary> Populate commands. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="type"> The type. </param>
        public void PopulateCommands( Type type )
        {
            this.Commands = new ProtocolCommandCollection( type );
        }

        /// <summary> Gets the commands. </summary>
        /// <value> The commands. </value>
        public ProtocolCommandCollection Commands { get; private set; }

        /// <summary> Gets the command ASCII. </summary>
        /// <value> The command ASCII. </value>
        public string CommandAscii => this.TransportProtocolMessage.CommandAscii;

        /// <summary> Gets the command description. </summary>
        /// <value> The command description. </value>
        public string CommandDescription => this.TransportMessageCommand.Description;

        /// <summary>
        /// Gets the sentinel indicating if the protocol message is a known transport message command.
        /// </summary>
        /// <value>
        /// The sentinel indicating if the protocol message is known transport message command.
        /// </value>
        public bool IsKnownTransportMessageCommand => this.Commands.CommandDictionary.ContainsKey( this.CommandAscii );

        /// <summary> Select command. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command Ascii. </param>
        /// <returns> A ProtocolCommand. </returns>
        public ProtocolCommand SelectCommand( string commandAscii )
        {
            return this.Commands.Command( commandAscii );
        }

        /// <summary>
        /// Gets the transport message command.  This could be the command HEX characters.
        /// </summary>
        /// <value> The name of the transport message command. </value>
        public ProtocolCommand TransportMessageCommand => this.Commands.Command( this.CommandAscii );

        #endregion

        #region " MEMBERS "

        /// <summary> Gets the item number. </summary>
        /// <value> The item number. </value>
        public int ItemNumber { get; set; }

        /// <summary>
        /// Gets the Transport status. This status includes timeout status as well as status informing of
        /// mismatch between the received and sent messages.
        /// </summary>
        /// <value> The Transport status. </value>
        public StatusCode TransportStatus { get; set; }

        /// <summary> Gets the <see cref="IProtocolMessage">Transport Protocol Message.</see> </summary>
        /// <value> A message describing the transport protocol. </value>
        public IProtocolMessage TransportProtocolMessage { get; set; }

        /// <summary>
        /// Gets the <see cref="IProtocolMessage">Protocol Message</see> that was sent.
        /// </summary>
        /// <value> A message describing the sent protocol. </value>
        public IProtocolMessage SentProtocolMessage { get; set; }

        /// <summary> Gets the status of the sent message. </summary>
        /// <value> The Send status. </value>
        public StatusCode SendStatus { get; set; }

        /// <summary>
        /// Gets the <see cref="IProtocolMessage">Protocol Message</see> that was Received.
        /// </summary>
        /// <value> A message describing the received protocol. </value>
        public IProtocolMessage ReceivedProtocolMessage { get; set; }

        /// <summary> Gets the receive status. </summary>
        /// <value> The receive status. </value>
        public StatusCode ReceiveStatus { get; set; }

        /// <summary> Clears the send/receive status. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ClearSendReceiveStatus()
        {
            this.ReceiveStatus = StatusCode.ValueNotSet;
            this.SendStatus = StatusCode.ValueNotSet;
            this.TransportStatus = StatusCode.ValueNotSet;
            this.TransportProtocolMessage.Status = StatusCode.ValueNotSet;
            this.SentProtocolMessage.Status = StatusCode.ValueNotSet;
            this.ReceivedProtocolMessage.Status = StatusCode.ValueNotSet;
        }

        /// <summary>
        /// Determines whether transport was a success. For success, all status values must have a value
        /// of Okay.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> <c>true</c> if this instance is success; otherwise, <c>false</c>. </returns>
        public bool IsSuccess()
        {
            return this.TransportStatus == StatusCode.Okay && this.SendStatus == StatusCode.Okay && this.ReceiveStatus == StatusCode.Okay;
        }

        /// <summary> The failure message format. </summary>
        private const string _FailureMessageFormat = "{0} failure occurred with status {1}:{2}.";

        /// <summary> Interprets all status values to return a proper message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A String. </returns>
        public string FailureMessage()
        {
            return this.IsSuccess()
                ? StatusCode.Okay.Description()
                : this.ReceiveStatus == StatusCode.ValueNotSet
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat, "Transport", ( int ) this.TransportStatus, this.TransportStatus.Description() )
                    : this.ReceiveStatus != StatusCode.Okay
                                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat, "Receive", ( int ) this.ReceiveStatus, this.ReceiveStatus.Description() )
                                    : this.SendStatus == StatusCode.ValueNotSet
                                                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat, "Transport", ( int ) this.TransportStatus, this.TransportStatus.Description() )
                                                    : this.SendStatus != StatusCode.Okay
                                                                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat, "Send", ( int ) this.SendStatus, this.SendStatus.Description() )
                                                                    : this.TransportStatus != StatusCode.Okay
                                                                                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, _FailureMessageFormat, "Transport", ( int ) this.TransportStatus, this.TransportStatus.Description() )
                                                                                    : "No status was set";
        }

        #endregion

        #region " LOG "

        /// <summary> Gets the transport hex message. This is the message that needs to be sent. </summary>
        /// <value> The transport message. </value>
        public string TransportHexMessage => this.TransportProtocolMessage is null ? string.Empty : this.TransportProtocolMessage.HexMessage;

        /// <summary>
        /// Returns the status code obtained after parsing the transport protocol message.
        /// </summary>
        /// <value> The transport message status. </value>
        public StatusCode TransportMessageStatus => this.TransportProtocolMessage is null ? StatusCode.ValueNotSet : this.TransportProtocolMessage.Status;

        /// <summary>
        /// Gets the hex message that was sent. This is the message that needs to be sent.
        /// </summary>
        /// <value> The Sent message. </value>
        public string SentHexMessage => this.SentProtocolMessage is null ? string.Empty : this.SentProtocolMessage.HexMessage;

        /// <summary> Returns the status code obtained after parsing the Sent protocol message. </summary>
        /// <value> The sent message status. </value>
        public StatusCode SentMessageStatus => this.SentProtocolMessage is null ? StatusCode.ValueNotSet : this.SentProtocolMessage.Status;

        /// <summary>
        /// Returns the status code obtained after parsing the Received protocol message.
        /// </summary>
        /// <value> The received message status. </value>
        public StatusCode ReceivedMessageStatus => this.ReceivedProtocolMessage is null ? StatusCode.ValueNotSet : this.ReceivedProtocolMessage.Status;

        /// <summary>
        /// Gets the hex message that was Received. This is the message that needs to be Received.
        /// </summary>
        /// <value> The Received message. </value>
        public string ReceivedHexMessage => this.ReceivedProtocolMessage is null ? string.Empty : this.ReceivedProtocolMessage.HexMessage;

        /// <summary> Builds the log header. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A String. </returns>
        public string BuildLogHeader()
        {
            string delimeter = ",";
            var logEntery = new System.Text.StringBuilder();
            _ = logEntery.Append( nameof( this.ItemNumber ) );
            _ = logEntery.Append( delimeter );
            _ = logEntery.Append( nameof( this.TransportStatus ) );
            _ = logEntery.Append( delimeter );
            _ = logEntery.Append( nameof( this.TransportHexMessage ) );
            _ = logEntery.Append( delimeter );
            _ = logEntery.Append( nameof( this.SentHexMessage ) );
            _ = logEntery.Append( delimeter );
            _ = logEntery.Append( nameof( this.SendStatus ) );
            _ = logEntery.Append( delimeter );
            _ = logEntery.Append( nameof( this.ReceivedHexMessage ) );
            _ = logEntery.Append( delimeter );
            _ = logEntery.Append( nameof( this.ReceiveStatus ) );
            return logEntery.ToString();
        }

        /// <summary> The log delimeter. </summary>
        private const char _LogDelimeter = ',';

        /// <summary> Builds the log line. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A String. </returns>
        public string BuildLogline()
        {
            var logEntery = new System.Text.StringBuilder();
            _ = logEntery.Append( this.ItemNumber.ToString() );
            _ = logEntery.Append( _LogDelimeter );
            _ = this.TransportStatus == StatusCode.ValueNotSet
                ? logEntery.Append( "" )
                : logEntery.Append( (( int ) this.TransportStatus).ToString() );

            _ = logEntery.Append( _LogDelimeter );
            _ = string.IsNullOrEmpty( this.TransportHexMessage ) ? logEntery.Append( "" ) : logEntery.Append( this.TransportHexMessage );

            _ = logEntery.Append( _LogDelimeter );
            _ = string.IsNullOrEmpty( this.SentHexMessage ) ? logEntery.Append( "" ) : logEntery.Append( this.SentHexMessage );

            _ = logEntery.Append( _LogDelimeter );
            _ = this.SendStatus == StatusCode.ValueNotSet ? logEntery.Append( "" ) : logEntery.Append( (( int ) this.SendStatus).ToString() );

            _ = logEntery.Append( _LogDelimeter );
            _ = string.IsNullOrEmpty( this.ReceivedHexMessage ) ? logEntery.Append( "" ) : logEntery.Append( this.ReceivedHexMessage );

            _ = logEntery.Append( _LogDelimeter );
            _ = this.ReceiveStatus == StatusCode.ValueNotSet
                ? logEntery.Append( "" )
                : logEntery.Append( (( int ) this.ReceiveStatus).ToString() );

            return logEntery.ToString();
        }

        /// <summary> Parses the log line. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool ParseLogline( string value )
        {
            if ( string.IsNullOrEmpty( value ) )
                return false;
            this.Clear();
            var values = value.Split( _LogDelimeter );
            if ( values.Length < 7 )
            {
                return false;
            }

            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
            {
                if ( string.IsNullOrEmpty( values[i] ) )
                {
                    values[i] = string.Empty;
                }

                switch ( i )
                {
                    case 0:
                        {
                            bool localTryParse()
                            {
                                _ = this.ItemNumber;
                                var ret = int.TryParse( values[i], out int argresult ); this.ItemNumber = argresult; return ret;
                            }

                            if ( !localTryParse() )
                            {
                                return false;
                            }

                            break;
                        }

                    case 1:
                        {
                            if ( !string.IsNullOrEmpty( values[i] ) )
                            {
                                bool localTryParse1()
                                {
                                    _ = this.TransportStatus;
                                    var ret = Enum.TryParse( values[i], out StatusCode argresult ); this.TransportStatus = argresult; return ret;
                                }

                                if ( !localTryParse1() )
                                {
                                    return false;
                                }
                            }

                            break;
                        }

                    case 2:
                        {
                            _ = this.TransportProtocolMessage.ParseHexMessage( values[i] );
                            break;
                        }

                    case 3:
                        {
                            _ = this.SentProtocolMessage.ParseHexMessage( values[i] );
                            break;
                        }

                    case 4:
                        {
                            if ( !string.IsNullOrEmpty( values[i] ) )
                            {
                                bool localTryParse2()
                                {
                                    _ = this.SendStatus;
                                    var ret = Enum.TryParse( values[i], out StatusCode argresult ); this.SendStatus = argresult; return ret;
                                }

                                if ( !localTryParse2() )
                                {
                                    return false;
                                }
                            }

                            break;
                        }

                    case 5:
                        {
                            _ = this.ReceivedProtocolMessage.ParseHexMessage( values[i] );
                            break;
                        }

                    case 6:
                        {
                            if ( !string.IsNullOrEmpty( values[i] ) )
                            {
                                bool localTryParse3()
                                {
                                    _ = this.ReceiveStatus;
                                    var ret = Enum.TryParse( values[i], out StatusCode argresult ); this.ReceiveStatus = argresult; return ret;
                                }

                                if ( !localTryParse3() )
                                {
                                    return false;
                                }
                            }

                            break;
                        }
                }
            }

            return true;
        }

        /// <summary> The delimiter. </summary>
        public const string Delimiter = " ";

        #endregion

    }

    /// <summary> Implements a collection of <see cref="Transport">Transport</see> entities. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public class TransportCollection : System.Collections.ObjectModel.Collection<ITransport>
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets or sets the transport template. </summary>
        /// <value> The transport template. </value>
        private ITransport TransportTemplate { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TransportCollection" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transport"> The transport. </param>
        public TransportCollection( ITransport transport ) : base()
        {
            this.TransportTemplate = Transport.Clone( transport );
        }

        /// <summary> Adds the specified transport message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transportMessage"> The transport message. </param>
        public void Add( string transportMessage )
        {
            if ( !string.IsNullOrEmpty( transportMessage ) )
            {
                var ci = Transport.Clone( this.TransportTemplate );
                _ = ci.TransportProtocolMessage.ParseHexMessage( transportMessage );
                ci.ItemNumber = this.Count;
                this.Add( ci );
            }
        }

        /// <summary> Adds the specified transport record. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transportRecord"> The transport record. </param>
        public void AddRecord( string transportRecord )
        {
            if ( !string.IsNullOrEmpty( transportRecord ) )
            {
                var ci = Transport.Clone( this.TransportTemplate );
                _ = ci.ParseLogline( transportRecord );
                this.Add( ci );
            }
        }

        /// <summary> Clears the send receive status. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ClearSendReceiveStatus()
        {
            foreach ( Transport ti in this )
                ti.ClearSendReceiveStatus();
        }

        /// <summary> Updates the transport info. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="transportInfo"> The Transport Info. </param>
        public void UpdateTransportInfo( ITransport transportInfo )
        {
            if ( transportInfo is null )
            {
                Debug.Assert( false, "Transport info is nothing" );
            }
            else if ( this.Count == 0 )
            {
            }
            // ignore if we have no configuration loaded.
            else if ( this.Count > transportInfo.ItemNumber && transportInfo.ItemNumber >= 0 )
            {
                var transport = this[transportInfo.ItemNumber];
                transport.ReceiveStatus = transportInfo.ReceiveStatus;
                transport.SendStatus = transportInfo.SendStatus;
                transport.TransportStatus = transportInfo.TransportStatus;
            }
            // transport.SentHexMessage = transportInfo.SentHexMessage
            // transport.ReceivedHexMessage = transportInfo.ReceivedHexMessage
            else
            {
                Debug.Assert( false, "Incorrect item number ", "Incorrect item number ", ( object ) transportInfo.ItemNumber );
            }
        }

        #endregion

        #region " FILE MANAGER "

        /// <summary> Reads all items from the specified file name. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="fileName"> Name of the file. </param>
        public void Read( string fileName )
        {
            using var sr = new StreamReader( fileName );
            while ( !sr.EndOfStream )
                this.Add( sr.ReadLine() );
        }

        /// <summary> Reads all items from the log. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="fileName"> Name of the file. </param>
        public void ReadLog( string fileName )
        {
            this.Clear();
            using var sr = new StreamReader( fileName );
            _ = sr.ReadLine();
            while ( !sr.EndOfStream )
                this.AddRecord( sr.ReadLine() );
        }

        /// <summary> Save all items to the log. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="fileName"> Name of the file. </param>
        public void SaveLog( string fileName )
        {
            using var sw = new StreamWriter( fileName );
            bool wroteHeader = false;
            foreach ( Transport item in this )
            {
                if ( !wroteHeader )
                {
                    sw.WriteLine( item.BuildLogHeader() );
                    wroteHeader = true;
                }

                sw.WriteLine( item.BuildLogline() );
            }
        }

        #endregion

    }
}
