using System;

namespace isr.Ports.Teleport
{

    /// <summary> Interface defining the <see cref="T:ITransport">transport</see>class. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public interface ITransport : ICloneable, IDisposable
    {

        #region " COMMANDS "

        /// <summary> Populate the protocol commands. </summary>
        /// <param name="collection"> The collection. </param>
        void PopulateCommands( ProtocolCommandCollection collection );

        /// <summary> Populate the protocol commands. </summary>
        /// <param name="type"> The type. </param>
        void PopulateCommands( Type type );

        /// <summary> Gets or sets the commands. </summary>
        /// <value> The commands. </value>
        ProtocolCommandCollection Commands { get; }

        /// <summary>
        /// Gets or sets the sentinel indicating if the protocol message is a known transport message
        /// command.
        /// </summary>
        /// <value>
        /// The sentinel indicating if the protocol message is known transport message command.
        /// </value>
        bool IsKnownTransportMessageCommand { get; }

        /// <summary> Select command. </summary>
        /// <param name="commandAscii"> The command Ascii. </param>
        /// <returns> A ProtocolCommand. </returns>
        ProtocolCommand SelectCommand( string commandAscii );

        /// <summary> Gets or sets the transport message command. </summary>
        /// <value> The 'transport message' command. </value>
        ProtocolCommand TransportMessageCommand { get; }

        /// <summary> Gets or sets the command ASCII. </summary>
        /// <value> The command ASCII. </value>
        string CommandAscii { get; }

        #endregion

        #region " MEMBERS "

        /// <summary> Gets or sets the item number. </summary>
        /// <value> The item number. </value>
        int ItemNumber { get; set; }

        /// <summary>
        /// Gets or sets the Transport status. This status includes timeout status as well as status
        /// informing of mismatch between the received and sent messages.
        /// </summary>
        /// <value> The Transport status. </value>
        StatusCode TransportStatus { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="IProtocolMessage">Transport Protocol Message.</see>
        /// </summary>
        /// <value> A message describing the transport protocol. </value>
        IProtocolMessage TransportProtocolMessage { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="IProtocolMessage">Protocol Message</see> that was sent.
        /// </summary>
        /// <value> A message describing the sent protocol. </value>
        IProtocolMessage SentProtocolMessage { get; set; }

        /// <summary> Gets or sets the status of the sent message. </summary>
        /// <value> The Send status. </value>
        StatusCode SendStatus { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="IProtocolMessage">Protocol Message</see> that was Received.
        /// </summary>
        /// <value> A message describing the received protocol. </value>
        IProtocolMessage ReceivedProtocolMessage { get; set; }

        /// <summary> Gets or sets the receive status. </summary>
        /// <value> The receive status. </value>
        StatusCode ReceiveStatus { get; set; }

        /// <summary> Clears this instance. </summary>
        void Clear();

        /// <summary> Clears the send/receive status. </summary>
        void ClearSendReceiveStatus();

        /// <summary>
        /// Determines whether transport was a success. For success, all status values must have a value
        /// of Okay.
        /// </summary>
        /// <returns> <c>true</c> if this instance is success; otherwise, <c>false</c>. </returns>
        bool IsSuccess();

        /// <summary> Interprets all status values to return a proper message. </summary>
        /// <returns> A String. </returns>
        string FailureMessage();

        #endregion

        #region " CLONE "

        /// <summary> Copies the contents of an existing <see cref="ITransport" />. </summary>
        /// <param name="transport"> The transport. </param>
        void CopyFrom( ITransport transport );

        #endregion

        #region " LOG "

        /// <summary>
        /// Gets or sets the transport hex message. This is the message that needs to be sent.
        /// </summary>
        /// <value> The transport message. </value>
        string TransportHexMessage { get; }

        /// <summary>
        /// Returns the status code obtained after parsing the transport protocol message.
        /// </summary>
        /// <value> The transport message status. </value>
        StatusCode TransportMessageStatus { get; }

        /// <summary>
        /// Gets or sets the hex message that was sent. This is the message that needs to be sent.
        /// </summary>
        /// <value> The Sent message. </value>
        string SentHexMessage { get; }

        /// <summary> Returns the status code obtained after parsing the Sent protocol message. </summary>
        /// <value> The sent message status. </value>
        StatusCode SentMessageStatus { get; }

        /// <summary>
        /// Gets or sets the hex message that was Received. This is the message that needs to be Received.
        /// </summary>
        /// <value> The Received message. </value>
        string ReceivedHexMessage { get; }

        /// <summary>
        /// Returns the status code obtained after parsing the Received protocol message.
        /// </summary>
        /// <value> The received message status. </value>
        StatusCode ReceivedMessageStatus { get; }

        /// <summary> Builds the log header. </summary>
        /// <returns> A String. </returns>
        string BuildLogHeader();

        /// <summary> Builds the log line. </summary>
        /// <returns> A String. </returns>
        string BuildLogline();

        /// <summary> Parses the log line. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        bool ParseLogline( string value );

        #endregion

    }
}
