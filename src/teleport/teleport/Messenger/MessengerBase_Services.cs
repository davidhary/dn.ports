using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.Ports.Serial;

namespace isr.Ports.Teleport
{
    public partial class MessengerBase
    {

        #region " RECEIVE BUFFER MANAGER "

        /// <summary> The stream initiation value. </summary>
        private byte _StreamInitiationValue;

        /// <summary> Gets or sets the stream Initiation value. </summary>
        /// <value> The stream Initiation value. </value>
        public byte StreamInitiationValue
        {
            get => this._StreamInitiationValue;

            set {
                if ( value != this.StreamInitiationValue )
                {
                    this._StreamInitiationValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The stream termination value. </summary>
        private byte _StreamTerminationValue;

        /// <summary> Gets or sets the stream termination value. </summary>
        /// <value> The stream termination value. </value>
        public byte StreamTerminationValue
        {
            get => this._StreamTerminationValue;

            set {
                if ( value != this.StreamTerminationValue )
                {
                    this._StreamTerminationValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The read pointer. </summary>
        private int _ReadPointer;

        /// <summary> Buffer for received data. </summary>
        private List<byte> _ReceivedBuffer;

        /// <summary> Reads the next byte. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The next. </returns>
        private byte ReadNext()
        {
            if ( this.InputBufferingOption == Serial.DataBufferingOption.CircularBuffer )
            {
                return this._Port.ReadNext();
            }
            else if ( this.HasData() )
            {
                this._ReadPointer += 1;
                return this._ReceivedBuffer[this._ReadPointer];
            }
            else
            {
                return 255;
            }
        }

        /// <summary> Returns the number of bytes left to read. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> An Integer. </returns>
        private int RemainingDataCount()
        {
            return this.InputBufferingOption == Serial.DataBufferingOption.CircularBuffer ? this._Port.DataCount() : this._ReceivedBuffer is null ? 0 : this._ReceivedBuffer.Count - this._ReadPointer;
        }

        /// <summary> Determines whether this instance has data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> <c>true</c> if this instance has data; otherwise, <c>false</c>. </returns>
        private bool HasData()
        {
            return this.RemainingDataCount() > 0;
        }

        /// <summary> Encapsulates the result of a read. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private struct ReadResult
        {

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="readResult"> The read result. </param>
            public ReadResult( ReadResult readResult ) : this( readResult.Reading, readResult.Timeout )
            {
                this.Reading = readResult.Reading;
                this.Timeout = readResult.Timeout;
            }

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="reading"> The reading. </param>
            /// <param name="timeout"> The timeout. </param>
            public ReadResult( byte? reading, bool timeout )
            {
                this.Reading = reading;
                this.Timeout = timeout;
            }

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="timeout"> The timeout. </param>
            public ReadResult( bool timeout )
            {
                this.Reading = new byte?();
                this.Timeout = timeout;
            }

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="reading"> The reading. </param>
            public ReadResult( byte reading )
            {
                this.Reading = reading;
                this.Timeout = false;
            }

            /// <summary> Gets or sets the reading. </summary>
            /// <value> The reading. </value>
            public byte? Reading { get; set; }

            /// <summary> Gets or sets the timeout. </summary>
            /// <value> The timeout. </value>
            public bool Timeout { get; set; }

            /// <summary> Stream started. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="streamInitiationValue"> The stream initiation value. </param>
            /// <returns> True if it succeeds, false if it fails. </returns>
            public bool StreamStarted( byte streamInitiationValue )
            {
                return this.Reading.HasValue && (streamInitiationValue == 0 || this.Reading.Value == streamInitiationValue);
            }

            /// <summary> Stream ended. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="streamTerminationValue"> The stream termination value. </param>
            /// <returns> True if it succeeds, false if it fails. </returns>
            public bool StreamEnded( byte streamTerminationValue )
            {
                return this.Reading.HasValue && this.Reading.Value == streamTerminationValue;
            }
        }

        /// <summary>   Reads the next byte. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="loopDelay">    The loop delay. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <param name="timeout">      The timeout. </param>
        /// <returns>   The next. </returns>
        private ReadResult ReadNext( TimeSpan loopDelay, TimeSpan pollInterval, TimeSpan timeout )
        {
            Stopwatch sw = Stopwatch.StartNew();
            var result = new ReadResult( false );
            do
            {
                if ( this.HasData() )
                {
                    result = new ReadResult( this.ReadNext() );
                }
                else if ( sw.Elapsed > timeout )
                {
                    result = new ReadResult( true );
                }
                else
                {
                    // wait a bit for the next character
                    _ = Serial.Port.Wait( loopDelay, pollInterval, Serial.Port.DoEventsAction );
                }
            }
            while ( !result.Reading.HasValue && !result.Timeout );
            return result;
        }


        #endregion

        #region " RECEIVE SERVICES: - BYTE-BY-BYTE READ AND SERVICE "

        /// <summary> Await start stream. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        private StatusCode AwaitStartStream()
        {
            var result = StatusCode.Okay;

            // loop waiting for data
            var delay = TimeSpan.FromMilliseconds( 0.75 );

            // set timeout for message turn around time. 
            TimeSpan turnaroundTime = this.Transport.SelectCommand( this.InputMessage.CommandAscii ).TurnaroundTime;
            var readResult = new ReadResult( this.ReadNext( delay, TimeSpan.Zero, turnaroundTime ) );
            if ( readResult.StreamEnded( this.StreamTerminationValue ) || readResult.Timeout || !readResult.Reading.HasValue )
            {
                result = StatusCode.MessageIncomplete;
            }
            else if ( readResult.StreamStarted( this.StreamInitiationValue ) )
            {
                this.InputMessage.StreamValues.Add( readResult.Reading.Value );
            }
            else
            {
                result = StatusCode.InvalidStreamStart;
            }

            return result;
        }

        /// <summary> Builds input message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        private StatusCode BuildInputMessage()
        {

            // clear the existing message.
            this.InputMessage.Clear();
            int startCountDown = 10;
            StatusCode result;
            do
            {
                startCountDown -= 1;
                result = this.AwaitStartStream();
            }
            while ( result != StatusCode.Okay && startCountDown != 0 );
            var delay = TimeSpan.FromMilliseconds( 0.75 );
            var timeout = TimeSpan.FromMilliseconds( 10 );
            var loopDelay = TimeSpan.Zero;
            var readResult = new ReadResult( this.ReadNext( delay, loopDelay, timeout ) );
            while ( !readResult.StreamEnded( this.StreamTerminationValue ) && !readResult.Timeout )
            {
                readResult = new ReadResult( this.ReadNext( delay, loopDelay, timeout ) );
                if ( readResult.Reading.HasValue )
                    this.InputMessage.StreamValues.Add( readResult.Reading.Value );
            }
            result = readResult.Timeout ? StatusCode.ReceiveTimeout : this.InputMessage.ParseStream();
            return result;
        }

        /// <summary> Receives and services the protocol message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        protected StatusCode ReceiveProtocolMessage()
        {
            StatusCode result;

            // read and parse the data from the interface
            result = this.BuildInputMessage();

            // validate status of message
            if ( IsErrorCode( result ) )
            {
                result = this.ServiceError( result );
            }
            else if ( result == StatusCode.Okay || result == StatusCode.ServiceMessageAvailable )
            {

                // Build and send back the reply.
                result = this.ServiceRequest();
            }

            // save last status code
            this.LastProcessStatus = result;

            // Return with the status code
            return result;
        }

        #endregion

        #region " RECEIVE SERVICES: - BUFFER RECEIPT SERVICE "


        #endregion

        #region " REPLY RESPONSE "

        /// <summary> Services the response. Responds to the received message after processing. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        protected StatusCode ServiceResponse()
        {
            _ = Serial.Port.Wait( this.OutputMessage.CommandTimeout, TimeSpan.Zero, Serial.Port.DoEventsAction );

            // transmit error status
            var sentStatusCode = this.SendProtocolMessage();


            // validate status of the message
            if ( IsErrorCode( sentStatusCode ) )
            {
                _ = TraceMethods.TraceWarning( $"Failed sending reply message '{sentStatusCode}'" );
            }
            else
            {
                sentStatusCode = StatusCode.Okay;
            }

            // return status
            return sentStatusCode;
        }

        #endregion

    }
}
