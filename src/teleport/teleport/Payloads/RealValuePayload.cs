using System;
using System.Collections.Generic;
using System.Linq;

using isr.Ports.Serial.PayloadExtensions;

namespace isr.Ports.Teleport
{

    /// <summary>
    /// A payload consisting of a single real value of <see cref="T:Double"/> type.
    /// </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public class RealValuePayload : IPayload
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public RealValuePayload() : this( Std.Primitives.RangeR.Full, new Std.Primitives.RangeR( -99999.99d, 99999.99d ) )
        {
        }

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="range">    The range. </param>
        /// <param name="overflow"> The positive overflow. </param>
        public RealValuePayload( Std.Primitives.RangeR range, Std.Primitives.RangeR overflow ) : base()
        {
            this.Range = new Std.Primitives.RangeR( range );
            this.Overflow = new Std.Primitives.RangeR( overflow );
            this.Format = "0.00";
            this.StatusCode = StatusCode.ValueNotSet;
            this.Unit = Arebis.StandardUnits.ElectricUnits.Volt;
        }

        #endregion

        #region " SIMULATE "

        /// <summary> Returns an emulated value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="range"> The range. </param>
        /// <returns> A Double. </returns>
        public static double SimulateValue( Std.Primitives.RangeR range )
        {
            if ( range is null )
                throw new ArgumentNullException( nameof( range ) );
            var rand = new Random( DateTime.Now.Second );
            return range.Min + range.Span * rand.NextDouble();
        }

        /// <summary> Gets the simulated value. </summary>
        /// <value> The simulated value. </value>
        public double SimulatedValue => SimulateValue( this.Range );

        /// <summary> Gets the simulated payload. </summary>
        /// <value> The simulated payload. </value>
        public IEnumerable<byte> SimulatedPayload => string.Format( this.Format, SimulateValue( this.Range ) ).ToBytes();

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the range. </summary>
        /// <value> The range. </value>
        public Std.Primitives.RangeR Range { get; set; }

        /// <summary> Gets or sets the overflow range. </summary>
        /// <value> The positive overflow. </value>
        public Std.Primitives.RangeR Overflow { get; set; }

        #endregion

        #region " VALUE "

        /// <summary> Gets or sets the real value unit. </summary>
        /// <value> The real value. </value>
        public Arebis.UnitsAmounts.Unit Unit { get; set; }

        /// <summary> Gets or sets the analog input read. </summary>
        /// <value> The analog input read. </value>
        public Arebis.UnitsAmounts.Amount Amount { get; set; }

        /// <summary> The real value. </summary>
        private double _RealValue;

        /// <summary> Gets or sets the value. </summary>
        /// <value> The real value. </value>
        public double RealValue
        {
            get => this._RealValue;

            set {
                if ( value != this.RealValue )
                {
                    this._RealValue = value;
                    this.Reading = string.Format( this.Format, value );
                    this.Amount = new Arebis.UnitsAmounts.Amount( value, this.Unit );
                }
            }
        }

        /// <summary> Gets the format to use. </summary>
        /// <value> The format. </value>
        public string Format { get; set; }

        /// <summary> Gets the has value. </summary>
        /// <value> The has value. </value>
        public bool HasValue => this.StatusCode == StatusCode.Okay;

        /// <summary> The reading. </summary>
        private string _Reading;

        /// <summary> Gets or sets the reading. </summary>
        /// <value> The reading. </value>
        public string Reading
        {
            get => this._Reading;

            set {
                if ( !string.Equals( value, this.Reading ) )
                {
                    this._Reading = value;
                    this.Payload = value.ToBytes();
                    bool localTryParse()
                    {
                        _ = this.RealValue;
                        var ret = double.TryParse( value, out double argresult ); this.RealValue = argresult; return ret;
                    }

                    this.StatusCode = localTryParse() ? StatusCode.Okay : StatusCode.PayloadParseError;
                }
            }
        }

        #endregion

        #region " I PAYLOAD IMPLEMENTATION "

        /// <summary> Gets or sets a list of payloads. </summary>
        /// <value> A list of payloads. </value>
        private List<byte> PayloadList { get; set; }

        /// <summary> Gets or sets the payload. </summary>
        /// <value> The payload. </value>
        public IEnumerable<byte> Payload
        {
            get => this.PayloadList;

            set {
                this.PayloadList = new List<byte>( value );
                if ( value?.Any() == true )
                {
                    var encoding = new System.Text.ASCIIEncoding();
                    this.Reading = encoding.GetString( value.ToArray() );
                }
                else
                {
                    this.Reading = string.Empty;
                }
            }
        }

        /// <summary> Gets or sets the status code. </summary>
        /// <value> The status code. </value>
        public StatusCode StatusCode { get; set; }

        /// <summary> Populates the payload from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="payload"> The payload. </param>
        /// <returns> A StatusCode. </returns>
        public StatusCode Parse( IEnumerable<byte> payload )
        {
            this.Payload = payload;
            return this.StatusCode;
        }

        /// <summary> Converts the payload to bytes. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> This object as an IEnumerable(Of Byte) </returns>
        public IEnumerable<byte> Build()
        {
            return this.Payload;
        }

        #endregion

    }
}
