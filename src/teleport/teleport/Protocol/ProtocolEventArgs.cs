using System;

namespace isr.Ports.Teleport
{

    /// <summary> Defines an event arguments class for protocol messages. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class ProtocolEventArgs : EventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolEventArgs" />
        /// using an empty <see cref="Teleport.IProtocolMessage">serial protocol message</see>
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ProtocolEventArgs() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="ProtocolEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="status">          The <see cref="StatusCode">status</see>. </param>
        /// <param name="protocolMessage"> The <see cref="IProtocolMessage">protocol message</see>. </param>
        public ProtocolEventArgs( StatusCode status, IProtocolMessage protocolMessage ) : base()
        {
            this.TransportStatus = status;
            this.ProtocolMessage = protocolMessage;
        }

        #endregion

        #region " MEMBERS "

        /// <summary>
        /// Gets or sets the <see cref="StatusCode">transport status</see>. This status is different from
        /// the status of the sent or received message.
        /// </summary>
        /// <value> The transport status. </value>
        public StatusCode TransportStatus { get; private set; }

        /// <summary> Gets or sets the <see cref="IProtocolMessage">protocol message</see>. </summary>
        /// <value> A message describing the protocol. </value>
        public IProtocolMessage ProtocolMessage { get; private set; }

        #endregion

    }
}
