using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using isr.Ports.Serial;
using isr.Ports.Serial.PayloadExtensions;

namespace isr.Ports.Teleport
{

    /// <summary> Defines the protocol message. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public abstract class ProtocolMessageBase : IProtocolMessage
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="encoding"> The encoding. </param>
        protected ProtocolMessageBase( System.Text.Encoding encoding ) : base()
        {
            this.Encoding = encoding;
            this.Initialize();
        }

        /// <summary> Initializes this object. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void Initialize()
        {
            this._Prefix = 0;
            this._PrefixAscii = string.Empty;
            this.StreamInitiationValues = new List<byte>();
            this.StreamTerminationValues = new List<byte>();
            this.ModuleAddressValues = new List<byte>();
            this._CommandAscii = string.Empty;
            this.PayloadValues = new ByteCollection();
            this.ChecksumValues = new List<byte>();
            this._Status = StatusCode.ValueNotSet;
            this._MessageType = MessageType.None;
            this.StreamValues = new ByteCollection();
            this.InternalStreamValues = new ByteCollection();
            this.Encoding = new System.Text.ASCIIEncoding();
        }

        /// <summary> Clears the contents of an existing <see cref="ProtocolMessageBase" />. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public virtual void Clear()
        {
            this.Checksum = new List<byte>();
            this.CommandAscii = string.Empty;
            this.InternalStream = new List<byte>();
            this.Payload = new List<byte>();
            this.PrefixAscii = string.Empty;
            this.Stream = new List<byte>();
            this.Status = StatusCode.ValueNotSet;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolMessageBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="protocolMessageBase"> The message. </param>
        protected ProtocolMessageBase( IProtocolMessage protocolMessageBase ) : this( ValidateInstance( protocolMessageBase ).Encoding )
        {
            this.CopyFromThis( protocolMessageBase );
        }

        /// <summary> Copies the contents of an existing <see cref="IProtocolMessage" />. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="protocolMessage"> The message. </param>
        private void CopyFromThis( IProtocolMessage protocolMessage )
        {
            this._Prefix = protocolMessage.Prefix;
            this.StreamInitiationValues = new List<byte>( protocolMessage.StreamInitiation );
            this.StreamTerminationValues = new List<byte>( protocolMessage.StreamTermination );
            this.ModuleAddressValues = new List<byte>( protocolMessage.ModuleAddress );
            this._CommandAscii = protocolMessage.CommandAscii;
            this.PayloadValues = new ByteCollection( protocolMessage.Payload );
            this.ChecksumValues = new List<byte>( protocolMessage.Checksum );
            this._Status = protocolMessage.Status;
            this._MessageType = protocolMessage.MessageType;
            this.StreamValues = new ByteCollection( protocolMessage.Stream );
            this.InternalStreamValues = new ByteCollection( protocolMessage.InternalStream );
            this.Encoding = protocolMessage.Encoding;
        }

        /// <summary> Copies the contents of an existing <see cref="IProtocolMessage" />. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="protocolMessage"> The validated protocol message. </param>
        public virtual void CopyFrom( IProtocolMessage protocolMessage )
        {
            if ( protocolMessage is null )
                throw new ArgumentNullException( nameof( protocolMessage ) );
            this.CopyFromThis( protocolMessage );
        }

        /// <summary> Makes a deep copy of this object. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> A copy of this object. </returns>
        public abstract object Clone();

        /// <summary> Validated the given protocol message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="protocolMessage"> The message. </param>
        /// <returns> An IProtocolMessage. </returns>
        public static IProtocolMessage ValidateInstance( IProtocolMessage protocolMessage )
        {
            return protocolMessage is null ? throw new ArgumentNullException( nameof( protocolMessage ) ) : protocolMessage;
        }

        #region "IDISPOSABLE SUPPORT"

        /// <summary> Gets or sets the is disposed. </summary>
        /// <value> The is disposed. </value>
        private bool IsDisposed { get; set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.StreamInitiationValues?.Clear();
                    this.StreamValues?.Clear();
                }
            }
            catch
            {
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // removes this object from the finalization(Queue); uncomment 
            // the following line if Finalize() is overridden.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        ~ProtocolMessageBase()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " START / END TEXT "

        /// <summary> Gets or sets the stream initiation values. </summary>
        /// <value> The stream initiation values. </value>
        private List<byte> StreamInitiationValues { get; set; }

        /// <summary> Gets or sets the stream initiation byte(s). </summary>
        /// <remarks> Typically \x02, ^B. </remarks>
        /// <value> The stream initiation byte(s). </value>
        public virtual IEnumerable<byte> StreamInitiation
        {
            get => this.StreamInitiationValues;

            set {
                if ( !Serial.PayloadExtensions.PayloadExtensionsMethods.NullableSequenceEquals( this.StreamInitiation, value ) )
                {
                    this.StreamInitiationValues = new List<byte>( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the stream termination values. </summary>
        /// <value> The stream termination values. </value>
        private List<byte> StreamTerminationValues { get; set; }

        /// <summary> Gets or sets the stream termination bytes(s). </summary>
        /// <remarks>
        /// Typically \x03 ^C or Acknowledge \x06 ^F (on the return message), or \x13 \r ^M or \x10 \n ^J
        /// or both \r \n.
        /// </remarks>
        /// <value> The stream termination bytes(s). </value>
        public virtual IEnumerable<byte> StreamTermination
        {
            get => this.StreamTerminationValues;

            set {
                if ( !Serial.PayloadExtensions.PayloadExtensionsMethods.NullableSequenceEquals( this.StreamTermination, value ) )
                {
                    this.StreamTerminationValues = new List<byte>( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " PREFIX "

        /// <summary> The prefix. </summary>
        private byte _Prefix;

        /// <summary> Gets or sets the command prefix. </summary>
        /// <value> The command prompt. </value>
        public virtual byte Prefix
        {
            get => this._Prefix;

            set {
                if ( this.Prefix != value )
                {
                    this._Prefix = value;
                    this.NotifyPropertyChanged();
                    if ( value != 0 )
                    {
                        // was Unicode Encoding
                        this._PrefixAscii = this.Encoding.GetString( new byte[] { this.Prefix } );
                    }
                    else
                    {
                        this._PrefixAscii = string.Empty;
                    }
                }
            }
        }

        /// <summary> The prefix ASCII. </summary>
        private string _PrefixAscii;

        /// <summary> Gets or sets the Prompt. </summary>
        /// <value> The Prompt. </value>
        public string PrefixAscii
        {
            get => this._PrefixAscii;

            set {
                if ( !string.Equals( this.PrefixAscii, value ) )
                {
                    this._PrefixAscii = value;
                    this.NotifyPropertyChanged();
                    this.Prefix = string.IsNullOrWhiteSpace( value ) ? ( byte ) 0 : this._PrefixAscii.ToByte();
                }
            }
        }

        #endregion

        #region " CONTENTS "

        /// <summary> The address mode. </summary>
        private AddressModes _AddressMode;

        /// <summary> Gets or sets the address mode. </summary>
        /// <value> The address mode. </value>
        public AddressModes AddressMode
        {
            get => this._AddressMode;

            set {
                if ( this._AddressMode != value )
                {
                    this._AddressMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the module address values. </summary>
        /// <value> The module address values. </value>
        private List<byte> ModuleAddressValues { get; set; }

        /// <summary> Gets or sets the module address. </summary>
        /// <value> The module address. </value>
        public IEnumerable<byte> ModuleAddress
        {
            get => this.ModuleAddressValues;

            set {
                if ( !Serial.PayloadExtensions.PayloadExtensionsMethods.NullableSequenceEquals( this.ModuleAddress, value ) )
                {
                    this.ModuleAddressValues = new List<byte>( value );
                    this.NotifyPropertyChanged();
                    if ( this.ModuleAddress.Any() )
                    {
                        // was Unicode Encoding
                        var encoding = new System.Text.ASCIIEncoding();
                        this._ModuleAddressAscii = encoding.GetString( this.ModuleAddress.ToArray() );
                    }
                    else
                    {
                        this._ModuleAddressAscii = string.Empty;
                    }
                }
            }
        }

        /// <summary> The module address ASCII. </summary>
        private string _ModuleAddressAscii;

        /// <summary> Gets or sets the ModuleAddress. </summary>
        /// <value> The ModuleAddress. </value>
        public string ModuleAddressAscii
        {
            get => this._ModuleAddressAscii;

            set {
                if ( !string.Equals( this.ModuleAddressAscii, value ) )
                {
                    this._ModuleAddressAscii = value;
                    this.NotifyPropertyChanged();
                    this.ModuleAddress = string.IsNullOrWhiteSpace( value ) ? new List<byte>() : ( IEnumerable<byte> ) this._ModuleAddressAscii.ToBytes();
                }
            }
        }

        /// <summary> Length of the total message. </summary>
        private int _TotalMessageLength;

        /// <summary>
        /// Gets or sets the length of the message including the address byte, checksum, command and
        /// payload.
        /// </summary>
        /// <value> The length of the message. </value>
        public int TotalMessageLength
        {
            get => this._TotalMessageLength;

            set {
                if ( this.TotalMessageLength != value )
                {
                    this._TotalMessageLength = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a message describing the error. </summary>
        /// <value> A message describing the error. </value>
        public string ErrorMessage => this.MessageType == MessageType.Failure ? this.Encoding.GetString( this.Payload.ToArray() ) : string.Empty;

        /// <summary> Type of the message. </summary>
        private MessageType _MessageType;

        /// <summary> Gets or sets the message type. </summary>
        /// <value> The message type. </value>
        public virtual MessageType MessageType
        {
            get => this._MessageType;

            set {
                if ( this.MessageType != value )
                {
                    this._MessageType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The command ASCII. </summary>
        private string _CommandAscii;

        /// <summary> Gets or sets the command. </summary>
        /// <value> The command. </value>
        public string CommandAscii
        {
            get => this._CommandAscii;

            set {
                if ( !string.Equals( this.CommandAscii, value ) )
                {
                    this._CommandAscii = value;
                    this.NotifyPropertyChanged();
                    this.Command = string.IsNullOrWhiteSpace( value ) ? new List<byte>() : ( IEnumerable<byte> ) this._CommandAscii.ToBytes();
                }
            }
        }

        /// <summary> Gets or sets the command values. </summary>
        /// <value> The command values. </value>
        private List<byte> CommandValues { get; set; }

        /// <summary> Gets or sets Command. </summary>
        /// <value> The command. </value>
        public IEnumerable<byte> Command
        {
            get => this.CommandValues;

            set {
                this.CommandValues = new List<byte>( value );
                this.NotifyPropertyChanged();
                this._CommandAscii = value?.Any() == true ? this.Encoding.GetString( this.CommandValues.ToArray() ) : string.Empty;
            }
        }

        /// <summary> The command timeout. </summary>
        private TimeSpan _CommandTimeout;

        /// <summary> Gets or sets the command timeout. </summary>
        /// <value> The command timeout. </value>
        public TimeSpan CommandTimeout
        {
            get => this._CommandTimeout;

            set {
                if ( this.CommandTimeout != value )
                {
                    this._CommandTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets a list of payloads. </summary>
        /// <value> A List of payloads. </value>
        public ByteCollection PayloadValues { get; private set; }

        /// <summary> Notifies the payload changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void NotifyPayloadChanged()
        {
            this.NotifyPropertyChanged( nameof( this.Payload ) );
        }

        /// <summary> Gets or sets payload. </summary>
        /// <value> The payload. </value>
        public IEnumerable<byte> Payload
        {
            get => this.PayloadValues;

            set {
                if ( !Serial.PayloadExtensions.PayloadExtensionsMethods.NullableSequenceEquals( value, this.Payload ) )
                {
                    this.PayloadValues = new ByteCollection( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the payload in hex. </summary>
        /// <value> The payload hexadecimal. </value>
        public string PayloadHex => this.Payload?.Any() == true ? this.Payload.ToHex() : string.Empty;

        /// <summary> Gets or sets the checksum values. </summary>
        /// <value> The checksum values. </value>
        private List<byte> ChecksumValues { get; set; }

        /// <summary> Gets or sets the checksum. </summary>
        /// <value> The checksum. </value>
        public IEnumerable<byte> Checksum
        {
            get => this.ChecksumValues;

            set {
                if ( !Serial.PayloadExtensions.PayloadExtensionsMethods.NullableSequenceEquals( this.Checksum, value ) )
                {
                    this.ChecksumValues = new List<byte>( value );
                    this.NotifyPropertyChanged();
                    this.ChecksumHex = value.ToHex();
                }
            }
        }

        /// <summary> The checksum hexadecimal. </summary>
        private string _ChecksumHex;

        /// <summary> Gets or sets the Checksum in hex. </summary>
        /// <value> The checksum hexadecimal. </value>
        public string ChecksumHex
        {
            get => this._ChecksumHex;

            set {
                if ( !string.Equals( this.ChecksumHex, value ) )
                {
                    this._ChecksumHex = value;
                    this.NotifyPropertyChanged();
                    this.Checksum = string.IsNullOrWhiteSpace( value ) ? new List<byte>() : value.ToHexBytes();
                }
            }
        }

        /// <summary> Gets a value indicating whether this instance has status value. </summary>
        /// <value> The has status value. </value>
        public bool HasStatusValue => this.Status != StatusCode.ValueNotSet;

        /// <summary> The status. </summary>
        private StatusCode _Status;

        /// <summary> Gets or sets the status. </summary>
        /// <value> The status. </value>
        public StatusCode Status
        {
            get => this._Status;

            set {
                if ( this.Status != value )
                {
                    this._Status = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.HasStatusValue ) );
                }
            }
        }

        #endregion

        #region " STREAM "

        /// <summary> Gets or sets a stream of bytes. </summary>
        /// <value> A stream of bytes. </value>
        public ByteCollection StreamValues { get; private set; }

        /// <summary> Gets or sets the stream. </summary>
        /// <value> The stream. </value>
        public IEnumerable<byte> Stream
        {
            get => this.StreamValues;

            set {
                if ( !Serial.PayloadExtensions.PayloadExtensionsMethods.NullableSequenceEquals( value, this.Stream ) )
                {
                    this.StreamValues = new ByteCollection( value );
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.HexMessage ) );
                }
            }
        }

        /// <summary> Gets a message as hexadecimal. </summary>
        /// <value> A message describing the hexadecimal. </value>
        public string HexMessage => this.Stream.ToHex();

        /// <summary> Gets or sets the Internal Stream of bytes. </summary>
        /// <value> A InternalStream of bytes. </value>
        public ByteCollection InternalStreamValues { get; private set; }

        /// <summary> Gets or sets the InternalStream. </summary>
        /// <value> The InternalStream. </value>
        public IEnumerable<byte> InternalStream
        {
            get => this.InternalStreamValues;

            set {
                if ( !Serial.PayloadExtensions.PayloadExtensionsMethods.NullableSequenceEquals( value, this.InternalStreamValues ) )
                {
                    this.InternalStreamValues = new ByteCollection( value );
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.InternalAsciiMessage ) );
                }
            }
        }

        /// <summary> Gets the encoding. </summary>
        /// <value> The encoding. </value>
        public System.Text.Encoding Encoding { get; set; }

        /// <summary> Gets a message describing the internal ASCII. </summary>
        /// <value> A message describing the internal ASCII. </value>
        public string InternalAsciiMessage => this.Encoding.GetString( this.InternalStream.ToArray() );

        #endregion

        #region " I PARSER PARSER "

        /// <summary> True to enable, false to disable the parse. </summary>
        private bool _ParseEnabled;

        /// <summary> Gets or sets the parse enabled. </summary>
        /// <value> The parse enabled. </value>
        public bool ParseEnabled
        {
            get => this._ParseEnabled;

            set {
                if ( this.ParseEnabled != value )
                {
                    this._ParseEnabled = true;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Parses the given values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> A Serial.MessageParserOutcome. </returns>
        public abstract MessageParserOutcome Parse( IEnumerable<byte> values );

        #endregion

        #region " PARSERS "

        /// <summary> Determines whether the message status signifies an incomplete message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns>
        /// <c>true</c> if the specified status is incomplete; otherwise, <c>false</c>.
        /// </returns>
        public bool IsIncomplete()
        {
            return IsIncomplete( this.Status );
        }

        /// <summary> Determines whether the specified status signifies an incomplete message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns>
        /// <c>true</c> if the specified status is incomplete; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsIncomplete( StatusCode status )
        {
            return status == StatusCode.MessageTooShort;
        }

        /// <summary> Determines whether the message status signifies an invalid message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> <c>true</c> if the message is invalid; otherwise, <c>false</c>. </returns>
        public bool IsInvalid()
        {
            return IsInvalid( this.Status );
        }

        /// <summary> Determines whether the specified status signifies an invalid message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns>
        /// <c>true</c> if [is invalid message] [the specified status]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInvalid( StatusCode status )
        {
            return status == StatusCode.InvalidModuleAddress || status == StatusCode.InvalidMessageLength || status == StatusCode.UnknownStatusCode || status == StatusCode.InvalidCommandCode || status == StatusCode.ChecksumInvalid;
        }

        /// <summary> Parse hexadecimal message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="hexMessage"> A message describing the hexadecimal. </param>
        /// <returns> A StatusCode. </returns>
        public abstract StatusCode ParseHexMessage( string hexMessage );

        /// <summary>
        /// Parses the <paramref name="data">byte data</paramref> into a protocol message.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="messageType"> The message type. </param>
        /// <param name="data">        The byte data. </param>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        public abstract StatusCode ParseStream( MessageType messageType, IEnumerable<byte> data );

        /// <summary>
        /// Parses the <paramref name="data">byte data</paramref> into a protocol message.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="messengerRole"> The messenger role. </param>
        /// <param name="data">          The byte data. </param>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        public StatusCode ParseStream( MessengerRole messengerRole, IEnumerable<byte> data )
        {
            return this.ParseStream( this.ToMessageType( messengerRole ), data );
        }

        /// <summary>
        /// Parses the <paramref name="data">byte data</paramref> into a protocol message.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data"> The byte data. </param>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        public StatusCode ParseStream( IEnumerable<byte> data )
        {
            return this.ParseStream( this.MessageType, data );
        }

        /// <summary> Parses the stream into a protocol message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns>
        /// <see cref="StatusCode.Okay">okay if parsed</see>; otherwise a failure code.
        /// </returns>
        public StatusCode ParseStream()
        {
            return this.ParseStream( this.MessageType, this.Stream );
        }

        /// <summary> Validates the protocol message relative to this protocol message. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="protocolMessage"> The validated protocol message. </param>
        /// <returns> An Integer. </returns>
        public abstract StatusCode Validate( IProtocolMessage protocolMessage );

        #endregion

        #region " BUILDERS "

        /// <summary> Calculates the checksum hexadecimal. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The calculated checksum hexadecimal. </returns>
        public abstract string CalculateChecksumHex();

        /// <summary> Calculates the checksum. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The calculated checksum. </returns>
        public IEnumerable<byte> CalculateChecksum()
        {
            return this.CalculateChecksumHex().ToBytes();
        }

        /// <summary> Calculates and sets the checksum value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void UpdateChecksum()
        {
            this.Checksum = this.CalculateChecksum();
        }

        /// <summary> Enumerates build message in this collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="messageType"> The message type. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        public abstract IEnumerable<byte> BuildStream( MessageType messageType );

        /// <summary> Enumerates build message in this collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="messengerRole"> The messenger role. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process build message in this collection.
        /// </returns>
        public IEnumerable<byte> BuildStream( MessengerRole messengerRole )
        {
            return this.BuildStream( this.ToMessageType( messengerRole ) );
        }

        /// <summary> Converts a value to a message type. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a MessageType. </returns>
        public MessageType ToMessageType( MessengerRole value )
        {
            return value == MessengerRole.Collector ? MessageType.Response : MessageType.Command;
        }

        #endregion

        #region " EVENT: PROPERTY CHANGED "

        /// <summary> Event queue for all listeners interested in property changed events. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Raises the property changed event . </summary>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            PropertyChanged?.Invoke( sender, e );
        }

        /// <summary>   Notifies a property changed. </summary>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

    }
}
