using System;
using System.Collections.Generic;
using System.Linq;

using FastEnums;

namespace isr.Ports.Teleport
{

    /// <summary> A protocol command. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-06-19 </para>
    /// </remarks>
    public class ProtocolCommand
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandCode">  The command code. </param>
        /// <param name="commandAscii"> The command. </param>
        /// <param name="description">  The description. </param>
        public ProtocolCommand( int commandCode, string commandAscii, string description ) : base()
        {
            this.CommandCode = commandCode;
            this.CommandAscii = commandAscii;
            this.Description = description;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandCode"> The command code. </param>
        public ProtocolCommand( Enum commandCode ) : this( Convert.ToInt32( commandCode ), commandCode.ExtractBetween(), commandCode.Description() )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="command"> The command. </param>
        public ProtocolCommand( ProtocolCommand command ) : base()
        {
            if ( command is object )
            {
                this.CommandAscii = command.CommandAscii;
                this.CommandCode = command.CommandCode;
                this.Description = command.Description;
                this.TurnaroundTime = command.TurnaroundTime;
            }
        }

        /// <summary> Gets or sets the command code. </summary>
        /// <value> The command code. </value>
        public int CommandCode { get; private set; }

        /// <summary> Gets or sets the command ASCII value. </summary>
        /// <value> The command. </value>
        public string CommandAscii { get; private set; }

        /// <summary> Gets or sets the description. </summary>
        /// <value> The description. </value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets or sets the time it takes from the receipt of the command to when the module starts to
        /// transmit a response.
        /// </summary>
        /// <value> The command timeout. </value>
        public virtual TimeSpan TurnaroundTime { get; set; }
    }

    /// <summary> Dictionary of Protocol commands. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-17 </para>
    /// </remarks>
    public class ProtocolCommandDictionary : Dictionary<string, int>
    {

#region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ProtocolCommandDictionary() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="collection"> The collection. </param>
        public ProtocolCommandDictionary( ProtocolCommandDictionary collection ) : base()
        {
            if ( collection is object )
            {
                foreach ( KeyValuePair<string, int> item in collection )
                    this.Add( item.Key, item.Value );
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="type"> The type. </param>
        public ProtocolCommandDictionary( Type type ) : base()
        {
            foreach ( Enum enumValue in Enum.GetValues( type ) )
                this.Add( enumValue.ExtractBetween(), enumValue.IntegerValues().First() );
        }

#endregion

    }

    /// <summary> Collection of Protocol commands. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-17 </para>
    /// </remarks>
    public class ProtocolCommandCollection : System.Collections.ObjectModel.KeyedCollection<int, ProtocolCommand>
    {

#region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ProtocolCommandCollection() : base()
        {
            this.CommandDictionary = new ProtocolCommandDictionary();
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="collection"> The collection. </param>
        public ProtocolCommandCollection( ProtocolCommandCollection collection ) : base()
        {
            if ( collection is object )
            {
                foreach ( ProtocolCommand item in collection )
                    this.Add( new ProtocolCommand( item ) );
                this.CommandDictionary = new ProtocolCommandDictionary( collection.CommandDictionary );
            }
        }

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="type"> The type. </param>
        public ProtocolCommandCollection( Type type ) : base()
        {
            foreach ( Enum enumValue in Enum.GetValues( type ) )
                this.Add( new ProtocolCommand( enumValue ) );
            this.CommandDictionary = new ProtocolCommandDictionary( type );
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns> The key for the specified element. </returns>
        protected override int GetKeyForItem( ProtocolCommand item )
        {
            return item is null ? throw new ArgumentNullException( nameof( item ) ) : item.CommandCode;
        }

        /// <summary> Gets or sets a dictionary of commands. </summary>
        /// <value> A Dictionary of commands. </value>
        public ProtocolCommandDictionary CommandDictionary { get; private set; }

        /// <summary> Contains command. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ContainsCommand( string commandAscii )
        {
            return this.CommandDictionary.ContainsKey( commandAscii );
        }

        /// <summary> Commands. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command ASCII. </param>
        /// <returns> A ProtocolCommand. </returns>
        public ProtocolCommand Command( string commandAscii )
        {
            return this[this.CommandDictionary[commandAscii]];
        }

#endregion

    }
}
