using System;

using isr.Std.Primitives;
using isr.Ports.Serial;
using isr.Ports.Serial.PayloadExtensions;
using isr.Ports.Teleport;

namespace isr.Ports.Love
{

    /// <summary> A base class for the Love Protocol modules. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-10 </para>
    /// </remarks>
    public abstract class LoveBase : Scribe
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="messenger"> The messenger. </param>
        private LoveBase( IMessenger messenger ) : base( messenger )
        {
            this.Messenger.ParseEnabled = false;
            this.Messenger.Transport.PopulateCommands( LoveCommandCollection.Get() );
            this.Messenger.OutputMessage = ProtocolMessage.Create();
            this.Messenger.InputMessage = ProtocolMessage.Create();
            this.Messenger.Transport.TransportProtocolMessage = ProtocolMessage.Create();
            this.Messenger.Transport.SentProtocolMessage = ProtocolMessage.Create();
            this.Messenger.Transport.ReceivedProtocolMessage = ProtocolMessage.Create();
            this.SetpointWindowTolerance = 0.01d;
            this._SetpointWindow = new RangeR( 0.2d );
            this._SetpointAccuracyRange = new RangeR( 0.1d );
            this._TemperatureAccuracyRange = new RangeR( 0.1d );
        }

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddressAscii"> The module address ASCII. </param>
        protected LoveBase( string moduleAddressAscii ) : this( Emitter.Create( moduleAddressAscii.ToBytes(), ProtocolMessage.Create() ) )
        {
            this.ModuleAddressAscii = moduleAddressAscii;
        }

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddressAscii"> The module address ASCII. </param>
        /// <param name="port">               The port. </param>
        protected LoveBase( string moduleAddressAscii, IPort port ) : this( Emitter.Create( moduleAddressAscii.ToBytes(), ProtocolMessage.Create(), port ) )
        {
            this.ModuleAddressAscii = moduleAddressAscii;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PRESETTABLE "

        /// <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
        /// <remarks> Use this to customize the reset. </remarks>
        public virtual void InitKnownState()
        {
        }

        /// <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
        /// <remarks> Use this to customize the reset. </remarks>
        /// <param name="moduleName"> The name of the module. </param>
        public void InitKnownState( string moduleName )
        {
            this.ModuleName = moduleName;
            _ = TraceMethods.TraceInformation( $"{this.ModuleName}.{this.Messenger.ModuleAddress} initializing known state" );
            // Me._AnalogInputPayload = New RealValuePayload() With {.Unit = Arebis.StandardUnits.ElectricUnits.Volt}
        }

        #endregion

        #region " PORT "

        /// <summary> Configure port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="port">     The port. </param>
        /// <param name="portName"> Name of the port. </param>
        /// <returns> A Tuple ( bool Success, string Details ) ( <c>true</c> if it succeeds; otherwise <c>false</c>, Details if failed)  </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details) TryOpenPort( IPort port, string portName )
        {
            if ( port is null )
                throw new ArgumentNullException( nameof( port ) );
            string activity = $"opening port {portName}";
            try
            {
                activity = $"checking if {portName} is open";
                if ( port.IsOpen )
                {
                    return (true, $"{activity}; port {portName} is already open");
                }
                else
                {
                    activity = $"setting port {portName} parameters";
                    port.PortParameters.PortName = portName;
                    port.PortParameters.BaudRate = Properties.Settings.Instance.BaudRate;
                    port.PortParameters.DataBits = Properties.Settings.Instance.DataBits;
                    port.PortParameters.Parity = Properties.Settings.Instance.Parity;
                    port.PortParameters.StopBits = Properties.Settings.Instance.StopBits;
                    port.PortParameters.ReceivedBytesThreshold = 1;
                    port.PortParameters.ReceiveDelay = 1;
                    activity = $"opening {portName}";
                    return port.TryOpen();
                }
            }
            catch ( Exception ex )
            {
                return (false, TraceMethods.TraceError( activity, ex ));
            }
        }

        /// <summary> Attempts to open port from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portName"> Name of the port. </param>
        /// <returns> A Tuple ( bool Success, string Details ) ( <c>true</c> if it succeeds; otherwise <c>false</c>, Details if failed)  </returns>
        public (bool Success, string Details) TryOpenPort( string portName )
        {
            return TryOpenPort( this.Messenger.Port, portName );
        }

        /// <summary> Attempts to close port from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> A Tuple ( bool Success, string Details ) ( <c>true</c> if it succeeds; otherwise <c>false</c>, Details if failed)  </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryClosePort()
        {
            string activity = string.Empty;
            try
            {
                activity = $"checking if port is open";
                if ( this.IsPortOpen )
                {
                    activity = $"closing {this.Messenger.Port.SerialPort.PortName}";
                    return this.Messenger.Port.TryClose();
                }
                else
                {
                    return (true, TraceMethods.TraceInformation( $"{activity}; port already closed" ));
                }
            }
            catch ( Exception ex )
            {
                return (false, TraceMethods.TraceError( activity, ex ));
            }
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Turnaround time. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command ASCII. </param>
        /// <returns> A timespan. </returns>
        public TimeSpan TurnaroundTime( string commandAscii )
        {
            return this.Messenger.Transport.SelectCommand( commandAscii ).TurnaroundTime;
        }

        /// <summary> Converts a commandAscii to a command code. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command ASCII. </param>
        /// <returns> CommandAscii as a LoveCommandCode. </returns>
        public LoveCommandCode ToCommandCode( string commandAscii )
        {
            return ToCommandCode( this.Messenger.Transport.SelectCommand( commandAscii ).CommandCode );
        }

        /// <summary> Converts a commandAscii to a command code. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> CommandAscii as a LoveCommandCode. </returns>
        public static LoveCommandCode ToCommandCode( int value )
        {
            return ( LoveCommandCode ) value;
        }


        #endregion

        #region " MODULE INFO "

        /// <summary> Gets or sets the module address ASCII. </summary>
        /// <value> The module address ASCII. </value>
        public string ModuleAddressAscii { get; private set; }

        /// <summary> Name of the module. </summary>
        private string _ModuleName;

        /// <summary> Gets or sets the name of the module. </summary>
        /// <value> The name of the module. </value>
        public string ModuleName
        {
            get => this._ModuleName;

            set {
                if ( !string.Equals( this.ModuleName, value ) )
                {
                    this._ModuleName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " TEMPERATURE "

        /// <summary> The temperature. </summary>
        private double? _Temperature;

        /// <summary> Gets or sets the temperature. </summary>
        /// <value> The temperature. </value>
        public double? Temperature
        {
            get => this._Temperature;

            protected set {
                if ( !Nullable.Equals( value, this.Temperature ) )
                {
                    this._Temperature = value;
                    this.NotifyPropertyChanged();
                }

                this.WithinSetpointWindow = this.IsWithinAppliedSetpointWindow();
            }
        }

        /// <summary> The temperature accuracy range. </summary>
        private RangeR _TemperatureAccuracyRange;

        /// <summary> Gets or sets the Temperature accuracy range. </summary>
        /// <value> The Temperature accuracy range. </value>
        public RangeR TemperatureAccuracyRange
        {
            get => this._TemperatureAccuracyRange;

            set {
                if ( value != this.TemperatureAccuracyRange )
                {
                    this._TemperatureAccuracyRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Attempts to read temperature from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="retryCount">   Number of retries. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <returns> A Tuple ( bool Success, string Details ) ( <c>true</c> if it succeeds; otherwise <c>false</c>, Details if failed)  </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual (bool Success, string Details) TryReadTemperature( int retryCount, TimeSpan pollInterval )
        {
            string activity = $"reading {this.ModuleName} temperature";
            try
            {
                activity = $"checking if {this.Messenger.Port.SerialPort.PortName} is open";
                if ( this.IsPortOpen )
                {
                    var status = StatusCode.ValueNotSet;
                    while ( retryCount != 0 && status != StatusCode.Okay )
                    {
                        activity = $"reading {this.ModuleName} temperature";
                        retryCount -= 1;
                        status = this.ReadTemperature();
                        if ( status == StatusCode.Okay )
                        {
                            this.Temperature = this.ReadTemperaturePayload.Temperature;
                            this.TemperatureAccuracyRange = new RangeR( Math.Pow( 10d, -this.ReadTemperaturePayload.Accuracy ) );
                            Serial.Port.DoEventsAction?.Invoke();
                        }
                        else
                        {
                            System.Threading.Thread.SpinWait( 10 );
                            _ = Serial.Port.Wait( pollInterval, TimeSpan.Zero, Serial.Port.DoEventsAction );
                        }
                    }

                    return status == StatusCode.Okay
                        ? (true, string.Empty)
                        : (false, TraceMethods.TraceWarning( $"Failed {activity} with status {( int ) status}:{status}" ));
                }
                else
                {
                    throw new InvalidOperationException( $"Failed {activity}; Serial port to this device is not open" );
                }
            }
            catch ( Exception ex )
            {
                this.OnEventHandlerError( ex );
                return (false, Serial.TraceMethods.TraceError( activity, ex ));
            }

        }

        /// <summary> The setpoint window tolerance. </summary>
        private double _SetpointWindowTolerance;

        /// <summary> Gets or sets the setpoint window tolerance. </summary>
        /// <value> The setpoint window tolerance. </value>
        public double SetpointWindowTolerance
        {
            get => this._SetpointWindowTolerance;

            set {
                if ( value != this.SetpointWindowTolerance )
                {
                    this._SetpointWindowTolerance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The setpoint window. </summary>
        private RangeR _SetpointWindow;

        /// <summary>
        /// Gets or sets the setpoint window. The heater is at setpoint if its temperature is within the
        /// <see cref="SetpointWindow"> setpoint window </see>with the specified
        /// <see cref="SetpointWindowTolerance"/> tolerance.
        /// </summary>
        /// <value> The setpoint window. </value>
        public RangeR SetpointWindow
        {
            get => this._SetpointWindow;

            set {
                if ( value != this.SetpointWindow )
                {
                    this._SetpointWindow = value;
                    this.NotifyPropertyChanged();
                }

                this.WithinSetpointWindow = this.IsWithinAppliedSetpointWindow();
            }
        }

        /// <summary> Query if this object is within the applied setpoint window. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> <c>true</c> if within setpoint window; otherwise <c>false</c> </returns>
        public bool IsWithinAppliedSetpointWindow()
        {
            return this.AppliedSetpointTemperature.HasValue && this.SetpointTemperature.HasValue && this.SetpointAccuracyRange.Contains( this.SetpointTemperature.Value - this.AppliedSetpointTemperature.Value ) && this.Temperature.HasValue && this.SetpointWindow.Contains( this.Temperature.Value - this.SetpointTemperature.Value, this.SetpointWindowTolerance );
        }

        /// <summary> True to within setpoint window. </summary>
        private bool _WithinSetpointWindow;

        /// <summary> Gets or sets the within setpoint window. </summary>
        /// <value> The within setpoint window. </value>
        public bool WithinSetpointWindow
        {
            get => this._WithinSetpointWindow;

            set {
                if ( value != this.WithinSetpointWindow )
                {
                    this._WithinSetpointWindow = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " TEMPERATURE: READ "

        /// <summary> Gets or sets the read temperature payload. </summary>
        /// <value> The temperature payload. </value>
        public ReadTemperaturePayload ReadTemperaturePayload { get; private set; }

        /// <summary> Reads the temperature. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The temperature. </returns>
        public StatusCode ReadTemperature()
        {
            this.ReadTemperaturePayload = new ReadTemperaturePayload();
            string activity = "building temperature query command";
            using ( IProtocolMessage replyMessage = new ProtocolMessage() )
            {
                replyMessage.ParseEnabled = true;
                replyMessage.Prefix = ProtocolMessage.LovePrompt;
                replyMessage.ModuleAddress = this.Messenger.ModuleAddress;
                this.Messenger.Port.MessageParser = replyMessage;
                replyMessage.MessageType = MessageType.Response;
                this.Messenger.InputMessage = replyMessage;
                using IProtocolMessage commandMessage = new ProtocolMessage {
                    ParseEnabled = false,
                    Prefix = ProtocolMessage.LovePrompt,
                    ModuleAddress = this.Messenger.ModuleAddress,
                    CommandAscii = this.Messenger.Transport.Commands[( int ) LoveCommandCode.ReadTemperature].CommandAscii,
                    MessageType = MessageType.Command
                };
                activity = "reading temperature";
                this.Messenger.OutputMessage = commandMessage;
                _ = this.Query( commandMessage, Properties.Settings.Instance.ModuleReadTimeout );
            }

            if ( this.Transport.IsSuccess() )
            {
                this.Transport.TransportStatus = this.ReadTemperaturePayload.Parse( this.Transport.ReceivedProtocolMessage.Payload );
            }
            else
            {
                this.ReadTemperaturePayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        /// <summary> Reply read temperature payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        public StatusCode ReplyReadTemperature()
        {
            this.ReadTemperaturePayload = new ReadTemperaturePayload();
            string activity = "building read module temperature reply";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.LovePrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) LoveCommandCode.ReadTemperature].CommandAscii;
                message.Payload = ReadTemperaturePayload.SimulatePayload().Payload;
                activity = "replying read temperature";
                _ = this.Query( message, Properties.Settings.Instance.ModuleReadTimeout );
            }

            if ( this.Transport.IsSuccess() )
            {
            }
            else
            {
                this.ReadTemperaturePayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        #endregion

        #region " SETPOINT TEMPERATURE "

        /// <summary> The applied setpoint temperature. </summary>
        private double? _AppliedSetpointTemperature;

        /// <summary> Gets or sets the applied setpoint temperature. </summary>
        /// <value> The applied setpoint temperature. </value>
        public double? AppliedSetpointTemperature
        {
            get => this._AppliedSetpointTemperature;

            protected set {
                if ( !Nullable.Equals( value, this.AppliedSetpointTemperature ) )
                {
                }

                this._AppliedSetpointTemperature = value;
                this.NotifyPropertyChanged();
                this.WithinSetpointWindow = this.IsWithinAppliedSetpointWindow();
            }
        }

        /// <summary> The setpoint accuracy range. </summary>
        private RangeR _SetpointAccuracyRange;

        /// <summary> Gets or sets the setpoint accuracy range. </summary>
        /// <value> The setpoint accuracy range. </value>
        public RangeR SetpointAccuracyRange
        {
            get => this._SetpointAccuracyRange;

            set {
                if ( value != this.SetpointAccuracyRange )
                {
                    this._SetpointAccuracyRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The setpoint temperature. </summary>
        private double? _SetpointTemperature;

        /// <summary> Gets or sets the setpoint temperature. </summary>
        /// <value> The setpoint temperature. </value>
        public double? SetpointTemperature
        {
            get => this._SetpointTemperature;

            protected set {
                if ( !Nullable.Equals( value, this.SetpointTemperature ) )
                {
                }

                this._SetpointTemperature = value;
                this.NotifyPropertyChanged();
                this.WithinSetpointWindow = this.IsWithinAppliedSetpointWindow();
            }
        }

        /// <summary>
        /// Attempts to apply (write and then read) setpoint Temperature from the given data.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="temperature">  The temperature. </param>
        /// <param name="retryCount">   Number of retries. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <returns> A Tuple ( bool Success, string Details ) ( <c>true</c> if it succeeds; otherwise <c>false</c>, Details if failed)  </returns>
        public (bool Success, string Details) TryApplySetpointTemperature( double temperature, int retryCount, TimeSpan pollInterval )
        {
            (bool Success, string Details) result = this.TryWriteSetpointTemperature( temperature, retryCount, pollInterval );
            if ( result.Success )
            {
                _ = Serial.Port.Wait( LoveCommand.SelectRefractoryPeriod( LoveCommandCode.WriteSetpoint ), TimeSpan.Zero, Serial.Port.DoEventsAction );
                result = this.TryReadSetpointTemperature( retryCount, pollInterval );
            }

            return result;
        }

        /// <summary> Attempts to read setpoint Temperature from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="retryCount">   Number of retries. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <returns> A Tuple ( bool Success, string Details ) ( <c>true</c> if it succeeds; otherwise <c>false</c>, Details if failed)  </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryReadSetpointTemperature( int retryCount, TimeSpan pollInterval )
        {
            string activity = $"reading {this.ModuleName} setpoint temperature";
            try
            {
                activity = $"checking if {this.Messenger.Port.SerialPort.PortName} is open";
                if ( this.IsPortOpen )
                {
                    var status = StatusCode.ValueNotSet;
                    while ( retryCount != 0 && status != StatusCode.Okay )
                    {
                        retryCount -= 1;
                        activity = $"reading {this.ModuleName} setpoint temperature";
                        status = this.ReadSetpoint();
                        if ( status == StatusCode.Okay )
                        {
                            this.SetpointTemperature = this.ReadSetpointPayload.Temperature;
                            this.SetpointAccuracyRange = new RangeR( Math.Pow( 10d, -this.ReadSetpointPayload.Accuracy ) );
                            Serial.Port.DoEventsAction?.Invoke();
                        }
                        else
                        {
                            System.Threading.Thread.SpinWait( 10 );
                            _ = Serial.Port.Wait( pollInterval, TimeSpan.Zero, Serial.Port.DoEventsAction );
                        }
                    }

                    return status == StatusCode.Okay
                        ? (true, string.Empty)
                        : (false, TraceMethods.TraceWarning( $"Failed {activity} with status {( int ) status}:{status}" ));
                }
                else
                {
                    throw new InvalidOperationException( $"Failed {activity}; Serial port to this device is not open" );
                }
            }
            catch ( Exception ex )
            {
                return (false, TraceMethods.TraceError( activity, ex ));
            }
        }

        /// <summary> Attempts to write setpoint temperature from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="temperature">  The temperature. </param>
        /// <param name="retryCount">   Number of retries. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <returns> A Tuple ( bool Success, string Details) (<c>true</c> if it succeeds; otherwise <c>false</c>, details if failed) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryWriteSetpointTemperature( double temperature, int retryCount, TimeSpan pollInterval )
        {
            string activity = $"writing {this.ModuleName} setpoint temperature";
            try
            {
                activity = $"checking if {this.Messenger.Port.SerialPort.PortName} is open";
                if ( this.IsPortOpen )
                {
                    var status = StatusCode.ValueNotSet;
                    while ( retryCount != 0 && status != StatusCode.Okay )
                    {
                        retryCount -= 1;
                        activity = $"writing {this.ModuleName} setpoint temperature";
                        status = this.WriteSetpoint( temperature );
                        if ( status == StatusCode.Okay )
                        {
                            this.AppliedSetpointTemperature = this.WriteSetpointPayload.Temperature;
                            Serial.Port.DoEventsAction?.Invoke();
                        }
                        else
                        {
                            System.Threading.Thread.SpinWait( 10 );
                            _ = Serial.Port.Wait( pollInterval, TimeSpan.Zero, Serial.Port.DoEventsAction );
                        }
                    }

                    return status == StatusCode.Okay
                        ? (true, string.Empty)
                        : (false, TraceMethods.TraceWarning( $"Failed {activity} with status {( int ) status}:{status}" ));
                }
                else
                {
                    throw new InvalidOperationException( $"Failed {activity}; Serial port to this device is not open" );
                }
            }
            catch ( Exception ex )
            {
                return (false, TraceMethods.TraceError( activity, ex ));
            }

        }

        #endregion

        #region " SETPOINT: READ "

        /// <summary> Gets or sets the Read Setpoint payload. </summary>
        /// <value> The Setpoint payload. </value>
        public ReadSetpointPayload ReadSetpointPayload { get; private set; }

        /// <summary> Reads the setpoint. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The setpoint. </returns>
        public StatusCode ReadSetpoint()
        {
            this.ReadSetpointPayload = new ReadSetpointPayload();
            string activity = "building Setpoint query command";
            using ( IProtocolMessage replyMessage = new ProtocolMessage() )
            {
                replyMessage.ParseEnabled = true;
                replyMessage.Prefix = ProtocolMessage.LovePrompt;
                replyMessage.ModuleAddress = this.Messenger.ModuleAddress;
                this.Messenger.Port.MessageParser = replyMessage;
                replyMessage.MessageType = MessageType.Response;
                this.Messenger.InputMessage = replyMessage;
                using IProtocolMessage commandMessage = new ProtocolMessage {
                    ParseEnabled = false,
                    Prefix = ProtocolMessage.LovePrompt,
                    ModuleAddress = this.Messenger.ModuleAddress,
                    CommandAscii = this.Messenger.Transport.Commands[( int ) LoveCommandCode.ReadSetpoint].CommandAscii,
                    MessageType = MessageType.Command
                };
                activity = "reading Setpoint";
                this.Messenger.OutputMessage = commandMessage;
                _ = this.Query( commandMessage, Properties.Settings.Instance.ModuleReadTimeout );
            }

            if ( this.Transport.IsSuccess() )
            {
                this.Transport.TransportStatus = this.ReadSetpointPayload.Parse( this.Transport.ReceivedProtocolMessage.Payload );
            }
            else
            {
                this.ReadSetpointPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        /// <summary> Reply read setpoint. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        public StatusCode ReplyReadSetpoint()
        {
            this.ReadSetpointPayload = new ReadSetpointPayload();
            string activity = "building read module setpoint read reply";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.LovePrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) LoveCommandCode.ReadSetpoint].CommandAscii;
                message.Payload = ReadSetpointPayload.SimulatePayload().Payload;
                activity = "replying read Setpoint";
                _ = this.Query( message, Properties.Settings.Instance.ModuleReadTimeout );
            }

            if ( this.Transport.IsSuccess() )
            {
            }
            else
            {
                this.ReadSetpointPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        #endregion

        #region " SETPOINT WRITE "

        /// <summary> Gets or sets the reply payload. </summary>
        /// <value> The reply payload. </value>
        public ReplyPayload ReplyPayload { get; private set; }

        /// <summary> Gets or sets the write setpoint payload. </summary>
        /// <value> The setpoint payload. </value>
        public WriteSetpointPayload WriteSetpointPayload { get; private set; }

        /// <summary> Writes setpoint. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="setpoint"> The setpoint. </param>
        /// <returns> A StatusCode. </returns>
        public StatusCode WriteSetpoint( double setpoint )
        {
            this.WriteSetpointPayload = new WriteSetpointPayload() { Temperature = setpoint };
            this.ReplyPayload = new ReplyPayload();
            string activity = "building setpoint command message";
            using ( IProtocolMessage replyMessage = new ProtocolMessage() )
            {
                replyMessage.ParseEnabled = true;
                replyMessage.Prefix = ProtocolMessage.LovePrompt;
                replyMessage.ModuleAddress = this.Messenger.ModuleAddress;
                this.Messenger.Port.MessageParser = replyMessage;
                replyMessage.MessageType = MessageType.Response;
                this.Messenger.InputMessage = replyMessage;
                using IProtocolMessage commandMessage = new ProtocolMessage {
                    ParseEnabled = false,
                    Prefix = ProtocolMessage.LovePrompt,
                    ModuleAddress = this.Messenger.ModuleAddress,
                    CommandAscii = this.Messenger.Transport.Commands[( int ) LoveCommandCode.WriteSetpoint].CommandAscii,
                    MessageType = MessageType.Command,
                    Payload = this.WriteSetpointPayload.Build()
                };
                activity = "Writing setpoint";
                this.Messenger.OutputMessage = commandMessage;
                _ = this.Query( commandMessage, Properties.Settings.Instance.ModuleReadTimeout );
            }

            if ( this.Transport.IsSuccess() )
            {
                this.Transport.TransportStatus = this.ReplyPayload.Parse( this.Transport.ReceivedProtocolMessage.Payload );
            }
            else
            {
                this.ReplyPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        /// <summary> Reply write setpoint. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        public StatusCode ReplyWriteSetpoint()
        {
            this.ReplyPayload = new ReplyPayload();
            string activity = "building write module setup reply";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.LovePrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = string.Empty;
                message.Payload = ReplyPayload.SimulatePayload().Payload;
                activity = "replying write setpoint";
                _ = this.Query( message, Properties.Settings.Instance.ModuleReadTimeout );
            }

            if ( this.Transport.IsSuccess() )
            {
            }
            else
            {
                this.ReplyPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        #endregion

        #region " COLLECTOR (listener) PROCESSING "

        /// <summary> Listener message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Protocol event information. </param>
        protected void ListenerMessageReceived( ProtocolEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            switch ( this.ToCommandCode( e.ProtocolMessage.CommandAscii ) )
            {
                case LoveCommandCode.ReadTemperature:
                    {
                        _ = this.ReplyReadTemperature();
                        break;
                    }

                case LoveCommandCode.ReadSetpoint:
                    {
                        _ = this.ReplyReadSetpoint();
                        break;
                    }

                case LoveCommandCode.WriteSetpoint:
                    {
                        _ = this.ReplyWriteSetpoint();
                        break;
                    }
            }
        }

        /// <summary> Notifies a message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Protocol event information. </param>
        protected override void NotifyMessageReceived( ProtocolEventArgs e )
        {
            base.NotifyMessageReceived( e );
            if ( this.Messenger.MessengerRole == MessengerRole.Collector && this.Transport.IsSuccess() )
            {
                this.ListenerMessageReceived( e );
            }
        }

        #endregion

    }
}
