
namespace isr.Ports.Love
{

    /// <summary> A Love 16A Temperature Controller Module class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-07-02 </para>
    /// </remarks>
    public class Love16A : LoveBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddressAscii"> The module address ASCII. </param>
        public Love16A( string moduleAddressAscii ) : base( moduleAddressAscii )
        {
        }

        #endregion

        #region " PRESETTABLE "

        /// <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
        /// <remarks> Use this to customize the reset. </remarks>
        public override void InitKnownState()
        {
            this.InitKnownState( nameof( Love16A ) );
            _ = Serial.TraceMethods.TraceInformation( $"Done initializing {this.ModuleName}.{this.Messenger.ModuleAddress} known state" );
        }

        #endregion

    }
}
