using System;
using System.Collections.Generic;

using isr.Ports.Serial.PayloadExtensions;
using isr.Ports.Teleport;

namespace isr.Ports.Love
{

    /// <summary> A write setpoint payload. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-07-05 </para>
    /// </remarks>
    public class WriteSetpointPayload : IPayload
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public WriteSetpointPayload() : base()
        {
            this.Accuracy = 1;
        }

        #endregion

        #region " SIMULATE A REPLY "

        /// <summary> Simulate payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A SetupPayload. </returns>
        public static WriteSetpointPayload SimulatePayload()
        {
            var result = new WriteSetpointPayload() { Temperature = 18.0d };
            return result;
        }

        #endregion

        #region " FIELDS "

        /// <summary> The accuracy. </summary>
        private int _Accuracy;

        /// <summary> Gets or sets the accuracy. </summary>
        /// <value> The accuracy. </value>
        public int Accuracy
        {
            get => this._Accuracy;

            set {
                if ( this.Accuracy != value )
                {
                    this._Accuracy = value;
                }
            }
        }

        /// <summary> The temperature. </summary>
        private double _Temperature;

        /// <summary> Gets or sets the temperature. </summary>
        /// <value> The temperature. </value>
        public double Temperature
        {
            get => this._Temperature;

            set {
                if ( value != this.Temperature )
                {
                    this._Temperature = value;
                    int wholeTemp = ( int ) (value * Math.Pow( 10d, this.Accuracy ));
                    this.Sign = value > 0d ? PositiveSign : NegativeSign;
                    this.TemperatureAscii = wholeTemp.ToString( "0000" );
                }
            }
        }

        /// <summary> The temperature ASCII. </summary>
        private string _TemperatureAscii;

        /// <summary> Gets or sets the temperature ASCII. </summary>
        /// <value> The temperature ASCII. </value>
        public string TemperatureAscii
        {
            get => this._TemperatureAscii;

            set {
                if ( !string.Equals( this.TemperatureAscii, value ) )
                {
                    this._TemperatureAscii = value;
                    int wholeTemp = int.Parse( value );
                    if ( this.Sign == ReadTemperaturePayload.NegativeSign )
                    {
                        wholeTemp = -wholeTemp;
                    }

                    this.Temperature = wholeTemp / Math.Pow( 10d, this.Accuracy );
                }
            }
        }

        /// <summary> The positive sign. </summary>
        public const int PositiveSign = 0;

        /// <summary> The negative sign. </summary>
        public const int NegativeSign = 255;

        /// <summary> The sign. </summary>
        private int _Sign;

        /// <summary> Gets or sets the Sign. </summary>
        /// <value> The Sign. </value>
        public int Sign
        {
            get => this._Sign;

            set {
                if ( this.Sign != value || string.IsNullOrEmpty( this.SignAscii ) )
                {
                    this._Sign = value;
                    this.SignAscii = value.ToString( "X2" );
                }
            }
        }

        /// <summary> The sign ASCII. </summary>
        private string _SignAscii;

        /// <summary> Gets or sets the Sign ASCII. </summary>
        /// <value> The Sign ASCII. </value>
        public string SignAscii
        {
            get => this._SignAscii;

            set {
                if ( !string.Equals( this.SignAscii, value ) )
                {
                    this._SignAscii = value;
                    this.Sign = int.Parse( value );
                }
            }
        }

        /// <summary> The reading. </summary>
        private string _Reading;

        /// <summary> Gets or sets the reading. </summary>
        /// <value> The reading. </value>
        public string Reading
        {
            get => this._Reading;

            set {
                if ( !string.Equals( value, this.Reading ) )
                {
                    this._Reading = value;
                }
            }
        }

        #endregion

        #region " I PAYLOAD IMPLEMENTATION "

        /// <summary> Gets or sets a list of payloads. </summary>
        /// <value> A list of payloads. </value>
        private List<byte> PayloadList { get; set; }

        /// <summary> Gets or sets the payload. </summary>
        /// <value> The payload. </value>
        public IEnumerable<byte> Payload
        {
            get => this.PayloadList;

            set => this.PayloadList = new List<byte>( value );
        }

        /// <summary> Gets or sets the status code. </summary>
        /// <value> The status code. </value>
        public StatusCode StatusCode { get; set; }

        /// <summary> Populates the payload from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="payload"> The payload. </param>
        /// <returns> A StatusCode. </returns>
        public StatusCode Parse( IEnumerable<byte> payload )
        {
            this.StatusCode = StatusCode.Okay;
            this.Payload = payload;
            var values = new Queue<byte>( payload );
            System.Text.Encoding encoding = new System.Text.ASCIIEncoding();
            this.TemperatureAscii = encoding.GetString( new byte[] { values.Dequeue(), values.Dequeue(), values.Dequeue(), values.Dequeue() } );
            this.SignAscii = encoding.GetString( new byte[] { values.Dequeue(), values.Dequeue() } );
            this._Reading = $"{this.TemperatureAscii}{this.SignAscii}";
            return this.StatusCode;
        }

        /// <summary> Converts the payload to bytes. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> This object as an IEnumerable(Of Byte) </returns>
        public IEnumerable<byte> Build()
        {
            this.StatusCode = StatusCode.Okay;
            var result = new List<byte>();
            result.AddRange( this.TemperatureAscii.ToBytes() );
            result.AddRange( this.SignAscii.ToBytes() );
            this.Payload = result;
            return this.Payload;
        }

        #endregion

    }
}
