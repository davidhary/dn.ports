using System;
using System.Collections.Generic;
using System.Linq;

using isr.Ports.Serial.PayloadExtensions;
using isr.Ports.Teleport;

namespace isr.Ports.Love
{

    /// <summary> A read temperature payload. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-07-05 </para>
    /// </remarks>
    public class ReadTemperaturePayload : IPayload
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ReadTemperaturePayload() : base()
        {
        }

        #endregion

        #region " SIMULATE A REPLY "

        /// <summary> Simulate payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A SetupPayload. </returns>
        public static ReadTemperaturePayload SimulatePayload()
        {
            var result = new ReadTemperaturePayload() { AlarmStatus = 48, Accuracy = 1, Temperature = 24.2d };
            return result;
        }

        #endregion

        #region " FIELDS "

        /// <summary> The alarm status. </summary>
        private int _AlarmStatus;

        /// <summary> Gets or sets the alarm status. </summary>
        /// <value> The alarm status. </value>
        public int AlarmStatus
        {
            get => this._AlarmStatus;

            set {
                if ( this.AlarmStatus != value )
                {
                    this._AlarmStatus = value;
                    this.AlarmStatusAscii = value.ToString( "00" );
                }
            }
        }

        /// <summary> The alarm status ASCII. </summary>
        private string _AlarmStatusAscii;

        /// <summary> Gets or sets the alarm status ASCII. </summary>
        /// <value> The alarm status ASCII. </value>
        public string AlarmStatusAscii
        {
            get => this._AlarmStatusAscii;

            set {
                if ( !string.Equals( this.AlarmStatusAscii, value ) )
                {
                    this._AlarmStatusAscii = value;
                    this.AlarmStatus = int.Parse( value );
                }
            }
        }

        /// <summary> The accuracy. </summary>
        private int _Accuracy;

        /// <summary> Gets or sets the accuracy. </summary>
        /// <value> The accuracy. </value>
        public int Accuracy
        {
            get => this._Accuracy;

            set {
                if ( this.Accuracy != value )
                {
                    this._Accuracy = value;
                    this.AccuracyAscii = value.ToString( "0" );
                }
            }
        }

        /// <summary> The accuracy ASCII. </summary>
        private string _AccuracyAscii;

        /// <summary> Gets or sets the accuracy ASCII. </summary>
        /// <value> The accuracy ASCII. </value>
        public string AccuracyAscii
        {
            get => this._AccuracyAscii;

            set {
                if ( !string.Equals( this.AccuracyAscii, value ) )
                {
                    this._AccuracyAscii = value;
                    this.Accuracy = int.Parse( value );
                }
            }
        }

        /// <summary> The temperature. </summary>
        private double _Temperature;

        /// <summary> Gets or sets the temperature. </summary>
        /// <value> The temperature. </value>
        public double Temperature
        {
            get => this._Temperature;

            set {
                if ( value != this.Temperature )
                {
                    this._Temperature = value;
                    int wholeTemp = ( int ) (value * Math.Pow( 10d, this.Accuracy ));
                    this.Sign = value > 0d ? PositiveSign : NegativeSign;
                    this.TemperatureAscii = wholeTemp.ToString( "0000" );
                }
            }
        }

        /// <summary> The temperature ASCII. </summary>
        private string _TemperatureAscii;

        /// <summary> Gets or sets the temperature ASCII. </summary>
        /// <value> The temperature ASCII. </value>
        public string TemperatureAscii
        {
            get => this._TemperatureAscii;

            set {
                if ( !string.Equals( this.TemperatureAscii, value ) )
                {
                    this._TemperatureAscii = value;
                    int wholeTemp = int.Parse( value );
                    if ( this.Sign == NegativeSign )
                    {
                        wholeTemp = -wholeTemp;
                    }

                    this.Temperature = wholeTemp / Math.Pow( 10d, this.Accuracy );
                }
            }
        }

        /// <summary> The positive sign. </summary>
        public const int PositiveSign = 4;

        /// <summary> The negative sign. </summary>
        public const int NegativeSign = 5;

        /// <summary> The sign. </summary>
        private int _Sign;

        /// <summary> Gets or sets the Sign. </summary>
        /// <value> The Sign. </value>
        public int Sign
        {
            get => this._Sign;

            set {
                if ( this.Sign != value )
                {
                    this._Sign = value;
                    this.SignAscii = value.ToString( "0" );
                }
            }
        }

        /// <summary> The sign ASCII. </summary>
        private string _SignAscii;

        /// <summary> Gets or sets the Sign ASCII. </summary>
        /// <value> The Sign ASCII. </value>
        public string SignAscii
        {
            get => this._SignAscii;

            set {
                if ( !string.Equals( this.SignAscii, value ) )
                {
                    this._SignAscii = value;
                    this.Sign = int.Parse( value );
                }
            }
        }

        /// <summary> The reading. </summary>
        private string _Reading;

        /// <summary> Gets or sets the reading. </summary>
        /// <value> The reading. </value>
        public string Reading
        {
            get => this._Reading;

            set {
                if ( !string.Equals( value, this.Reading ) )
                {
                    this._Reading = value;
                }
            }
        }

        #endregion

        #region " I PAYLOAD IMPLEMENTATION "

        /// <summary> Gets or sets a list of payloads. </summary>
        /// <value> A list of payloads. </value>
        private List<byte> PayloadList { get; set; }

        /// <summary> Gets or sets the payload. </summary>
        /// <value> The payload. </value>
        public IEnumerable<byte> Payload
        {
            get => this.PayloadList;

            set => this.PayloadList = new List<byte>( value );
        }

        /// <summary> Gets or sets the status code. </summary>
        /// <value> The status code. </value>
        public StatusCode StatusCode { get; set; }

        /// <summary> Populates the payload from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="payload"> The payload. </param>
        /// <returns> A StatusCode. </returns>
        public StatusCode Parse( IEnumerable<byte> payload )
        {
            this.StatusCode = StatusCode.Okay;
            this.Payload = payload;
            var values = new Queue<byte>( payload );
            System.Text.Encoding encoding = new System.Text.ASCIIEncoding();
            if ( this.StatusCode == StatusCode.Okay )
            {
                if ( values.Any() && values.Count >= 2 )
                {
                    this.AlarmStatusAscii = encoding.GetString( new byte[] { values.Dequeue(), values.Dequeue() } );
                }
                else
                {
                    this.StatusCode = StatusCode.MessageIncomplete;
                }
            }

            if ( this.StatusCode == StatusCode.Okay )
            {
                if ( values.Any() )
                {
                    this.AccuracyAscii = encoding.GetString( new byte[] { values.Dequeue() } );
                }
                else
                {
                    this.StatusCode = StatusCode.MessageIncomplete;
                }
            }

            if ( this.StatusCode == StatusCode.Okay )
            {
                if ( values.Any() )
                {
                    this.SignAscii = encoding.GetString( new byte[] { values.Dequeue() } );
                }
                else
                {
                    this.StatusCode = StatusCode.MessageIncomplete;
                }
            }

            if ( this.StatusCode == StatusCode.Okay )
            {
                if ( values.Any() && values.Count >= 4 )
                {
                    this.TemperatureAscii = encoding.GetString( new byte[] { values.Dequeue(), values.Dequeue(), values.Dequeue(), values.Dequeue() } );
                }
                else
                {
                    this.StatusCode = StatusCode.MessageIncomplete;
                }
            }

            this._Reading = $"{this.AlarmStatusAscii}{this.AccuracyAscii}{this.SignAscii}{this.TemperatureAscii}";
            return this.StatusCode;
        }

        /// <summary> Converts the payload to bytes. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> This object as an IEnumerable(Of Byte) </returns>
        public IEnumerable<byte> Build()
        {
            this.StatusCode = StatusCode.Okay;
            var result = new List<byte>();
            result.AddRange( this.AlarmStatusAscii.ToBytes() );
            result.AddRange( this.AccuracyAscii.ToBytes() );
            result.AddRange( this.SignAscii.ToBytes() );
            result.AddRange( this.TemperatureAscii.ToBytes() );
            this.Payload = result;
            return this.Payload;
        }

        #endregion

    }
}
