using System;
using System.Diagnostics;

namespace isr.Ports.Love.MSTest
{
    /// <summary>   A settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( Love16Settings ) )]
    public class Love16Settings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public Love16Settings() : base( System.Reflection.Assembly.GetAssembly( typeof( Love16Settings ) ) )
        { }

        #region " TEST SITE CONFIGURATION "

        private bool _Verbose;
        /// <summary>   Gets or sets a value indicating whether test reporting is verbose. </summary>
        /// <value> True if verbose, false if not. </value>
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( value, this.Verbose ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled;
        /// <summary>   Gets or sets a value indicating whether testing is enabled. </summary>
        /// <value> True if enabled, false if not. </value>
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( value, this.Enabled ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _All;
        /// <summary>   Gets or sets a value indicating whether all testing is to be ran. </summary>
        /// <value> True if All, false if not. </value>
        public bool All
        {
            get => this._All;
            set {
                if ( !bool.Equals( value, this.All ) )
                {
                    this._All = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LOVE 16 "

        /// <summary>   Name of the port. </summary>
        private string _PortName = "COM3";
        /// <summary>   Gets or sets the name of the Port. </summary>
        /// <value> The name of the Port. </value>
        public string PortName
        {
            get => this._PortName;
            set {
                if ( !string.Equals( value, this.PortName ) )
                {
                    this._PortName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _BaudRate = 9600;
        /// <summary>   Gets or sets the baud rate. </summary>
        /// <value> The baud rate. </value>
        public int BaudRate
        {
            get => this._BaudRate;
            set {
                if ( !int.Equals( value, this.BaudRate ) )
                {
                    this._BaudRate = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _DataBits = 8;
        /// <summary>   Gets or sets the data bits. </summary>
        /// <value> The data bits. </value>
        public int DataBits
        {
            get => this._DataBits;
            set {
                if ( !int.Equals( value, this.DataBits ) )
                {
                    this._DataBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.IO.Ports.StopBits _StopBits = System.IO.Ports.StopBits.One;
        /// <summary>   Gets or sets the stop bits. </summary>
        /// <value> The stop bits. </value>
        public System.IO.Ports.StopBits StopBits
        {
            get => this._StopBits;
            set {
                if ( !System.IO.Ports.StopBits.Equals( value, this.StopBits ) )
                {
                    this._StopBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.IO.Ports.Parity _Parity = System.IO.Ports.Parity.None;
        /// <summary>   Gets or sets the parity. </summary>
        /// <value> The parity. </value>
        public System.IO.Ports.Parity Parity
        {
            get => this._Parity;
            set {
                if ( !System.IO.Ports.Parity.Equals( value, this.Parity ) )
                {
                    this._Parity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _ModuleConfigurationFileName = "";
        /// <summary>   Gets or sets the filename of the module configuration file. </summary>
        /// <value> The filename of the module configuration file. </value>
        public string ModuleConfigurationFileName
        {
            get => this._ModuleConfigurationFileName;
            set {
                if ( !string.Equals( value, this.ModuleConfigurationFileName ) )
                {
                    this._ModuleConfigurationFileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _PortConfigurationFileName = "";
        /// <summary>   Gets or sets the filename of the port configuration file. </summary>
        /// <value> The filename of the port configuration file. </value>
        public string PortConfigurationFileName
        {
            get => this._PortConfigurationFileName;
            set {
                if ( !string.Equals( value, this.PortConfigurationFileName ) )
                {
                    this._PortConfigurationFileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _ModuleAddress = "02";
        /// <summary>   Gets or sets the module address. </summary>
        /// <value> The module address. </value>
        public string ModuleAddress
        {
            get => this._ModuleAddress;
            set {
                if ( !String.Equals( value, this.ModuleAddress ) )
                {
                    this._ModuleAddress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _SetpointTemperature = 18;
        /// <summary>   Gets or sets the setpoint temperature. </summary>
        /// <value> The setpoint temperature. </value>
        public int SetpointTemperature
        {
            get => this._SetpointTemperature;
            set {
                if ( !int.Equals( value, this.SetpointTemperature ) )
                {
                    this._SetpointTemperature = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }

}
