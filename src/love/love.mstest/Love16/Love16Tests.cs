using System;
using System.Linq;

using isr.Ports.Serial;
using isr.Ports.Serial.PayloadExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Ports.Love.MSTest
{

    /// <summary> Tests the Love 16A Temperature Controller Module . </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-26 </para>
    /// </remarks>
    [TestClass()]
    public class Love16Tests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );

            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( AppSettings.Instance.TestSiteSettings.Exists(), $"{nameof( AppSettings.Instance.TestSiteSettings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( AppSettings.Instance.TestSiteSettings.TimeZoneOffset() ) < expectedUpperLimit,
                           $"{nameof( AppSettings.Instance.TestSiteSettings.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( AppSettings.Instance.Love16Settings.Exists(), $"{nameof( AppSettings.Instance.Love16Settings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " CHECK SUM TESTS "

        /// <summary> (Unit Test Method) tests ASCII bytes. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void AsciiBytesTest()
        {
            string asciiValue = "00";
            var expectedBytes = new byte[] { 48, 48 };
            System.Text.Encoding encoding = new System.Text.ASCIIEncoding();
            var actualBytes = encoding.GetBytes( asciiValue );
            Assert.IsTrue( expectedBytes.SequenceEqual( actualBytes ), $"Expected.SequenceEquals values for text={asciiValue}" );
            actualBytes = asciiValue.ToBytes();
            Assert.IsTrue( expectedBytes.SequenceEqual( actualBytes ), $"{asciiValue}.ToBytes values for text={asciiValue}" );
        }

        /// <summary> (Unit Test Method) tests read command checksum. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void ReadCommandChecksumTest()
        {
            using var loveProtocol = new Love.ProtocolMessage() {
                MessageType = Teleport.MessageType.Command,
                CommandAscii = Love.LoveCommandCollection.Get()[( int ) Love.LoveCommandCode.ReadTemperature].CommandAscii,
                ModuleAddressAscii = "01"
            };
            string expectedChecksum = "C1";
            string actualChecksum = loveProtocol.CalculateChecksumHex();
            _ = loveProtocol.BuildStream( Teleport.MessageType.Command );
            Assert.AreEqual( expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}" );
        }

        /// <summary> (Unit Test Method) tests temperature response checksum. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void TemperatureResponseChecksumTest()
        {
            var payload = Love.ReadTemperaturePayload.SimulatePayload();
            using var loveProtocol = new Love.ProtocolMessage() {
                MessageType = Teleport.MessageType.Response,
                CommandAscii = string.Empty,
                ModuleAddressAscii = "01",
                Payload = payload.Build()
            };
            string expectedChecksum = "46";
            string actualChecksum = loveProtocol.CalculateChecksumHex();
            _ = loveProtocol.BuildStream( Teleport.MessageType.Response );
            Assert.AreEqual( expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}" );
        }

        /// <summary> (Unit Test Method) tests setpoint command checksum. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void SetpointCommandChecksumTest()
        {
            var payload = Love.WriteSetpointPayload.SimulatePayload();
            using var loveProtocol = new Love.ProtocolMessage() {
                MessageType = Teleport.MessageType.Command,
                CommandAscii = Love.LoveCommandCollection.Get()[( int ) Love.LoveCommandCode.WriteSetpoint].CommandAscii,
                ModuleAddressAscii = "01",
                Payload = payload.Build()
            };
            string expectedChecksum = "4C";
            string actualChecksum = loveProtocol.CalculateChecksumHex();
            _ = loveProtocol.BuildStream( Teleport.MessageType.Command );
            Assert.AreEqual( expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}" );
        }

        /// <summary> (Unit Test Method) tests setpoint command address 02 checksum. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void SetpointCommandAddress02ChecksumTest()
        {
            var payload = Love.WriteSetpointPayload.SimulatePayload();
            using var loveProtocol = new Love.ProtocolMessage() {
                MessageType = Teleport.MessageType.Command,
                CommandAscii = Love.LoveCommandCollection.Get()[( int ) Love.LoveCommandCode.WriteSetpoint].CommandAscii,
                ModuleAddressAscii = "02",
                Payload = payload.Build()
            };
            string expectedChecksum = "4D";
            string actualChecksum = loveProtocol.CalculateChecksumHex();
            _ = loveProtocol.BuildStream( Teleport.MessageType.Command );
            Assert.AreEqual( expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}" );
        }

        /// <summary> (Unit Test Method) tests setpoint process 2 address 02 checksum. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void SetpointProcess2Address02ChecksumTest()
        {
            var payload = Love.WriteSetpointPayload.SimulatePayload();
            using var loveProtocol = new Love.ProtocolMessage() {
                MessageType = Teleport.MessageType.Command,
                CommandAscii = Love.LoveCommandCollection.Get()[( int ) Love.LoveCommandCode.WriteSetpointProcess2].CommandAscii,
                ModuleAddressAscii = "02",
                Payload = payload.Build()
            };
            string expectedChecksum = "4E";
            string actualChecksum = loveProtocol.CalculateChecksumHex();
            _ = loveProtocol.BuildStream( Teleport.MessageType.Command );
            Assert.AreEqual( expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}" );
        }

        /// <summary> (Unit Test Method) tests setpoint process 2 address 01 checksum. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void SetpointProcess2Address01ChecksumTest()
        {
            var payload = Love.WriteSetpointPayload.SimulatePayload();
            using var loveProtocol = new Love.ProtocolMessage() {
                MessageType = Teleport.MessageType.Command,
                CommandAscii = Love.LoveCommandCollection.Get()[( int ) Love.LoveCommandCode.WriteSetpointProcess2].CommandAscii,
                ModuleAddressAscii = "01",
                Payload = payload.Build()
            };
            string expectedChecksum = "4D";
            string actualChecksum = loveProtocol.CalculateChecksumHex();
            _ = loveProtocol.BuildStream( Teleport.MessageType.Command );
            Assert.AreEqual( expectedChecksum, actualChecksum, $"Check sum of {loveProtocol.InternalAsciiMessage}" );
        }

        #endregion

        #region " PAYLOAD TESTS "

        /// <summary> Reads temperature payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private static void ReadTemperaturePayload()
        {
            var expectedPayload = Love.ReadTemperaturePayload.SimulatePayload();
            var actualPayload = new Love.ReadTemperaturePayload();
            _ = actualPayload.Parse( expectedPayload.Build() );
            Assert.AreEqual( expectedPayload.AlarmStatus, actualPayload.AlarmStatus, "Alarm status" );
            Assert.AreEqual( expectedPayload.Sign, actualPayload.Sign, "temperature sign" );
            Assert.AreEqual( expectedPayload.Accuracy, actualPayload.Accuracy, "temperature accuracy" );
            Assert.AreEqual( expectedPayload.Temperature, actualPayload.Temperature, "temperature value" );
        }

        /// <summary> (Unit Test Method) tests read temperature payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void ReadTemperaturePayloadTest()
        {
            ReadTemperaturePayload();
        }

        /// <summary> Reads Setpoint payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private static void ReadSetpointPayload()
        {
            var expectedPayload = Love.ReadSetpointPayload.SimulatePayload();
            var actualPayload = new Love.ReadSetpointPayload();
            _ = actualPayload.Parse( expectedPayload.Build() );
            Assert.AreEqual( expectedPayload.Sign, actualPayload.Sign, "Setpoint sign" );
            Assert.AreEqual( expectedPayload.Accuracy, actualPayload.Accuracy, "Setpoint accuracy" );
            Assert.AreEqual( expectedPayload.Temperature, actualPayload.Temperature, "Setpoint value" );
        }

        /// <summary> (Unit Test Method) tests read Setpoint payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void ReadSetpointPayloadTest()
        {
            ReadSetpointPayload();
        }

        /// <summary> Writes the setpoint payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private static void WriteSetpointPayload()
        {
            var expectedPayload = Love.WriteSetpointPayload.SimulatePayload();
            var actualPayload = new Love.WriteSetpointPayload();
            _ = actualPayload.Parse( expectedPayload.Build() );
            Assert.AreEqual( expectedPayload.Temperature, actualPayload.Temperature, "Setpoint temperature" );
            Assert.AreEqual( expectedPayload.Sign, actualPayload.Sign, "Setpoint sign" );
        }

        /// <summary> (Unit Test Method) tests read temperature payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        public void WriteSetpointPayloadTest()
        {
            WriteSetpointPayload();
        }

        #endregion

        #region " PORT FUNCTIONS "

        /// <summary> Configure port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> The port. </param>
        private static void ConfigurePort( IPort port )
        {
            port.PortParameters.PortName = AppSettings.Instance.Love16Settings.PortName;
            Assert.AreEqual( AppSettings.Instance.Love16Settings.PortName, port.PortParameters.PortName, "Port name" );
            port.PortParameters.BaudRate = AppSettings.Instance.Love16Settings.BaudRate;
            Assert.AreEqual( AppSettings.Instance.Love16Settings.BaudRate, port.PortParameters.BaudRate, "Baud Rate" );
            port.PortParameters.DataBits = AppSettings.Instance.Love16Settings.DataBits;
            port.PortParameters.Parity = AppSettings.Instance.Love16Settings.Parity;
            port.PortParameters.StopBits = AppSettings.Instance.Love16Settings.StopBits;
            port.PortParameters.ReceivedBytesThreshold = 1;
            port.PortParameters.ReceiveDelay = 1;
        }

        /// <summary> Opens port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> The port. </param>
        private static void OpenPort( IPort port )
        {
            (bool success, string details) = port.TryOpen();
            Assert.IsTrue( success, details );
            Assert.AreEqual( AppSettings.Instance.Love16Settings.PortName, port.SerialPort.PortName, "Serial Port name" );
            Assert.AreEqual( AppSettings.Instance.Love16Settings.BaudRate, port.SerialPort.BaudRate, "Serial Port Baud Rate" );
        }

        /// <summary> Closes a port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="love16"> The love 16. </param>
        private static void ClosePort( Love.Love16A love16 )
        {
            love16.Messenger.Port.SerialPort.Close();
            Assert.IsFalse( love16.Messenger.Port.SerialPort.IsOpen, $"Serial port is {love16.Messenger.Port.PortParameters.PortName} close" );
        }

        /// <summary> (Unit Test Method) tests open port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        [TestCategory( "Love16" )]
        public void OpenPortTest()
        {
            using var love16a = new Love.Love16A( AppSettings.Instance.Love16Settings.ModuleAddress );
            try
            {
                ConfigurePort( love16a.Messenger.Port );
                OpenPort( love16a.Messenger.Port );
            }
            catch
            {
                throw;
            }
            finally
            {
                ClosePort( love16a );
            }
        }

        #endregion

        #region " MODULE TEMPERATURE READ TESTS "

        /// <summary> Reads a temperature. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="love16"> The love 16. </param>
        private static void ReadTemperature( Love.Love16A love16 )
        {
            var statusCode = love16.ReadTemperature();
            Assert.AreEqual( Teleport.StatusCode.Okay, statusCode, "Read temperature status code" );
            Assert.AreEqual( 1, love16.ReadTemperaturePayload.Accuracy, "Temperature accuracy" );
            Assert.AreEqual( 4, love16.ReadTemperaturePayload.Sign, "Temperature sign" );
            Assert.AreEqual( 48, love16.ReadTemperaturePayload.AlarmStatus, "Temperature alarm status" );
            Assert.AreEqual( 24d, love16.ReadTemperaturePayload.Temperature, 4d, "Temperature" );
        }

        /// <summary> (Unit Test Method) tests read temperature. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        [TestCategory( "Love16" )]
        public void ReadTemperatureTest()
        {
            using var love16a = new Love.Love16A( AppSettings.Instance.Love16Settings.ModuleAddress );
            try
            {
                ConfigurePort( love16a.Messenger.Port );
                OpenPort( love16a.Messenger.Port );
                ReadTemperature( love16a );
            }
            catch
            {
                throw;
            }
            finally
            {
                ClosePort( love16a );
            }
        }

        #endregion

        #region " MODULE SETPOINT WRITE TESTS "

        /// <summary> Writes a setpoint. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="love16">      The love 16. </param>
        /// <param name="temperature"> The temperature. </param>
        private static void WriteSetpoint( Love.Love16A love16, double temperature )
        {
            var statusCode = love16.WriteSetpoint( temperature );
            Assert.AreEqual( Teleport.StatusCode.Okay, statusCode, "Write setpoint status code" );
            Assert.AreEqual( 0, love16.ReplyPayload.ReplyStatus, "Reply status" );
        }

        /// <summary> Reads a setpoint. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="love16">              The love 16. </param>
        /// <param name="expectedTemperature"> The expected temperature. </param>
        private static void ReadSetpoint( Love.Love16A love16, double expectedTemperature )
        {
            var statusCode = love16.ReadSetpoint();
            Assert.AreEqual( Teleport.StatusCode.Okay, statusCode, "Read setpoint status code" );
            Assert.AreEqual( 1, love16.ReadSetpointPayload.Accuracy, "Setpoint accuracy" );
            Assert.AreEqual( Love.ReadSetpointPayload.PositiveSign, love16.ReadSetpointPayload.Sign, "Setpoint sign" );
            Assert.AreEqual( expectedTemperature, love16.ReadSetpointPayload.Temperature, 0.1d, "Setpoint Temperature" );
        }

        /// <summary> (Unit Test Method) writes the setpoint test. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [TestMethod()]
        [TestCategory( "Love16" )]
        public void WriteSetpointTest()
        {
            using var love16a = new Love.Love16A( AppSettings.Instance.Love16Settings.ModuleAddress );
            try
            {
                ConfigurePort( love16a.Messenger.Port );
                OpenPort( love16a.Messenger.Port );
                double temperature = DateTimeOffset.Now.Second + 0.1d * DateTimeOffset.Now.Second;
                WriteSetpoint( love16a, temperature );
                _ = Serial.Port.Wait( Love.LoveCommand.SelectRefractoryPeriod( Love.LoveCommandCode.WriteSetpoint ), TimeSpan.Zero, Serial.Port.DoEventsAction );
                ReadSetpoint( love16a, temperature );
            }
            catch
            {
                throw;
            }
            finally
            {
                ClosePort( love16a );
            }
        }

        #endregion

    }
}
