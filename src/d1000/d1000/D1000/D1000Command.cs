using System;
using System.ComponentModel;
using System.Linq;

using isr.Ports.Teleport;

namespace isr.Ports.D1000
{

    /// <summary> Defines the D1000 family command. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-16 </para>
    /// </remarks>
    public class D1000Command : ProtocolCommand
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandCode"> The command code. </param>
        public D1000Command( D1000CommandCode commandCode ) : base( commandCode )
        {
        }

        /// <summary>
        /// Gets or sets the time it takes from the receipt of the command to when the module starts to
        /// transmit a response.
        /// </summary>
        /// <value> The command timeout. </value>
        public override TimeSpan TurnaroundTime
        {
            get => SelectTurnaroundTime( this.CommandAscii );

            set => base.TurnaroundTime = value;
        }

        /// <summary> The ten millisecond commands. </summary>
        private static readonly string[] TenMillisecondCommands = new string[] { "DI", "DO", "RD", "WE" };

        /// <summary>
        /// Selects the time it takes from the receipt of the command to when the module starts to
        /// transmit a response.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command ASCII. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan SelectTurnaroundTime( string commandAscii )
        {
            return TenMillisecondCommands.Contains( commandAscii, StringComparer.OrdinalIgnoreCase ) ? TimeSpan.FromMilliseconds( 10d ) : TimeSpan.FromMilliseconds( 100d );
        }
    }

    /// <summary> Dictionary of D1000 commands. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-16 </para>
    /// </remarks>
    public sealed class D1000CommandCollection : ProtocolCommandCollection
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private D1000CommandCollection() : base( typeof( D1000CommandCode ) )
        {
        }

        #endregion

        #region " SINGLETON "

        /// <summary>
        /// Gets or sets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object SyncLocker { get; set; } = new object();

        /// <summary> Gets or sets the instance. </summary>
        /// <value> The instance. </value>
        private static D1000CommandCollection Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static D1000CommandCollection Get()
        {
            if ( Instance is null )
            {
                lock ( SyncLocker )
                    Instance = new D1000CommandCollection();
            }

            return Instance;
        }

        #endregion

    }

    /// <summary> Values that represent the D1000 commands. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public enum D1000CommandCode
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "(None)" )]
        None,

        /// <summary> An enum constant representing the read digital inputs option. </summary>
        [Description( "Read Digital Inputs (DI)" )]
        ReadDigitalInputs,

        /// <summary> An enum constant representing the set digital outputs option. </summary>
        [Description( "Set Digital Outputs (DO)" )]
        SetDigitalOutputs,

        /// <summary> An enum constant representing the new data option. </summary>
        [Description( "New Data (ND)" )]
        NewData,

        /// <summary> An enum constant representing the read data option. </summary>
        [Description( "Read Data (RD)" )]
        ReadData,

        /// <summary> An enum constant representing the read events option. </summary>
        [Description( "Read Events (RE)" )]
        ReadEvents,

        /// <summary> An enum constant representing the read low alarm option. </summary>
        [Description( "Read Low Alarm (RL)" )]
        ReadLowAlarm,

        /// <summary> An enum constant representing the read high alarm option. </summary>
        [Description( "Read High Alarm (RH)" )]
        ReadHighAlarm,

        /// <summary> An enum constant representing the read setup option. </summary>
        [Description( "Read Setup (RS)" )]
        ReadSetup,

        /// <summary> An enum constant representing the read zero register option. </summary>
        [Description( "Read Zero Register (RZ)" )]
        ReadZeroRegister,

        /// <summary> An enum constant representing the write enable option. </summary>
        [Description( "Write Enable (WE)" )]
        WriteEnable,

        /// <summary> An enum constant representing the clear alarms option. Write protected Command. </summary>
        [Description( "Clear Alarms (CA)" )]
        ClearAlarms,

        /// <summary> An enum constant representing the clear events option. Write protected Command. </summary>
        [Description( "Clear Events (CE)" )]
        ClearEvents,

        /// <summary> An enum constant representing the clear zero register option. Write protected Command. </summary>
        [Description( "Clear Zero Register (CZ)" )]
        ClearZeroRegister,

        /// <summary> An enum constant representing the disable alarms option. Write protected Command. </summary>
        [Description( "Disable Alarms (DA)" )]
        DisableAlarms,

        /// <summary> An enum constant representing the enable alarms option. Write protected Command. </summary>
        [Description( "Enable Alarms (EA)" )]
        EnableAlarms,

        /// <summary>   An enum constant representing the events clear option. </summary>
        [Description( "Events Clear (EC)" )]
        EventsClear,

        /// <summary> An enum constant representing the set high alarm option. Write protected Command. </summary>
        [Description( "Set High Alarm (HI)" )]
        SetHighAlarm,

        /// <summary> An enum constant representing the set low alarm option. Write protected Command. </summary>
        [Description( "Set Low Alarm (LO)" )]
        SetLowAlarm,

        /// <summary> An enum constant representing the remote reset option. Write protected Command. </summary>
        [Description( "Remote Reset (RR)" )]
        RemoteReset,

        /// <summary> An enum constant representing the setup module option. Write protected Command. </summary>
        [Description( "Setup Module (SU)" )]
        SetupModule,

        /// <summary> An enum constant representing the set setpoint option. Write protected Command. </summary>
        [Description( "Set Setpoint (SP)" )]
        SetSetpoint,

        /// <summary> An enum constant representing the trim span option. Write protected Command. </summary>
        [Description( "Trim Span (TS)" )]
        TrimSpan,

        /// <summary> An enum constant representing the trim zero option. Write protected Command. </summary>
        [Description( "Trim Zero (TZ)" )]
        TrimZero
    }
}
