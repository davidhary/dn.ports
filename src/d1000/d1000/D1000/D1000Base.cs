using System;
using System.Linq;

using Arebis.UnitsAmounts;

using isr.Ports.Teleport;

namespace isr.Ports.D1000
{

    /// <summary> A base class for the D1000 modules. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-10 </para>
    /// </remarks>
    public abstract class D1000Base : Scribe
    {

        #region " CONSTRUCTION "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        protected D1000Base() : base()
        {
            this.Messenger.Transport.PopulateCommands( D1000CommandCollection.Get() );
            this.Messenger.OutputMessage = ProtocolMessage.Create();
            this.Messenger.InputMessage = ProtocolMessage.Create();
            this.Messenger.Transport.TransportProtocolMessage = ProtocolMessage.Create();
            this.Messenger.Transport.SentProtocolMessage = ProtocolMessage.Create();
            this.Messenger.Transport.ReceivedProtocolMessage = ProtocolMessage.Create();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PRESETTABLE "

        /// <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
        /// <remarks> Use this to customize the reset. </remarks>
        public virtual void InitKnownState()
        {
        }

        /// <summary> Initializes the Device. Used after reset to set a desired initial state. </summary>
        /// <remarks> Use this to customize the reset. </remarks>
        /// <param name="moduleName"> The name of the module. </param>
        public void InitKnownState( string moduleName )
        {
            this.ModuleName = moduleName;
            _ = Serial.TraceMethods.TraceInformation( $"{this.ModuleName}.{this.Messenger.ModuleAddress} initializing known state" );
            this.AnalogInputPayload = new RealValuePayload() { Unit = Arebis.StandardUnits.ElectricUnits.Volt };
            this.ReadSetupPayload = new SetupPayload();
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Turnaround time. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command ASCII. </param>
        /// <returns> A timespan. </returns>
        public TimeSpan TurnaroundTime( string commandAscii )
        {
            return this.Messenger.Transport.SelectCommand( commandAscii ).TurnaroundTime;
        }

        /// <summary> Converts a commandAscii to a command code. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandAscii"> The command ASCII. </param>
        /// <returns> CommandAscii as a D1000CommandCode. </returns>
        public D1000CommandCode ToCommandCode( string commandAscii )
        {
            return ToCommandCode( this.Messenger.Transport.SelectCommand( commandAscii ).CommandCode );
        }

        /// <summary> Converts a commandAscii to a command code. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> CommandAscii as a D1000CommandCode. </returns>
        public static D1000CommandCode ToCommandCode( int value )
        {
            return ( D1000CommandCode ) value;
        }


        #endregion

        #region " MODULE INFO "

        /// <summary> Name of the module. </summary>
        private string _ModuleName;

        /// <summary> Gets or sets the name of the module. </summary>
        /// <value> The name of the module. </value>
        public string ModuleName
        {
            get => this._ModuleName;

            set {
                if ( !string.Equals( this.ModuleName, value ) )
                {
                    this._ModuleName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " ANALOG INPUT "

        /// <summary> The analog input read. </summary>
        private Arebis.UnitsAmounts.Amount _AnalogInputRead;

        /// <summary> Gets or sets the Analog Input. </summary>
        /// <value> The Input Error read. </value>
        public Amount AnalogInputRead
        {
            get => this._AnalogInputRead;

            protected set {
                if ( value != this.AnalogInputRead )
                {
                    this._AnalogInputRead = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the analog input payload. </summary>
        /// <value> The analog input payload. </value>
        public RealValuePayload AnalogInputPayload { get; private set; }

        /// <summary> Reads Analog Input. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The Analog Input. </returns>
        public Amount ReadAnalogInput()
        {
            string activity = "building read analog input message";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.CommandPrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) D1000CommandCode.ReadData].CommandAscii;
                activity = "reading analog input";
                _ = this.Query( message, TimeSpan.FromSeconds( 0.01d ) );
            }

            if ( this.Transport.IsSuccess() )
            {
                _ = this.AnalogInputPayload.Parse( this.Transport.ReceivedProtocolMessage.Payload );
                this.AnalogInputRead = this.AnalogInputPayload.Amount;
            }
            else
            {
                this.AnalogInputPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? Serial.TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : Serial.TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.AnalogInputRead;
        }

        /// <summary> Reply analog input. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        public StatusCode ReplyAnalogInput()
        {
            string activity = "building analog input reply";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.ResponsePrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) D1000CommandCode.ReadData].CommandAscii;
                message.Payload = this.AnalogInputPayload.SimulatedPayload;
                activity = "replying analog input";
                _ = this.Query( message, TimeSpan.FromSeconds( 0.01d ) );
            }

            if ( this.Transport.IsSuccess() )
            {
            }
            else
            {
                this.AnalogInputPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? Serial.TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : Serial.TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        #endregion

        #region " WRITE ENABLE "

        /// <summary> Enables the write. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <returns> A StatusCode. </returns>
        public StatusCode EnableWrite()
        {
            string activity = "building read analog input message";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.CommandPrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) D1000CommandCode.WriteEnable].CommandAscii;
                activity = "applying and reading back write enable";
                _ = this.Query( message, TimeSpan.FromSeconds( 0.01d ) );
            }

            _ = this.Transport.IsSuccess()
                ? Serial.TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : Serial.TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        /// <summary> Reply enable write. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <returns> A StatusCode. </returns>
        public StatusCode ReplyEnableWrite()
        {
            string activity = "building write enable reply";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.ResponsePrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) D1000CommandCode.WriteEnable].CommandAscii;
                activity = "replying write enable";
                _ = this.Query( message, TimeSpan.FromSeconds( 0.01d ) );
            }

            _ = this.Transport.IsSuccess()
                ? Serial.TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : Serial.TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        #endregion

        #region " MODULE SETUP: READ "

        /// <summary> Gets or sets the module setup payload that was read. </summary>
        /// <value> The module setup payload. </value>
        public SetupPayload ReadSetupPayload { get; private set; }

        /// <summary> Reads module setup. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The status. </returns>
        public StatusCode ReadModuleSetup()
        {
            string activity = "building module setup read message";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.CommandPrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) D1000CommandCode.ReadSetup].CommandAscii;
                activity = "reading setup";
                _ = this.Query( message, TimeSpan.FromSeconds( 0.01d ) );
            }

            if ( this.Transport.IsSuccess() )
            {
                _ = this.ReadSetupPayload.Parse( this.Transport.ReceivedProtocolMessage.Payload );
            }
            else
            {
                this.ReadSetupPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? Serial.TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : Serial.TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        /// <summary> Reply read module setup. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        public StatusCode ReplyReadModuleSetup()
        {
            string activity = "building read module setup read reply";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.ResponsePrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) D1000CommandCode.ReadSetup].CommandAscii;
                message.Payload = SetupPayload.SimulatePayload( this.Messenger.ModuleAddress.ElementAtOrDefault( 0 ) ).Payload;
                activity = "replying read module setup";
                _ = this.Query( message, TimeSpan.FromSeconds( 0.01d ) );
            }

            if ( this.Transport.IsSuccess() )
            {
            }
            else
            {
                this.ReadSetupPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? Serial.TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : Serial.TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        #endregion

        #region " MODULE SETUP: WRITE "

        /// <summary> Gets or sets the module setup payload that was written. </summary>
        /// <value> The module setup payload. </value>
        public SetupPayload WrittenSetupPayload { get; private set; }

        /// <summary> Writes module setup. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The status. </returns>
        public StatusCode WriteModuleSetup()
        {
            string activity = "building module setup write message";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.CommandPrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) D1000CommandCode.SetupModule].CommandAscii;
                activity = "Writing setup";
                _ = this.Query( message, TimeSpan.FromSeconds( 0.01d ) );
            }

            if ( this.Transport.IsSuccess() )
            {
                _ = this.WrittenSetupPayload.Parse( this.Transport.ReceivedProtocolMessage.Payload );
            }
            else
            {
                this.WrittenSetupPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? Serial.TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : Serial.TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        /// <summary> Reply Write module setup. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A StatusCode. </returns>
        public StatusCode ReplyWriteModuleSetup()
        {
            string activity = "building write module setup reply";
            using ( IProtocolMessage message = new ProtocolMessage() )
            {
                message.Prefix = ProtocolMessage.ResponsePrompt;
                message.ModuleAddress = this.Messenger.ModuleAddress;
                message.CommandAscii = this.Messenger.Transport.Commands[( int ) D1000CommandCode.SetupModule].CommandAscii;
                message.Payload = SetupPayload.SimulatePayload( this.Messenger.ModuleAddress.ElementAtOrDefault( 0 ) ).Payload;
                activity = "replying write module setup";
                _ = this.Query( message, TimeSpan.FromSeconds( 0.01d ) );
            }

            if ( this.Transport.IsSuccess() )
            {
            }
            else
            {
                this.WrittenSetupPayload.StatusCode = this.Transport.ReceiveStatus != StatusCode.Okay
                    ? this.Transport.ReceiveStatus
                    : this.Transport.SendStatus != StatusCode.Okay
                                    ? this.Transport.SendStatus
                                    : this.Transport.TransportStatus != StatusCode.Okay ? this.Transport.TransportStatus : StatusCode.ValueNotSet;
            }

            _ = this.Transport.IsSuccess()
                ? Serial.TraceMethods.TraceInformation( $"Success {activity}: {this.Transport.ReceivedHexMessage};. " )
                : Serial.TraceMethods.TraceWarning( $"{this.Transport.FailureMessage()} error {activity};. " );

            return this.Transport.TransportStatus;
        }

        #endregion

        #region " COLLECTOR (listener) PROCESSING "

        /// <summary> Listener message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Protocol event information. </param>
        protected void ListenerMessageReceived( ProtocolEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            switch ( this.ToCommandCode( e.ProtocolMessage.CommandAscii ) )
            {
                case D1000CommandCode.ReadData:
                    {
                        _ = this.ReplyAnalogInput();
                        break;
                    }

                case D1000CommandCode.WriteEnable:
                    {
                        _ = this.ReplyEnableWrite();
                        break;
                    }

                case D1000CommandCode.ReadSetup:
                    {
                        _ = this.ReplyReadModuleSetup();
                        break;
                    }

                case D1000CommandCode.SetupModule:
                    {
                        _ = this.ReplyWriteModuleSetup();
                        break;
                    }
            }
        }

        /// <summary> Notifies a message received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Protocol event information. </param>
        protected override void NotifyMessageReceived( ProtocolEventArgs e )
        {
            base.NotifyMessageReceived( e );
            if ( this.Messenger.MessengerRole == MessengerRole.Collector && this.Transport.IsSuccess() )
            {
                this.ListenerMessageReceived( e );
            }
        }

        #endregion

    }
}
