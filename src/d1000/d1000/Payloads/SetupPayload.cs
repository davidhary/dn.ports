using System.Collections.Generic;
using System.Linq;

using isr.Ports.Serial;
using isr.Ports.Serial.PayloadExtensions;
using isr.Ports.Teleport;

namespace isr.Ports.D1000
{

    /// <summary> A payload consisting of the setup information. </summary>
    /// <remarks> David, 2020-10-22. </remarks>
    public class SetupPayload : IPayload
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public SetupPayload() : base()
        {
        }

        #endregion

        #region " SIMULATE "

        /// <summary> Simulate payload. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="moduleAddress"> The module address. </param>
        /// <returns> A SetupPayload. </returns>
        public static SetupPayload SimulatePayload( byte moduleAddress )
        {
            var result = new SetupPayload() {
                ModuleAddress = moduleAddress,
                LinefeedEnabled = false,
                ParityEnabled = false,
                Parity = System.IO.Ports.Parity.Even,
                BaudRate = 9600
            };
            return result;
        }

        #endregion

        #region " VALUES "

        /// <summary> Gets or sets the module address. </summary>
        /// <value> The module address. </value>
        public byte ModuleAddress { get; set; }

        /// <summary> Gets or sets the module address. </summary>
        /// <value> The module address. </value>
        public string ModuleAddressPayload
        {
            get => this.ModuleAddress.ToHex();

            set => this.ModuleAddress = byte.Parse( value, System.Globalization.NumberStyles.HexNumber );
        }

        /// <summary> Gets or sets the module address packed nibbles. </summary>
        /// <value> The module address packed nibbles. </value>
        public PackedNibblesInt8 ModuleAddressNibbles
        {
            get {
                var result = new PackedNibblesInt8( this.ModuleAddress );
                return result;
            }

            set {
                if ( value is object )
                    this.ModuleAddress = value.PackedValue.Value;
            }
        }

        /// <summary> Gets or sets the line feeds bit. </summary>
        /// <value> The line feeds bit. </value>
        public byte LinefeedsBit { get; set; }

        /// <summary> Gets or sets the linefeed enabled. </summary>
        /// <value> The linefeed enabled. </value>
        public bool LinefeedEnabled
        {
            get => this.LinefeedsBit == 1;

            set => this.LinefeedsBit = ( byte ) (value ? 1 : 0);
        }

        /// <summary> Gets or sets the parity enabled bit. </summary>
        /// <value> The parity enabled bit. </value>
        public byte ParityEnabledBit { get; set; }

        /// <summary> Gets or sets the parity enabled. </summary>
        /// <value> The parity enabled. </value>
        public bool ParityEnabled
        {
            get => this.ParityEnabledBit == 1;

            set => this.ParityEnabledBit = ( byte ) (value ? 1 : 0);
        }

        /// <summary> Gets or sets the parity bit. </summary>
        /// <value> The parity bit. </value>
        public byte ParityBit { get; set; }

        /// <summary> Gets or sets the parity. </summary>
        /// <value> The parity. </value>
        public System.IO.Ports.Parity Parity
        {
            get => this.ParityBit == 1 ? System.IO.Ports.Parity.Odd : System.IO.Ports.Parity.Even;

            set => this.ParityBit = ( byte ) (value == System.IO.Ports.Parity.Even ? 0 : 1);
        }

        /// <summary> Gets or sets the zero-based index of the baud rate. </summary>
        /// <value> The baud rate index. </value>
        public byte BaudRateIndex { get; set; }

        /// <summary> Gets or sets the supported baud rates. </summary>
        /// <value> The supported baud rates. </value>
        private int[] SupportedBaudRates { get; set; } = new int[] { 300, 600, 1200, 2400, 4800, 9600, 19200, 38400 };

        /// <summary> Gets or sets the baud rate. </summary>
        /// <value> The baud rate. </value>
        public int BaudRate
        {
            get => this.SupportedBaudRates[this.BaudRateIndex - 7];

            set {
                byte i = 0;
                foreach ( int rate in this.SupportedBaudRates )
                {
                    if ( value <= rate )
                        break;
                    i += 1;
                }

                this.BaudRateIndex = ( byte ) (i + 7);
            }
        }

        /// <summary> Gets or sets the communication nibbles. </summary>
        /// <value> The communication nibbles. </value>
        public PackedNibblesInt8 CommunicationNibbles
        {
            get {
                var result = new PackedNibblesInt8( ( byte ) (this.LinefeedsBit << 7 | this.ParityBit << 6 | this.ParityEnabledBit << 5 | this.BaudRateIndex) );
                return result;
            }

            set {
                if ( value is object )
                {
                    this.LinefeedsBit = ( byte ) (1 & value.PackedValue.Value >> 7);
                    this.ParityBit = ( byte ) (1 & value.PackedValue.Value >> 6);
                    this.ParityEnabledBit = ( byte ) (1 & value.PackedValue.Value >> 5);
                    this.BaudRateIndex = ( byte ) (7 & value.PackedValue.Value);
                }
            }
        }

        /// <summary> Gets or sets the options nibbles. </summary>
        /// <value> The options nibbles. </value>
        public PackedNibblesInt8 OptionsNibbles { get; set; }

        /// <summary> Gets or sets the display, filtering and time constant nibbles. </summary>
        /// <value> The display nibbles. </value>
        public PackedNibblesInt8 DisplayNibbles { get; set; } = new PackedNibblesInt8( 0x82 );

        #endregion

        #region " PAYLOAD "

        /// <summary> The reading. </summary>
        private string _Reading;

        /// <summary> Gets or sets the reading. </summary>
        /// <value> The reading. </value>
        public string Reading
        {
            get => this._Reading;

            set {
                if ( !string.Equals( value, this.Reading ) )
                {
                    this._Reading = value;
                    this.Payload = value.ToHexBytes();
                }
            }
        }

        /// <summary> The payload. </summary>
        private List<byte> _Payload;

        /// <summary> Gets or sets the payload. </summary>
        /// <value> The payload. </value>
        public IEnumerable<byte> Payload
        {
            get {
                this._Payload = new List<byte>() { this.ModuleAddressNibbles.PackedValue.HighNibble, this.ModuleAddressNibbles.PackedValue.LowNibble, this.CommunicationNibbles.PackedValue.HighNibble, this.CommunicationNibbles.PackedValue.LowNibble, this.OptionsNibbles.PackedValue.HighNibble, this.OptionsNibbles.PackedValue.LowNibble, this.DisplayNibbles.PackedValue.HighNibble, this.DisplayNibbles.PackedValue.LowNibble };
                return this._Payload;
            }

            set {
                this._Payload = new List<byte>( value );
                if ( value?.Any() == true )
                {
                    this.Reading = value.ToHex();
                    var values = new Queue<byte>( value );
                    this.ModuleAddressNibbles = new PackedNibblesInt8( values.Dequeue(), values.Dequeue() );
                    this.CommunicationNibbles = new PackedNibblesInt8( values.Dequeue(), values.Dequeue() );
                    this.OptionsNibbles = new PackedNibblesInt8( values.Dequeue(), values.Dequeue() );
                    this.DisplayNibbles = new PackedNibblesInt8( values.Dequeue(), values.Dequeue() );
                }
                else
                {
                    this.Reading = string.Empty;
                }
            }
        }

        /// <summary> Gets or sets the status code. </summary>
        /// <value> The status code. </value>
        public StatusCode StatusCode { get; set; }

        #endregion

        #region " I PAYLOAD IMPLEMENTATION "

        /// <summary> Populates the value from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> A StatusCode. </returns>
        public StatusCode Parse( IEnumerable<byte> values )
        {
            this.Payload = values.ToHex().ToHexBytes();
            return this.StatusCode;
        }

        /// <summary> Returns the payload value in bytes. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> This object as an IEnumerable(Of Byte) </returns>
        public IEnumerable<byte> Build()
        {
            return this.Payload.ToHex().ToHexBytes();
        }

        #endregion

    }
}
