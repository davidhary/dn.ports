using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Ports.D1000.Properties
{
    /// <summary>   A settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( Settings ) )]
    public class Settings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public Settings() : base( System.Reflection.Assembly.GetAssembly( typeof( Settings ) ) )
        { }

        /// <summary>   (Immutable) The lazy instance. </summary>
        private static readonly Lazy<Settings> LazyInstance = new( () => new Settings() );

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static Settings Instance
        {
            get {
                if ( !LazyInstance.IsValueCreated )
                {
                    // This ensures that the settings is created if the application context settings file
                    // does not exist.
                    LazyInstance.Value.Initialize( new object[] { LazyInstance.Value } );
                }
                return LazyInstance.Value;
            }
        }

        /// <summary>   Saves this object. </summary>
        /// <remarks>   David, 2021-12-06. </remarks>
        public void Save()
        {
            this.SaveSettings( new object[] { this } );
        }

        /// <summary>   Name of the port. </summary>
        private string _PortName = "COM3";
        /// <summary>   Gets or sets the name of the Port. </summary>
        /// <value> The name of the Port. </value>
        [Description("The name of the serial port")]
        public string PortName
        {
            get => this._PortName;
            set {
                if ( !string.Equals( value, this.PortName ) )
                {
                    this._PortName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _ModuleConfigurationFileName = "";
        /// <summary>   Gets or sets the filename of the module configuration file. </summary>
        /// <value> The filename of the module configuration file. </value>
        [Description("The full file name of the module configuration file")]
        public string ModuleConfigurationFileName
        {
            get => this._ModuleConfigurationFileName;
            set {
                if ( !string.Equals( value, this.ModuleConfigurationFileName ) )
                {
                    this._ModuleConfigurationFileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _PortConfigurationFileName = "";
        /// <summary>   Gets or sets the filename of the port configuration file. </summary>
        /// <value> The filename of the port configuration file. </value>
        [Description( "The full file name of the serial port configuration file" )]
        public string PortConfigurationFileName
        {
            get => this._PortConfigurationFileName;
            set {
                if ( !string.Equals( value, this.PortConfigurationFileName ) )
                {
                    this._PortConfigurationFileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        private TimeSpan _RetryDelay = TimeSpan.FromMilliseconds(200);
        /// <summary>   Gets or sets the retry delay. </summary>
        /// <value> The retry delay. </value>
        [Description("The time to wait between retries")]
        public TimeSpan RetryDelay
        {
            get => this._RetryDelay;
            set {
                if ( !TimeSpan.Equals( value, this.RetryDelay ) )
                {
                    this._RetryDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _RetryCount = 3;
        /// <summary>   Gets or sets the number of retries. </summary>
        /// <value> The number of retries. </value>
        [Description("The number of time to retry a failed communication attempt before giving up")]
        public int RetryCount
        {
            get => this._RetryCount;
            set {
                if ( !int.Equals( value, this.RetryCount ) )
                {
                    this._RetryCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private TimeSpan _ModuleReadTimeout = TimeSpan.FromMilliseconds(4000);
        /// <summary>   Gets or sets the module read timeout. </summary>
        /// <value> The module read timeout. </value>
        [Description("The time to wait for the completion of a read command")]
        public TimeSpan ModuleReadTimeout
        {
            get => this._ModuleReadTimeout;
            set {
                if ( !TimeSpan.Equals( value, this.ModuleReadTimeout ) )
                {
                    this._ModuleReadTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _BaudRate = 9600;
        /// <summary>   Gets or sets the baud rate. </summary>
        /// <value> The baud rate. </value>
        [Description("Sets the serial port baud rate")]
        public int BaudRate
        {
            get => this._BaudRate;
            set {
                if ( !int.Equals( value, this.BaudRate ) )
                {
                    this._BaudRate = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _DataBits = 8;
        /// <summary>   Gets or sets the data bits. </summary>
        /// <value> The data bits. </value>
        [Description( "Sets the serial port data bits" )]
        public int DataBits
        {
            get => this._DataBits;
            set {
                if ( !int.Equals( value, this.DataBits ) )
                {
                    this._DataBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.IO.Ports.StopBits _StopBits = System.IO.Ports.StopBits.One;
        /// <summary>   Gets or sets the stop bits. </summary>
        /// <value> The stop bits. </value>
        [Description( "Sets the serial port stop bits" )]
        public System.IO.Ports.StopBits StopBits
        {
            get => this._StopBits;
            set {
                if ( !System.IO.Ports.StopBits.Equals( value, this.StopBits ) )
                {
                    this._StopBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private System.IO.Ports.Parity _Parity = System.IO.Ports.Parity.None;
        /// <summary>   Gets or sets the parity. </summary>
        /// <value> The parity. </value>
        [Description( "Sets the serial port parity" )]
        public System.IO.Ports.Parity Parity
        {
            get => this._Parity;
            set {
                if ( !System.IO.Ports.Parity.Equals( value, this.Parity ) )
                {
                    this._Parity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _ModuleAddress = "02";
        /// <summary>   Gets or sets the module address. </summary>
        /// <value> The module address. </value>
        [Description("Sets the module address")]
        public string ModuleAddress
        {
            get => this._ModuleAddress;
            set {
                if ( !String.Equals( value, this.ModuleAddress ) )
                {
                    this._ModuleAddress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

    }

}
