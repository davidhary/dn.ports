using System;

using isr.Ports.Serial.PayloadExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Ports.D1000.MSTest
{

    /// <summary> Tests the D1141 Module. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 1/26/2018 </para></remarks>
    [TestClass()]
    public class D1141Tests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );

            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( AppSettings.Instance.TestSiteSettings.Exists(), $"{nameof( AppSettings.Instance.TestSiteSettings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( AppSettings.Instance.TestSiteSettings.TimeZoneOffset() ) < expectedUpperLimit,
                           $"{nameof( AppSettings.Instance.TestSiteSettings.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( AppSettings.Instance.D1141Settings.Exists(), $"{nameof( AppSettings.Instance.D1141Settings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion


        #region " CHECK SUM TESTS "

        /// <summary> (Unit Test Method) tests checksum. </summary>
        /// <remarks> David, 2020-11-16. </remarks>
        [TestMethod()]
        public void ChecksumTest()
        {
            string payload = "#1DOFF";
            string expected = "73";
            string actual = payload.Checksum();
            Assert.AreEqual( expected, actual, $"Check sum of {payload}" );
        }

        /// <summary> (Unit Test Method) validates the checksum test. </summary>
        /// <remarks> David, 2020-11-16. </remarks>
        [TestMethod()]
        public void ValidateChecksumTest()
        {
            string payload = "*1RD+00072.10A4";
            string expected = "A4";
            string message = payload.Remove( payload.Length - 2, 2 );
            string expectedMessage = "*1RD+00072.10";
            Assert.AreEqual( expectedMessage, message, $"Stripped message" );
            string actual = message.Checksum();
            Assert.AreEqual( expected, actual, $"Validated check sum of {payload}" );
        }

        #endregion

    }
}
