using System;
using System.Reflection;

using isr.Ports.Serial.PayloadExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Ports.Serial.MSTest
{

    /// <summary> Tests the <see cref="isr.Ports.Serial.Port"/> port. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-26 </para>
    /// </remarks>
    [TestClass()]
    public class FrameworkTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );

            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( AppSettings.Instance.TestSiteSettings.Exists(), $"{nameof( AppSettings.Instance.TestSiteSettings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( AppSettings.Instance.TestSiteSettings.TimeZoneOffset() ) < expectedUpperLimit,
                           $"{nameof( AppSettings.Instance.TestSiteSettings.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( AppSettings.Instance.FrameworkSettings.Exists(), $"{nameof( AppSettings.Instance.FrameworkSettings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " EXTENSION: DELIMITER TESTS "

        /// <summary>   (Unit Test Method) delimiter should be inserted. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        [TestMethod()]
        public void DelimiterShouldBeInserted()
        {
            string value = "000003";
            int everyLength = 2;
            string delimiter = " ";
            string expected = "00 00 03";
            string actual;
            actual = value.InsertDelimiter( everyLength, delimiter );
            Assert.AreEqual( expected, actual );
            Assert.AreEqual( expected, value.InsertDelimiter( everyLength, delimiter ) );
        }

        [TestMethod()]
        public void DelimiterShouldBeRemoved()
        {
            string value = "00 00 03";
            string delimiter = " ";
            string expected = "000003";
            string actual;
            actual = value.RemoveDelimiter( delimiter );
            Assert.AreEqual( expected, actual );
            Assert.AreEqual( expected, value.RemoveDelimiter( delimiter ) );
        }

        /// <summary>   (Unit Test Method) delimiter should be inserted and removed. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        [TestMethod()]
        public void DelimiterShouldBeInsertedAndRemoved()
        {
            string value = "00 00 03";
            int everyLength = 2;
            string delimiter = " ";
            string expected = "00 00 03";
            string actual;
            actual = value.RemoveDelimiter( delimiter ).InsertDelimiter( everyLength, delimiter );
            Assert.AreEqual( expected, actual );
        }
        #endregion

        #region " CHECK SUM TESTS "

        /// <summary>   (Unit Test Method) checksum should match. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        [TestMethod()]
        public void ChecksumShouldMatch()
        {
            string payload = "#1DOFF";
            string expected = "73";
            string actual = payload.Checksum();
            Assert.AreEqual( expected, actual, $"Check sum of {payload}" );
        }

        /// <summary>   (Unit Test Method) checksum should validate. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        [TestMethod()]
        public void ChecksumShouldValidate()
        {
            string payload = "*1RD+00072.10A4";
            string expected = "A4";
            string message = payload.Remove( payload.Length - 2, 2 );
            string expectedMessage = "*1RD+00072.10";
            Assert.AreEqual( expectedMessage, message, $"Stripped message" );
            string actual = message.Checksum();
            Assert.AreEqual( expected, actual, $"Validated check sum of {payload}" );
        }

        #endregion

        #region " TRANSIT TIME "

        /// <summary>   (Unit Test Method) default character bit count should match. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        [TestMethod()]
        public void DefaultCharacterBitCountShouldMatch()
        {
            var dix = PortParametersDictionary.DefaultPortParameters();
            Assert.AreEqual( AppSettings.Instance.FrameworkSettings.DefaultCharacterBitCount, dix.CharacterBitCount( 1 ), "Bits per character" );
        }

        /// <summary>   (Unit Test Method) default minimum transit time should match. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        [TestMethod()]
        public void DefaultMinimumTransitTimeShouldMatch()
        {
            var dix = PortParametersDictionary.DefaultPortParameters();
            Asserts.Instance.AreEqual( AppSettings.Instance.FrameworkSettings.DefaultMinimumTransitTime, dix.MinimumTransitTimespan( 1 ),
                                    TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerMillisecond * 0.01d) ), "Minimum transit time" );
        }

        #endregion

    }
}
