using System;

using isr.Ports.Serial;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Ports.Serial.MSTest
{

    /// <summary> Tests the <see cref="isr.Ports.Serial.Port"/> port using a two port null model. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-26 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "NullModemTwoPorts" )]
    public class NullModemTwoPortTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( AppSettings.Instance.TestSiteSettings.Exists(), $"{nameof( AppSettings.Instance.TestSiteSettings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( AppSettings.Instance.TestSiteSettings.TimeZoneOffset() ) < expectedUpperLimit,
                           $"{nameof( AppSettings.Instance.TestSiteSettings.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( AppSettings.Instance.NullModemSettings.Exists(), $"{nameof( AppSettings.Instance.NullModemSettings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " NULL MODEM TESTS "

        /// <summary> Check connected port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> The port. </param>
        private static void CheckConnectedPort( IPort port )
        {
            Assert.AreEqual( true, port.IsOpen, $"Port {port.PortParameters.PortName} is open" );
        }

        /// <summary>   (Unit Test Method) ports should open send and receive. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        [TestMethod()]
        public void PortsShouldOpenSendAndReceive()
        {
            if ( !AppSettings.Instance.NullModemSettings.CheckPort1Exists() )
                Assert.Inconclusive( $"{AppSettings.Instance.NullModemSettings.Port1Name} not found" );
            using IPort port1 = Port.Create();
            try
            {
                (bool success, string details) = port1.TryOpen( AppSettings.Instance.NullModemSettings.Port1Name );
                Assert.IsTrue( success, $"Failed connecting {AppSettings.Instance.NullModemSettings.Port1Name}: {details}" );
                Assert.AreEqual( AppSettings.Instance.NullModemSettings.Port1Name, port1.SerialPort.PortName, $"Serial port name should match" );
                CheckConnectedPort( port1 );
            }
            catch
            {
                throw;
            }
            finally
            {
            }

            using IPort port2 = Port.Create();
            try
            {
                (bool success, string details) = port2.TryOpen( AppSettings.Instance.NullModemSettings.Port2Name );
                Assert.IsTrue( success, $"Failed connecting {AppSettings.Instance.NullModemSettings.Port2Name}: {details}" );
                Assert.AreEqual( AppSettings.Instance.NullModemSettings.Port2Name, port2.SerialPort.PortName, $"Serial port name should match" );
                CheckConnectedPort( port2 );
            }
            catch
            {
                throw;
            }
            finally
            {
            }

            string message = "ABC";
            _ = Port.SendAsciiData( port2, message );

            // wait for the data to go out
            _ = Port.Wait( port2.PortParameters.MinimumTransitTimespan( message.Length ) );
            string sentMessage = Port.Decode( port2.SentValues(), port2.SerialPort.Encoding );

            // wait for the characters to come in.
            _ = port1.TryWaitReceiveCount( message.Length, 10 );
            string receivedMessage = Port.Decode( port1.ReceivedValues(), port1.SerialPort.Encoding );
            Assert.AreEqual( message, sentMessage, $"sent message should match" );
            Assert.AreEqual( message, receivedMessage, $"received message should match" );
        }

        #endregion

    }
}
