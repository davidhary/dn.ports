using System;

namespace isr.Ports.Serial.MSTest
{

    /// <summary> A serial port framework settings. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [isr.Json.SettingsSection( nameof( FrameworkSettings ) )]
    internal class FrameworkSettings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public FrameworkSettings() : base( System.Reflection.Assembly.GetAssembly( typeof( FrameworkSettings ) ) )
        { }

        #region " TEST SITE CONFIGURATION "

        private bool _Verbose;
        /// <summary>   Gets or sets a value indicating whether test reporting is verbose. </summary>
        /// <value> True if verbose, false if not. </value>
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( value, this.Verbose ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled;
        /// <summary>   Gets or sets a value indicating whether testing is enabled. </summary>
        /// <value> True if enabled, false if not. </value>
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( value, this.Enabled ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _All;
        /// <summary>   Gets or sets a value indicating whether all testing is to be ran. </summary>
        /// <value> True if All, false if not. </value>
        public bool All
        {
            get => this._All;
            set {
                if ( !bool.Equals( value, this.All ) )
                {
                    this._All = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " FRAMEWORK INFORMATION "

        private TimeSpan _DefaultMinimumTransitTime = TimeSpan.FromMilliseconds( 1.04 );
        /// <summary> Gets or sets the default minimum transit time. </summary>
        /// <value> The default minimum transit time. </value>
        public TimeSpan DefaultMinimumTransitTime
        {
            get => this._DefaultMinimumTransitTime;
            set {
                if ( !TimeSpan.Equals( value, this.DefaultMinimumTransitTime ) )
                {
                    this._DefaultMinimumTransitTime = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private Double _DefaultCharacterBitCount = 10;
        /// <summary> Gets or sets the default character bit count. </summary>
        /// <value> The default character bit count. </value>
        public Double DefaultCharacterBitCount
        {
            get => this._DefaultCharacterBitCount;
            set {
                if ( !Double.Equals( value, this.DefaultCharacterBitCount ) )
                {
                    this._DefaultCharacterBitCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
