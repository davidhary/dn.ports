using isr.Ports.Serial;

namespace isr.Ports.Serial.MSTest
{

    /// <summary> A serial port null modem settings. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [isr.Json.SettingsSection( nameof( NullModemSettings ) )]
    internal class NullModemSettings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public NullModemSettings() : base( System.Reflection.Assembly.GetAssembly( typeof( NullModemSettings ) ) )
        { }

        #region " TEST SITE CONFIGURATION "

        private bool _Verbose;
        /// <summary>   Gets or sets a value indicating whether test reporting is verbose. </summary>
        /// <value> True if verbose, false if not. </value>
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( value, this.Verbose ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled;
        /// <summary>   Gets or sets a value indicating whether testing is enabled. </summary>
        /// <value> True if enabled, false if not. </value>
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( value, this.Enabled ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _All;
        /// <summary>   Gets or sets a value indicating whether all testing is to be ran. </summary>
        /// <value> True if All, false if not. </value>
        public bool All
        {
            get => this._All;
            set {
                if ( !bool.Equals( value, this.All ) )
                {
                    this._All = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " DEVICE INFORMATION "

        /// <summary> Queries if a given check port 1 exists. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool CheckPort1Exists()
        {
            return Port.PortExists( this.Port1Name );
        }

        private string _Port1Name;
        /// <summary> Gets or sets the name of the port 1. </summary>
        /// <value> The name of the port 1. </value>
        public string Port1Name
        {
            get => this._Port1Name;
            set {
                if ( !string.Equals( value, this.Port1Name ) )
                {
                    this._Port1Name = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Queries if a given check port 1 exists. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool CheckPort2Exists()
        {
            return Port.PortExists( this.Port2Name );
        }

        private string _Port2Name;
        /// <summary> Gets or sets the name of the port 2. </summary>
        /// <value> The name of the port 2. </value>
        public string Port2Name
        {
            get => this._Port2Name;
            set {
                if ( !string.Equals( value, this.Port2Name ) )
                {
                    this._Port2Name = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
