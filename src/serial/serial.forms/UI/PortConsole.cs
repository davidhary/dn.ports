using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using isr.Ports.Serial.EscapeSequencesExtensions;
using isr.Ports.Serial.PayloadExtensions;
using isr.Ports.Serial.Forms.UIExtensions;
using System.Reflection;

namespace isr.Ports.Serial.Forms
{

    /// <summary> Hosts the <see cref="IPort">port</see>. </summary>
    /// <remarks>
    /// Based on Extended Serial Port Windows Forms Sample
    /// http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37 <para>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public partial class PortConsole : UserControl
    {

        /// <summary>   Gets or sets a value indicating whether the initializing components. </summary>
        /// <value> True if initializing components, false if not. </value>
        private bool InitializingComponents { get; set; }

        /// <summary> Initializes a new instance of the <see cref="PortConsole" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
        public PortConsole( IPort port ) : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this._ErrorProvider = new ErrorProvider();
            this.InitializingComponents = false;
            Serial.Port.DoEventsAction = System.Windows.Forms.Application.DoEvents;
            this.BindPortThis( port );
            this.ToolStrip.Renderer = new CustomProfessionalRenderer();
            this.ToggleTransmissionRepeatControlEnabled( false );
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="PortConsole" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PortConsole() : this( new Port() )
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    if ( this.Port is object )
                    {
                        if ( this.IsPortOwner )
                            this.Port.Dispose();
                        this.Port = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " SHOW IN FORM "

        /// <summary> Shows the control in a mode-less form. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="formCaption">    The form caption. </param>
        public static void ShowModeless( string formCaption )
        {
            PortConsole control = null;
            System.Windows.Forms.Form f = null;
            try
            {
                f = new();
                control = new PortConsole();
                f.Text = formCaption;
                f.Controls.Add( control );
                control.Dock = DockStyle.Fill;
                f.Show();
            }
            catch
            {
                control?.Dispose();
                f?.Dispose();
                throw;
            }
        }

        /// <summary> Shows the control in a modal form. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="formCaption">    The form caption. </param>
        public static void ShowModal( string formCaption )
        {
            using var f = new System.Windows.Forms.Form();
            using var control = new PortConsole();
            f.Text = formCaption;
            f.Controls.Add( control );
            control.Dock = DockStyle.Fill;
            _ = f.ShowDialog();
        }

        /// <summary> Shows the simple read write console. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public static void ShowSimpleReadWriteDialog()
        {
            PortConsole.ShowModal( "Serial Port Dialog" );
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary> Handles the Load event of the form control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {
                this.OnFormShown();
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( ex.ToString(), "Exception loading the form" );
            }
        }

        /// <summary>
        /// Handles the ResizeEnd event of the form control. Adjusts the character counts.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Form_ResizeEnd( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                activity = "displaying the receive text box ruler";
                this.ReceiveTextBoxCharactersPerLine = this.ReceiveRulerLabel.DisplayRuler( this.ReceiveTextBox.Width, this.ReceiveTextBoxCharacterWidth, this.ReceiveDisplayOption );
                activity = "displaying the transmit text box ruler";
                this.TransmitTextBoxCharactersPerLine = this.TransmitRulerLabel.DisplayRuler( this.TransmitTextBox.Width, this.TransmitTextBoxCharacterWidth, this.TransmitDisplayOption );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Clears the selections. A workaround to prevent highlights. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ClearSelections()
        {
            this.PortNamesCombo.ComboBox.Select( 0, 0 );
            this.BaudRateCombo.ComboBox.Select( 0, 0 );
            this.DataBitsCombo.ComboBox.Select( 0, 0 );
            this.ParityCombo.ComboBox.Select( 0, 0 );
            this.StopBitsCombo.ComboBox.Select( 0, 0 );
            this.ReceiveDelayCombo.ComboBox.Select( 0, 0 );
            this.ReceiveThresholdCombo.ComboBox.Select( 0, 0 );
            this.TransmitDisplayOptionComboBox.ComboBox.Select( 0, 0 );
            this.TransmitEnterOptionComboBox.ComboBox.Select( 0, 0 );
            this.ReceiveDisplayOptionComboBox.ComboBox.Select( 0, 0 );
        }

        /// <summary> Messenger console visible changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MessengerConsole_VisibleChanged( object sender, EventArgs e )
        {
            this.ClearSelections();
        }

        /// <summary> Handles the display of the controls. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void OnFormShown()
        {

            var assembly = Assembly.GetExecutingAssembly();
            var versionInfo = FileVersionInfo.GetVersionInfo( assembly.Location );

            // set the form caption
            this.Text = versionInfo.ProductName;
            this.Text += $"Version {versionInfo.ProductMajorPart}.{versionInfo.ProductMinorPart:00}";
            this.StatusLabel.Text = "closed";
            this.TransmitTerminationComboBox.Text = Properties.Settings.Instance.Termination;
            this.TransmitEnterOption = ( DisplayOptions ) Properties.Settings.Instance.EnteredDataEncodingOption;
            this.TransmitDisplayOption = ( DisplayOptions ) Properties.Settings.Instance.TransmitDisplayOption;
            this.ClearTransmitBox();
            this.ReceiveDisplayOption = ( DisplayOptions ) Properties.Settings.Instance.ReceiveDisplayOption;
            this.ClearReceiveBox();
            this.SelectFontSize( Properties.Settings.Instance.FontSize );

            // list the port names.
            _ = this.ListPortNames();

            // clear the selections
            this.ClearSelections();
            this.OnConnectionChanged( this.Port.IsOpen );
        }

        #endregion

        #region " TRANSMIT MANAGEMENT "

        /// <summary> The delimiter. </summary>
        private const string _Delimiter = " ";

        /// <summary>
        /// Handles the Click event of the _TransmitCopyMenuItem control. Copies a transmit box selection.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void TransmitCopyMenuItem_Click( object sender, EventArgs e )
        {
            this.TransmitTextBox.Copy();
        }

        /// <summary>
        /// Handles the Click event of the _TransmitPasteMenuItem control. Pastes a transmit box
        /// selection.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void TransmitPasteMenuItem_Click( object sender, EventArgs e )
        {
            this.TransmitTextBox.Paste();
        }

        /// <summary>
        /// Handles the Click event of the _TransmitCutMenuItem control. Cuts the transmit text box item.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void TransmitCutMenuItem_Click( object sender, EventArgs e )
        {
            this.TransmitTextBox.Cut();
        }

        /// <summary>
        /// Handles the Click event of the _TransmitSendMenuItem control. Sends position of caret line
        /// from transmit text box.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitSendMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "setting transmission data from text line";
                int loc = this.TransmitTextBox.GetFirstCharIndexOfCurrentLine();
                int ln = this.TransmitTextBox.GetLineFromCharIndex( loc );
                if ( 0 <= ln && ln < this.TransmitTextBox.Lines.Length )
                {
                    activity = "transmitting text line";
                    this.SendData( this.TransmitTextBox.Lines[ln], this.EnterTransmitHexData );
                    activity = "all text sent";
                    this.TransmitMessageCombo.Text = string.Empty;
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Sends only selection in transmit box to com. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitSendSelectionMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "setting transmission data from selected text";
                if ( this.TransmitTextBox.SelectionLength > 0 )
                {
                    activity = "sending selected text";
                    this.SendData( this.TransmitTextBox.SelectedText, this.EnterTransmitHexData );
                    activity = "select text sent";
                    this.TransmitMessageCombo.Text = string.Empty;
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> fetches a line from the transmit box to the transmit message box. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Mouse event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitTextBox_MouseClick( object sender, MouseEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "fetching transmission data from text box";
                if ( sender is RichTextBox rb )
                {
                    int loc = rb.GetFirstCharIndexOfCurrentLine();
                    int ln = rb.GetLineFromCharIndex( loc );
                    if ( 0 <= ln && ln < rb.Lines.Length )
                    {
                        activity = "updating transmission combo box";
                        this.TransmitMessageCombo.Text = rb.Lines[ln];
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Clears the transmit box. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ClearTransmitBox()
        {
            string activity = string.Empty;
            try
            {
                activity = "clearing the Transmit text box";
                this.TransmitTextBox.Clear();
                activity = "displaying the Transmit text box ruler";
                this.TransmitTextBoxCharactersPerLine = this.TransmitRulerLabel.DisplayRuler( this.TransmitTextBox.Width, this.TransmitTextBoxCharacterWidth, this.TransmitDisplayOption );
                this.TransmitCount = 0;
                activity = "setting Transmit status image";
                this.TransmitStatusLabel.Image = this.IsPortOpen ? Properties.Resources.LedCornerOrange : Properties.Resources.LedCornerGray;
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>
        /// Handles the Click event of the _ClearTransmitBoxButton control. Clears the transmit box.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearTransmitBoxButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "clearing transmission text box";
                this.ClearTransmitBox();
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Build hex string in the transmit box. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TransmitMessageCombo_TextUpdate( object sender, EventArgs e )
        {
            if ( this.HasNewHexCharacter && this.EnterTransmitHexData )
            {
                if ( sender is ToolStripComboBox cb )
                {
                    cb.Text = cb.Text.ToByteFormatted( _Delimiter );
                    cb.SelectionStart = cb.Text.Length;
                }
            }
        }

        /// <summary> Gets or sets the transmit enter option. </summary>
        /// <value> The transmit enter option. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public DisplayOptions TransmitEnterOption
        {
            get => ( DisplayOptions ) (this.TransmitEnterOptionComboBox.SelectedIndex + 1);

            set {
                if ( value != this.TransmitEnterOption )
                {
                    this.TransmitEnterOptionComboBox.SelectedIndex = ( int ) value - 1;
                }
            }
        }

        /// <summary> Gets information describing the enter transmit hexadecimal. </summary>
        /// <value> Information describing the enter transmit hexadecimal. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool EnterTransmitHexData => DisplayOptions.Hex == (this.TransmitEnterOption & DisplayOptions.Hex);

        /// <summary> Gets or sets the has new hexadecimal character. </summary>
        /// <value> The has new hexadecimal character. </value>
        private bool HasNewHexCharacter { get; set; }

        /// <summary>
        /// Event handler to suppress ding sound on enter. Called by _LotNumberTextBox for key down
        /// events.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SuppressDingHandler( object sender, KeyEventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                activity = "handling transmit message combo down";
                if ( e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return )
                {
                    // this suppresses the bell.
                    e.Handled = true;
                    // do not suppress the key press, just the key bell.
                    e.SuppressKeyPress = false;
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>
        /// Handles the KeyPress event of the cboEnterMessage control. Enter only allowed keys in hex
        /// mode.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="KeyPressEventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitMessageCombo_KeyPress( object sender, KeyPressEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.HasNewHexCharacter = false;
                if ( Environment.NewLine.First() == e.KeyChar )
                {
                    activity = $"sending {this.TransmitMessageCombo.Text}";
                    _ = TraceMethods.TraceInformation( activity );
                    this.SendData( this.TransmitMessageCombo.Text, this.EnterTransmitHexData );
                    this.TransmitMessageCombo.Text = string.Empty;
                    e.Handled = true;
                }
                else if ( this.EnterTransmitHexData )
                {
                    activity = "building message";
                    this.HasNewHexCharacter = e.KeyChar.IsHexadecimal();
                    if ( char.IsControl( e.KeyChar ) )
                    {
                    }
                    else if ( char.IsLetterOrDigit( e.KeyChar ) && !e.KeyChar.IsHexadecimal() )
                    {
                        e.Handled = true;
                    }
                    else if ( char.IsPunctuation( e.KeyChar ) )
                    {
                        e.Handled = true;
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>
        /// Handles the CheckedChanged event of the _TransmitShowHEXToolStripMenuItem control. Toggles
        /// showing the transmit data in hex.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TransmitDisplayOptionComboBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                activity = "displaying the transmit text box ruler";
                this.TransmitTextBoxCharactersPerLine = this.TransmitRulerLabel.DisplayRuler( this.TransmitTextBox.Width, this.TransmitTextBoxCharacterWidth, this.TransmitDisplayOption );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Gets or sets the transmit display option. </summary>
        /// <value> The transmit display option. </value>
        public DisplayOptions TransmitDisplayOption
        {
            get => ( DisplayOptions ) (this.TransmitDisplayOptionComboBox.SelectedIndex + 1);

            set => this.TransmitDisplayOptionComboBox.SelectedIndex = ( int ) value - 1;
        }

        /// <summary> Gets information describing the display transmit hexadecimal. </summary>
        /// <value> Information describing the display transmit hexadecimal. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool DisplayTransmitHexData => DisplayOptions.Hex == (this.TransmitDisplayOption & DisplayOptions.Hex);

        /// <summary> Number of transmits. </summary>
        private int _TransmitCount;

        /// <summary> Gets or sets the transmit count. </summary>
        /// <value> The transmit count. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int TransmitCount
        {
            get => this._TransmitCount;

            set {
                this._TransmitCount = value;
                this.TransmitCountLabel.Text = $"{this.TransmitCount:D6}";
            }
        }

        /// <summary> Sends the data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="textData">  The text data. </param>
        /// <param name="isHexData"> True if is hexadecimal data, false if not. </param>
        public void SendData( string textData, bool isHexData )
        {
            if ( string.IsNullOrWhiteSpace( textData ) )
            {
            }
            else if ( !this.IsPortOpen )
            {
                throw new InvalidOperationException( "Port not connected" );
            }
            else
            {
                var data = new List<byte>();
                if ( isHexData )
                {
                    data.AddRange( textData.RemoveDelimiter( _Delimiter ).ToHexBytes() );
                    if ( data[0] == PayloadExtensionsMethods.ParseErrorValue )
                    {
                        string message = $"failed converting '{textData}'; '{( char ) data[1]}' is not a valid HEX character";
                        _ = TraceMethods.TraceWarning( message );
                        this.OnEventHandlerError( new InvalidOperationException( message ) );
                    }

                }
                else
                {
                    data.AddRange( Encoding.ASCII.GetBytes( textData ) );
                }

                data.AddRange( this.TransmitTerminationComboBox.Text.CommonEscapeValues() );
                this.Port.SendData( data );
            }
            // Store the data in the transmit combo box.
            _ = this.TransmitMessageCombo.Items.Add( textData );
        }

        #endregion

        #region " RECEIVE HANDLERS "

        /// <summary> Number of receives. </summary>
        private int _ReceiveCount;

        /// <summary> Gets or sets the Receive count. </summary>
        /// <value> The Receive count. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int ReceiveCount
        {
            get => this._ReceiveCount;

            set {
                this._ReceiveCount = value;
                this.ReceiveCountLabel.Text = $"{this.ReceiveCount:D6}";
            }
        }

        /// <summary> Clears the transmit box. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ClearReceiveBox()
        {
            string activity = string.Empty;
            try
            {
                activity = "clearing the receive text box";
                this.ReceiveTextBox.Clear();
                activity = "displaying the receive text box ruler";
                this.ReceiveTextBoxCharactersPerLine = this.ReceiveRulerLabel.DisplayRuler( this.ReceiveTextBox.Width, this.ReceiveTextBoxCharacterWidth, this.ReceiveDisplayOption );
                this.ReceiveCount = 0;
                activity = "setting receive status image";
                this.ReceiveStatusLabel.Image = this.IsPortOpen ? Properties.Resources.LedCornerOrange : Properties.Resources.LedCornerGray;
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>
        /// Handles the Click event of the _ClearReceiveBoxButton control. clear receive box.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        private void ClearReceiveBoxButton_Click( object sender, EventArgs e )
        {
            this.ClearReceiveBox();
        }

        /// <summary> Gets or sets the receive display option. </summary>
        /// <value> The receive display option. </value>
        public DisplayOptions ReceiveDisplayOption
        {
            get => ( DisplayOptions ) (this.ReceiveDisplayOptionComboBox.SelectedIndex + 1);

            set => this.ReceiveDisplayOptionComboBox.SelectedIndex = ( int ) value - 1;
        }

        /// <summary> Gets information describing the display receive hexadecimal. </summary>
        /// <value> Information describing the display receive hexadecimal. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool DisplayReceiveHexData => DisplayOptions.Hex == (this.ReceiveDisplayOption & DisplayOptions.Hex);

        /// <summary> Receive display option combo box index changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReceiveDisplayOptionComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                activity = "displaying the receive text box ruler";
                this.ReceiveTextBoxCharactersPerLine = this.ReceiveRulerLabel.DisplayRuler( this.ReceiveTextBox.Width, this.ReceiveTextBoxCharacterWidth, this.ReceiveDisplayOption );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>
        /// Handles the Click event of the _SaveReceiveTextBoxButton control. Save receive box to file.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SaveReceiveTextBoxButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "instantiating the save received data dialog";
                using var saveFileDialog = new System.Windows.Forms.SaveFileDialog {
                    DefaultExt = "*.TXT",
                    Filter = "Text files (*.txt)|*.txt"
                };
                activity = "showing the save received data dialog";
                if ( saveFileDialog.ShowDialog() == DialogResult.OK )
                {
                    string fullpath = saveFileDialog.FileName;
                    activity = $"saving received data to {fullpath}";
                    this.ReceiveTextBox.SaveFile( fullpath, RichTextBoxStreamType.PlainText );
                    this.StatusLabel.Text = System.IO.Path.GetFileName( fullpath ) + " written";
                }
                else
                {
                    this.StatusLabel.Text = "no data chosen";
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>
        /// Handles the Click event of the _LoadTransmitFileButton control. load file into transmit box.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LoadTransmitFileButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "instantiating open dialog for transmit data";
                using var openFileDialog = new System.Windows.Forms.OpenFileDialog {
                    DefaultExt = "*.TXT",
                    Filter = "Text files (*.txt)|*.txt"
                };
                activity = "opening transmit data file dialog";
                if ( openFileDialog.ShowDialog() == DialogResult.OK )
                {
                    string fullpath = openFileDialog.FileName;
                    this.TransmitTextBox.Clear();
                    activity = $"loading transmit data from {fullpath}";
                    this.TransmitTextBox.LoadFile( fullpath, RichTextBoxStreamType.PlainText );
                }
                else
                {
                    this.StatusLabel.Text = "no data chosen";
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        #endregion

        #region " CONNECTION MANAGEMENT "

        /// <summary> Handles the Click event of the _ConnectButton control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConnectButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "handing connect button click";
                this.OnConnecting( this.IsPortOpen );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Applies the port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A PortParametersDictionary. </returns>
        private PortParametersDictionary BuildPortParamters()
        {
            var result = this.Port.ToPortParameters();
            result.PortName = this.PortNamesCombo.Text;
            result.BaudRate = int.Parse( this.BaudRateCombo.Text );
            result.DataBits = int.Parse( this.DataBitsCombo.Text );
            result.Parity = PortParametersDictionary.ParseParity( this.ParityCombo.Text );
            result.StopBits = PortParametersDictionary.ParseStopBits( this.StopBitsCombo.Text );
            result.ReceiveDelay = int.Parse( this.ReceiveDelayCombo.Text );
            result.ReceivedBytesThreshold = int.Parse( this.ReceiveThresholdCombo.Text );
            return result;
        }

        /// <summary> Process connecting. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="isConnected"> if set to <c>True</c> [is connected]. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnConnecting( bool isConnected )
        {
            (bool success, string details) outcome = (true, string.Empty);
            string activity = string.Empty;
            this._ErrorProvider.Clear();
            try
            {
                if ( isConnected )
                {
                    if ( this.Port is object )
                    {
                        activity = $"disconnecting {this.Port.SerialPort?.PortName}";
                        outcome = this.Port.TryClose();
                    }
                }
                else
                {
                    activity = $"connecting {this.PortNamesCombo.Text}";
                    if ( this.Port is null )
                    {
                        _ = TraceMethods.TraceWarning( $"Failed {activity}; Port class is nothing" );
                    }
                    else
                    {
                        activity = $"building port parameters for {this.PortNamesCombo.Text}";
                        activity = $"connecting {this.PortNamesCombo.Text}";
                        outcome = this.Port.TryOpen( this.BuildPortParamters() );
                    }
                }

                if ( !outcome.success )
                {
                    _ = TraceMethods.TraceWarning( outcome.details );
                    this.OnEventHandlerError( new InvalidOperationException( outcome.details ) );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Handles the change of connection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="isConnected"> if set to <c>True</c> [is connected]. </param>
        private void OnConnectionChanged( bool isConnected )
        {
            this._ErrorProvider.Clear();
            if ( isConnected )
            {
                this.ParityCombo.Enabled = false;
                this.StopBitsCombo.Enabled = false;
                this.PortNamesCombo.Enabled = false;
                this.BaudRateCombo.Enabled = false;
                this.DataBitsCombo.Enabled = false;
                this.ReceiveDelayCombo.Enabled = false;
                this.ReceiveThresholdCombo.Enabled = false;
                this.ConnectButton.Image = Properties.Resources.LedCornerGreen;
                this.ReceiveStatusLabel.Image = Properties.Resources.LedCornerOrange;
                this.TransmitStatusLabel.Image = Properties.Resources.LedCornerOrange;
                if ( this.Port is object )
                    this.ShowPortParameters( this.Port.ToPortParameters() );
            }
            else
            {
                this.ParityCombo.Enabled = true;
                this.StopBitsCombo.Enabled = true;
                this.PortNamesCombo.Enabled = true;
                this.BaudRateCombo.Enabled = true;
                this.DataBitsCombo.Enabled = true;
                this.ReceiveDelayCombo.Enabled = true;
                this.ReceiveThresholdCombo.Enabled = true;
                this.ConnectButton.Image = Properties.Resources.LedCornerGray;
                this.ReceiveStatusLabel.Image = Properties.Resources.LedCornerGray;
                this.TransmitStatusLabel.Image = Properties.Resources.LedCornerGray;
                this.StatusLabel.Text = "Disconnected";
            }
        }

        #endregion

        #region " PORT PARAMETERS FILE MANAGEMENT "

        /// <summary> load configuration. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LoadConfigButton_Click( object sender, EventArgs e )
        {
            string activity = $"loading port parameters from file";
            try
            {
                var (Success, Details) = this.Port.TryRestorePortParameters();
                if ( !Success )
                {
                    _ = TraceMethods.TraceWarning( Details );
                    this.OnEventHandlerError( new InvalidOperationException( Details ) );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> save port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SaveConfigButton_Click( object sender, EventArgs e )
        {
            string activity = $"saving port parameters to file";
            try
            {
                var (Success, Details) = this.Port.TryStorePortParameters();
                if ( !Success )
                {
                    _ = TraceMethods.TraceWarning( Details );
                    this.OnEventHandlerError( new InvalidOperationException( Details ) );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }


        #endregion

        #region " COM PORT MANAGEMENT "

        /// <summary> Gets the port. </summary>
        /// <value> The port. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public IPort Port { get; private set; }

        /// <summary> Gets the is port that owns this item. </summary>
        /// <value> The is port owner. </value>
        private bool IsPortOwner { get; set; }

        /// <summary> Bind port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
        public void BindPort( IPort port )
        {
            this.BindPortThis( port );
            this.IsPortOwner = false;
        }

        /// <summary> Bind port this. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
        private void BindPortThis( IPort port )
        {
            if ( this.Port is object )
            {
                this.Port.PropertyChanged -= this.PortPropertyChanged;
                this.Port.ConnectionChanged -= this.Port_ConnectionChanged;
                this.Port.DataReceived -= this.Port_DataReceived;
                this.Port.DataSent -= this.Port_DataSent;
                this.Port.SerialPortErrorReceived -= this.Port_SerialPortErrorReceived;
                this.Port.Timeout -= this.Port_Timeout;
                if ( this.IsPortOwner )
                    this.Port.Dispose();
                this.Port = null;
            }

            this.Port = port;
            if ( this.Port is null )
            {
                this.OnConnectionChanged( this.IsPortOpen );
            }
            else
            {
                this.Port.PropertyChanged += this.PortPropertyChanged;
                this.Port.ConnectionChanged += this.Port_ConnectionChanged;
                this.Port.DataReceived += this.Port_DataReceived;
                this.Port.DataSent += this.Port_DataSent;
                this.Port.SerialPortErrorReceived += this.Port_SerialPortErrorReceived;
                this.Port.Timeout += this.Port_Timeout;
                this.HandlePropertyChanged( this.Port, nameof( IPort.IsOpen ) );
                this.HandlePropertyChanged( this.Port, nameof( IPort.PortParametersFileName ) );
                this.HandlePropertyChanged( this.Port, nameof( IPort.SupportedBaudRates ) );
                this.HandlePropertyChanged( this.Port, nameof( IPort.InputBufferingOption ) );
            }

            this.IsPortOwner = true;
            this.BindPortParameters( port );
        }

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( IPort sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( IPort.InputBufferingOption ):
                    {
                        break;
                    }

                case nameof( IPort.IsOpen ):
                    {
                        this.OnConnectionChanged( sender.IsOpen );
                        break;
                    }

                case nameof( IPort.SupportedBaudRates ):
                    {
                        this.BaudRateCombo.ComboBox.DataSource = null;
                        this.BaudRateCombo.Items.Clear();
                        this.BaudRateCombo.ComboBox.DataSource = sender.SupportedBaudRates;
                        break;
                    }
            }
        }

        /// <summary> Port property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PortPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( IPort.PropertyChanged )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.PortPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as IPort, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Gets the is port open. </summary>
        /// <value> The is port open. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsPortOpen => this.Port is object && this.Port.IsOpen;

        /// <summary> Gets or sets options for controlling the port. </summary>
        /// <value> Options that control the port. </value>
        private PortParametersDictionary PortParameters { get; set; }

        /// <summary> Bind port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> Specifies the <see cref="IPort">port</see>. </param>
        private void BindPortParameters( IPort port )
        {
            if ( this.PortParameters is object )
            {
                this.PortParameters.PropertyChanged -= this.PortPropertyChanged;
                this.PortParameters = null;
            }

            if ( port is object )
            {
                this.PortParameters = port.PortParameters;
                this.PortParameters.PropertyChanged += this.PortParametersPropertyChanged;
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.BaudRate ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.DataBits ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.Handshake ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.Parity ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.PortName ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.ReadBufferSize ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.ReadTimeout ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.ReceivedBytesThreshold ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.ReceiveDelay ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.RtsEnable ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.StopBits ) );
                this.HandlePropertyChanged( this.PortParameters, nameof( PortParametersDictionary.WriteBufferSize ) );
            }

            this.IsPortOwner = true;
        }

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( PortParametersDictionary sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( PortParametersDictionary.BaudRate ):
                    {
                        this.BaudRateCombo.Text = sender.BaudRate.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.DataBits ):
                    {
                        this.DataBitsCombo.Text = sender.DataBits.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.Handshake ):
                    {
                        break;
                    }

                case nameof( PortParametersDictionary.Parity ):
                    {
                        this.ParityCombo.Text = sender.Parity.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.PortName ):
                    {
                        this.PortNamesCombo.Text = sender.PortName;
                        break;
                    }

                case nameof( PortParametersDictionary.ReadBufferSize ):
                    {
                        break;
                    }

                case nameof( PortParametersDictionary.ReadTimeout ):
                    {
                        break;
                    }

                case nameof( PortParametersDictionary.ReceivedBytesThreshold ):
                    {
                        this.ReceiveThresholdCombo.Text = sender.ReceivedBytesThreshold.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.ReceiveDelay ):
                    {
                        this.ReceiveDelayCombo.Text = sender.ReceiveDelay.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.RtsEnable ):
                    {
                        break;
                    }

                case nameof( PortParametersDictionary.StopBits ):
                    {
                        this.StopBitsCombo.Text = sender.StopBits.ToString();
                        break;
                    }

                case nameof( PortParametersDictionary.WriteBufferSize ):
                    {
                        break;
                    }
            }
        }

        /// <summary> Port parameters property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PortParametersPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( PortParametersDictionary )}.{nameof( PortParametersDictionary.PropertyChanged )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.PortPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as PortParametersDictionary, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Handles the data received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Port event information. </param>
        private void HandleDataReceived( IPort sender, ReceiveDataEventArgs e )
        {
            if ( sender is null || e is null )
                return;
            if ( 0 != (e.ReceiveDataStatus & ReceiveDataStatuses.Received) )
            {
                this.ReceiveStatusLabel.Image = Properties.Resources.LedCornerGreen;
                if ( e.DataBuffer is object )
                {
                    this.ReceiveCount += e.NewData.Count();
                    this.ReceiveTextBox.AppendBytes( e.NewData, this.Port.SerialPort.Encoding, this.ReceiveTextBoxCharactersPerLine, this.ReceiveDisplayOption );
                }
            }
            else
            {
                this.ReceiveStatusLabel.Image = Properties.Resources.LedCornerRed;
            }
        }

        /// <summary>
        /// Handles the Data Received event of the _serialPort control. Updates the data boxes and the
        /// received status.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="ReceiveDataEventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_DataReceived( object sender, ReceiveDataEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( IPort.DataReceived )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, ReceiveDataEventArgs>( this.Port_DataReceived ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleDataReceived( sender as IPort, e );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>   Handles the data sent. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Sent data event information. </param>
        private void HandleDataSent( IPort sender, SentDataEventArgs e )
        {
            if ( sender is null || e is null )
                return;
            if ( e.SendDataStatus == SendDataStatus.Sent )
            {
                this.TransmitStatusLabel.Image = Properties.Resources.LedCornerGreen;
                this.TransmitCount += e.DataBuffer.Count();
                this.TransmitStatusLabel.Image = Properties.Resources.LedCornerGray;
                this.TransmitTextBox.AppendBytes( e.DataBuffer, this.Port.SerialPort.Encoding, this.TransmitTextBoxCharactersPerLine, this.TransmitDisplayOption );
            }
            else
            {
                this.TransmitStatusLabel.Image = Properties.Resources.LedCornerRed;
            }
        }

        /// <summary>
        /// Handles the Data Sent event of the _serialPort control. Updates controls and data.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="SentDataEventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_DataSent( object sender, SentDataEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( IPort.DataSent )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, SentDataEventArgs>( this.Port_DataSent ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleDataSent( sender as IPort, e );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Handles the connection changed described by e. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Connection event information. </param>
        private void HandleConnectionChanged( ConnectionEventArgs e )
        {
            if ( e is null )
                return;
            this.OnConnectionChanged( e.IsPortOpen );
        }

        /// <summary> Handles the Connection Changed event of the _serialPort control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="ConnectionEventArgs" /> instance containing the event
        /// data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_ConnectionChanged( object sender, ConnectionEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( IPort.ConnectionChanged )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, ConnectionEventArgs>( this.Port_ConnectionChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleConnectionChanged( e );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Handles the serial port error received described by e. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Serial error received event information. </param>
        private void HandleSerialPortErrorReceived( IPort sender, SerialErrorReceivedEventArgs e )
        {
            if ( sender is null || sender.SerialPort is null || e is null )
                return;
            string message = $"Serial port error received from port {sender.SerialPort.PortName}: {e}";
            this.OnEventHandlerError( new InvalidOperationException( message ) );
        }

        /// <summary> Port serial port error received. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Serial error received event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_SerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( Serial.Port.SerialPortErrorReceived )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, SerialErrorReceivedEventArgs>( this.Port_SerialPortErrorReceived ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleSerialPortErrorReceived( sender as IPort, e );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Handles the timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void HandleTimeout( IPort sender, EventArgs e )
        {
            if ( sender is null || sender.SerialPort is null || e is null )
                return;
            string message = $"Serial port {sender.SerialPort.PortName} timeout";
            this.OnEventHandlerError( new InvalidOperationException( message ) );
        }

        /// <summary> Port timeout. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Port_Timeout( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {typeof( IPort )}.{nameof( Serial.Port.Timeout )} event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, EventArgs>( this.Port_Timeout ), new object[] { sender, e } );
                }
                else
                {
                    this.HandleTimeout( sender as IPort, e );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> List port names. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A String() </returns>
        private string[] ListPortNames()
        {
            var portNames = this.PortNamesCombo.ComboBox.ListPortNames( this.Port.SerialPort.PortName );
            this.PortNamesCombo.Items.Clear();
            this.PortNamesCombo.Items.AddRange( portNames );
            // read available ports on system
            if ( portNames.Length > 0 )
            {
                this.PortNamesCombo.Text = portNames.Contains( Properties.Settings.Instance.PortName, StringComparer.OrdinalIgnoreCase )
                                                    ? Properties.Settings.Instance.PortName
                                                    : portNames[0];
            }
            else
            {
                this.StatusLabel.Text = "no ports detected";
                this.PortNamesCombo.Text = "NO PORTS";
                this.ConnectButton.Text = "NO PORTS";
                this.ConnectButton.Enabled = false;
            }

            return portNames;
        }

        /// <summary> Refresh port list button click. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RefreshPortListButton_Click( object sender, EventArgs e )
        {
            _ = this.ListPortNames();
        }


        #endregion

        #region " DISPLAY MANAGEMENT "

        /// <summary> Width of the receive text box character. </summary>
        private float _ReceiveTextBoxCharacterWidth;

        /// <summary> Gets or sets the average width of the receive text box character. </summary>
        /// <value> The average width of the receive text box character. </value>
        private float ReceiveTextBoxCharacterWidth
        {
            get {
                if ( this._ReceiveTextBoxCharacterWidth == 0f )
                {
                    this._ReceiveTextBoxCharacterWidth = EstimateCharacterWidth( this.ReceiveTextBox );
                }

                return this._ReceiveTextBoxCharacterWidth;
            }

            set => this._ReceiveTextBoxCharacterWidth = value;
        }

        /// <summary> Width of the transmit text box character. </summary>
        private float _TransmitTextBoxCharacterWidth;

        /// <summary> Gets or sets the average width of the Transmit text box character. </summary>
        /// <value> The average width of the Transmit text box character. </value>
        private float TransmitTextBoxCharacterWidth
        {
            get {
                if ( this._TransmitTextBoxCharacterWidth == 0f )
                {
                    this._TransmitTextBoxCharacterWidth = EstimateCharacterWidth( this.TransmitTextBox );
                }

                return this._TransmitTextBoxCharacterWidth;
            }

            set => this._TransmitTextBoxCharacterWidth = value;
        }

        /// <summary> Estimate character width. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="textBox"> The text box control. </param>
        /// <returns> A Single. </returns>
        private static float EstimateCharacterWidth( RichTextBox textBox )
        {
            float result = 10f;
            using ( var g = textBox.CreateGraphics() )
            {
                // measure with a test string
                var szF = g.MeasureString( "0123456789", textBox.Font );
                result = 0.1f * szF.Width;
            }

            return result;
        }

        /// <summary> Gets or sets the transmit panel characters per line. </summary>
        /// <value> The transmit panel characters per line. </value>
        private int TransmitTextBoxCharactersPerLine { get; set; }

        /// <summary> Gets or sets the receive panel characters per line. </summary>
        /// <value> The receive panel characters per line. </value>
        private int ReceiveTextBoxCharactersPerLine { get; set; }

        /// <summary> Selected font. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Font. </returns>
        private static Font SelectedFont( FontSizeOption value )
        {
            Font result;
            switch ( value )
            {
                case FontSizeOption.Large:
                    {
                        result = new Font( Properties.Settings.Instance.FontName, Properties.Settings.Instance.LargeFontSize, FontStyle.Regular, GraphicsUnit.Point, 0 );
                        break;
                    }

                case FontSizeOption.Medium:
                    {
                        result = new Font( Properties.Settings.Instance.FontName, Properties.Settings.Instance.MediumFontSize, FontStyle.Regular, GraphicsUnit.Point, 0 );
                        break;
                    }

                case FontSizeOption.Small:
                    {
                        result = new Font( Properties.Settings.Instance.FontName, Properties.Settings.Instance.SmallFontSize, FontStyle.Regular, GraphicsUnit.Point, 0 );
                        break;
                    }

                default:
                    {
                        result = new Font( Properties.Settings.Instance.FontName, Properties.Settings.Instance.MediumFontSize, FontStyle.Regular, GraphicsUnit.Point, 0 );
                        break;
                    }
            }

            return result;
        }

        /// <summary> Select font size. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        private void SelectFontSize( int value )
        {
            if ( Enum.IsDefined( typeof( FontSizeOption ), value ) )
            {
                this.SelectFontSize( ( FontSizeOption ) value );
            }
        }

        /// <summary> Select font size. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        private void SelectFontSize( FontSizeOption value )
        {
            this.ReceiveTextBox.Font = SelectedFont( value );
            this.ReceiveRulerLabel.Font = this.ReceiveTextBox.Font;
            this.TransmitTextBox.Font = this.ReceiveTextBox.Font;
            this.TransmitRulerLabel.Font = this.ReceiveTextBox.Font;
            this.ReceiveTextBoxCharacterWidth = EstimateCharacterWidth( this.ReceiveTextBox );
            this.TransmitTextBoxCharacterWidth = EstimateCharacterWidth( this.TransmitTextBox );
            Properties.Settings.Instance.FontSize = value;
            if ( this.FontSizeComboBox.SelectedIndex != ( int ) value )
            {
                this.InitializingComponents = true;
                this.FontSizeComboBox.SelectedIndex = ( int ) value;
                this.InitializingComponents = false;
            }
        }

        /// <summary> Font size combo box selected index changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FontSizeComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = "selecting font size";
            try
            {
                if ( sender is ToolStripComboBox combo && combo.SelectedIndex > 0 )
                {
                    this.SelectFontSize( combo.SelectedIndex );
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Shows the Messenger parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portParameters"> Options for controlling the port. </param>
        private void ShowPortParameters( PortParametersDictionary portParameters )
        {
            var s = new StringBuilder();
            _ = s.Append( "Connected using " );
            _ = s.Append( portParameters[PortParameterKey.PortName] );
            _ = s.Append( ';' );
            _ = s.Append( portParameters[PortParameterKey.BaudRate] );
            _ = s.Append( ';' );
            _ = s.Append( portParameters[PortParameterKey.DataBits] );
            _ = s.Append( ';' );
            _ = s.Append( portParameters[PortParameterKey.StopBits] );
            _ = s.Append( ';' );
            _ = s.Append( portParameters[PortParameterKey.Parity] );
            this.StatusLabel.Text = s.ToString();
        }

        #endregion

        #region " TRANSMIT REPEAT "

        /// <summary> Toggle transmission repeat control enabled. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ToggleTransmissionRepeatControlEnabled()
        {
            this.ToggleTransmissionRepeatControlEnabled( !this.TransmitPlayButton.Visible );
        }

        /// <summary> Toggle transmission repeat control enabled. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="enabled"> True to enable, false to disable. </param>
        public void ToggleTransmissionRepeatControlEnabled( bool enabled )
        {
            this.TransmitPlayButton.Visible = enabled;
            this.TransmitDelayMillisecondsTextBox.Visible = enabled;
            this.TransmitRepeatCountTextBox.Visible = enabled;
            this.TransmitSeparator9.Visible = enabled;
        }

        /// <summary> Transmit play button checked changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TransmitPlayButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( this.TransmitPlayButton.Enabled && this.TransmitPlayButton.Visible )
            {
                this.TransmitPlayButton.Text = this.TransmitPlayButton.Checked ? "STOP" : "PLAY";
            }
        }

        #endregion

        #region " MESSAGE NOTIFICATIONS "

        private readonly ErrorProvider _ErrorProvider;



        /// <summary>   Enunciates the specified control. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="control">              The control. </param>
        /// <param name="errorIconAlignment">   The error icon alignment. </param>
        /// <param name="padding">              The padding. </param>
        /// <param name="eventType">            Type of the event. </param>
        /// <param name="details">              The details. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void Enunciate( Control control, ErrorIconAlignment errorIconAlignment, int padding, TraceEventType eventType, string details )
        {
            if ( eventType <= TraceEventType.Warning )
            {
                this._ErrorProvider.SetIconAlignment( control, errorIconAlignment );
                this._ErrorProvider.SetIconPadding( control, padding );
                this._ErrorProvider.SetError( control, details );
            }
            else
            {
                this._ErrorProvider.Clear();
            }
        }

        #endregion

        #region " TOOL STRIP RENDERER "

        /// <summary> A custom professional renderer. </summary>
        /// <remarks>
        /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2017-10-30 </para>
        /// </remarks>
        private class CustomProfessionalRenderer : ToolStripProfessionalRenderer
        {

            /// <summary>
            /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
            /// event.
            /// </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
            /// contains the event data. </param>
            protected override void OnRenderLabelBackground( ToolStripItemRenderEventArgs e )
            {
                if ( e is object && e.Item.BackColor != SystemColors.ControlDark )
                {
                    using var brush = new SolidBrush( e.Item.BackColor );
                    e.Graphics.FillRectangle( brush, e.Item.ContentRectangle );
                }
            }
        }

        #endregion

        #region " EVENT: EVENT HANDLER ERROR "

        /// <summary>   Event queue for all listeners interested in <see cref="System.Exception"/> events. </summary>
        public event System.Threading.ThreadExceptionEventHandler ExceptionEventHandler;

        /// <summary>
        /// Raises the  <see cref="System.Threading.ThreadExceptionEventHandler"/> event.
        /// </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnEventHandlerError( System.Threading.ThreadExceptionEventArgs e )
        {
            this.ExceptionEventHandler?.Invoke( this, e );
        }

        /// <summary>
        /// Raises the  <see cref="System.Threading.ThreadExceptionEventHandler"/> event.
        /// </summary>
        /// <param name="exception">    The exception. </param>
        protected virtual void OnEventHandlerError( System.Exception exception )
        {
            this.OnEventHandlerError( new System.Threading.ThreadExceptionEventArgs( exception ) );
        }

        #endregion


    }

    /// <summary> Values that represent font size options. </summary>
    public enum FontSizeOption
    {

        /// <summary> An enum constant representing the small option. </summary>
        Small,

        /// <summary> An enum constant representing the medium option. </summary>
        Medium,

        /// <summary> An enum constant representing the large option. </summary>
        Large
    }


}
