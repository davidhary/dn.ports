using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Ports.Serial.Forms
{

    public partial class PortConsole
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolStrip = new System.Windows.Forms.ToolStrip();
            this.RefreshPortListButton = new System.Windows.Forms.ToolStripButton();
            this.PortNamesCombo = new System.Windows.Forms.ToolStripComboBox();
            this.ConnectButton = new System.Windows.Forms.ToolStripButton();
            this.LoadConfigButton = new System.Windows.Forms.ToolStripButton();
            this.SaveConfigButton = new System.Windows.Forms.ToolStripButton();
            this.BaudRateComboLabel = new System.Windows.Forms.ToolStripLabel();
            this.BaudRateCombo = new System.Windows.Forms.ToolStripComboBox();
            this.DataBitsComboLabel = new System.Windows.Forms.ToolStripLabel();
            this.DataBitsCombo = new System.Windows.Forms.ToolStripComboBox();
            this.ParityComboLabel = new System.Windows.Forms.ToolStripLabel();
            this.ParityCombo = new System.Windows.Forms.ToolStripComboBox();
            this.StopBitsComboLabel = new System.Windows.Forms.ToolStripLabel();
            this.StopBitsCombo = new System.Windows.Forms.ToolStripComboBox();
            this.ReceiveDelayComboLabel = new System.Windows.Forms.ToolStripLabel();
            this.ReceiveDelayCombo = new System.Windows.Forms.ToolStripComboBox();
            this.ReceiveThresholdComboLabel = new System.Windows.Forms.ToolStripLabel();
            this.ReceiveThresholdCombo = new System.Windows.Forms.ToolStripComboBox();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.FontDialog = new System.Windows.Forms.FontDialog();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.ReceiveTextBox = new System.Windows.Forms.RichTextBox();
            this.ReceiveRulerLabel = new System.Windows.Forms.Label();
            this.ReceiveToolStrip = new System.Windows.Forms.ToolStrip();
            this.ReceiveTextBoxLabel = new System.Windows.Forms.ToolStripLabel();
            this.Separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ClearReceiveBoxButton = new System.Windows.Forms.ToolStripButton();
            this.Separator = new System.Windows.Forms.ToolStripSeparator();
            this.SaveReceiveTextBoxButton = new System.Windows.Forms.ToolStripButton();
            this.Separator7 = new System.Windows.Forms.ToolStripSeparator();
            this.ReceiveCountLabel = new System.Windows.Forms.ToolStripLabel();
            this.Separator8 = new System.Windows.Forms.ToolStripSeparator();
            this.ReceiveStatusLabel = new System.Windows.Forms.ToolStripLabel();
            this.Separator9 = new System.Windows.Forms.ToolStripSeparator();
            this.ReceiveDisplayOptionComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.FontSizeComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.TransmitTextBox = new System.Windows.Forms.RichTextBox();
            this.TransmitContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TransmitCopyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransmitPasteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransmitCutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransmitSendMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransmitSendSelectionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransmitRulerLabel = new System.Windows.Forms.Label();
            this.SendDataToolStrip = new System.Windows.Forms.ToolStrip();
            this.TransmitMessageCombo = new System.Windows.Forms.ToolStripComboBox();
            this.TransmitEnterOptionComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.TransmitToolStrip = new System.Windows.Forms.ToolStrip();
            this.TransmitTextBoxLabel = new System.Windows.Forms.ToolStripLabel();
            this.TransmitSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ClearTransmitBoxButton = new System.Windows.Forms.ToolStripButton();
            this.TransmitSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.LoadTransmitFileButton = new System.Windows.Forms.ToolStripButton();
            this.TransmitSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TransmitCountLabel = new System.Windows.Forms.ToolStripLabel();
            this.TransmitSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TransmitStatusLabel = new System.Windows.Forms.ToolStripLabel();
            this.TransmitSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TransmitDisplayOptionComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.TransmitSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.TransmitTerminationComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.TransmitSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.TransmitRepeatCountTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.TransmitDelayMillisecondsTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.TransmitPlayButton = new System.Windows.Forms.ToolStripButton();
            this.TransmitSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.ToolStrip.SuspendLayout();
            this.StatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).BeginInit();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            this.ReceiveToolStrip.SuspendLayout();
            this.TransmitContextMenu.SuspendLayout();
            this.SendDataToolStrip.SuspendLayout();
            this.TransmitToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStrip
            // 
            this.ToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RefreshPortListButton,
            this.PortNamesCombo,
            this.ConnectButton,
            this.LoadConfigButton,
            this.SaveConfigButton,
            this.BaudRateComboLabel,
            this.BaudRateCombo,
            this.DataBitsComboLabel,
            this.DataBitsCombo,
            this.ParityComboLabel,
            this.ParityCombo,
            this.StopBitsComboLabel,
            this.StopBitsCombo,
            this.ReceiveDelayComboLabel,
            this.ReceiveDelayCombo,
            this.ReceiveThresholdComboLabel,
            this.ReceiveThresholdCombo});
            this.ToolStrip.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip.Name = "ToolStrip";
            this.ToolStrip.Size = new System.Drawing.Size(762, 25);
            this.ToolStrip.TabIndex = 0;
            this.ToolStrip.Text = "Tool Strip";
            // 
            // RefreshPortListButton
            // 
            this.RefreshPortListButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RefreshPortListButton.Image = global::isr.Ports.Serial.Forms.Properties.Resources.refresh;
            this.RefreshPortListButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RefreshPortListButton.Name = "RefreshPortListButton";
            this.RefreshPortListButton.Size = new System.Drawing.Size(23, 22);
            this.RefreshPortListButton.Text = "Refresh";
            this.RefreshPortListButton.ToolTipText = "Refreshes list of available ports";
            this.RefreshPortListButton.Click += new System.EventHandler(this.RefreshPortListButton_Click);
            // 
            // PortNamesCombo
            // 
            this.PortNamesCombo.AutoSize = false;
            this.PortNamesCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PortNamesCombo.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.PortNamesCombo.Name = "PortNamesCombo";
            this.PortNamesCombo.Size = new System.Drawing.Size(60, 23);
            this.PortNamesCombo.ToolTipText = "Port number";
            // 
            // ConnectButton
            // 
            this.ConnectButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ConnectButton.CheckOnClick = true;
            this.ConnectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ConnectButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConnectButton.Image = global::isr.Ports.Serial.Forms.Properties.Resources.LedCornerGray;
            this.ConnectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(23, 22);
            this.ConnectButton.Text = "*CONNECT*";
            this.ConnectButton.ToolTipText = "Connect/Disconnect";
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // LoadConfigButton
            // 
            this.LoadConfigButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LoadConfigButton.Image = global::isr.Ports.Serial.Forms.Properties.Resources.Disk;
            this.LoadConfigButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LoadConfigButton.Name = "LoadConfigButton";
            this.LoadConfigButton.Size = new System.Drawing.Size(23, 22);
            this.LoadConfigButton.Text = "Load Port Configuration";
            this.LoadConfigButton.ToolTipText = "Loads port parameters from file";
            this.LoadConfigButton.Click += new System.EventHandler(this.LoadConfigButton_Click);
            // 
            // SaveConfigButton
            // 
            this.SaveConfigButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveConfigButton.Image = global::isr.Ports.Serial.Forms.Properties.Resources.DiskDownload;
            this.SaveConfigButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveConfigButton.Name = "SaveConfigButton";
            this.SaveConfigButton.Size = new System.Drawing.Size(23, 22);
            this.SaveConfigButton.Text = "Save Port Configuration";
            this.SaveConfigButton.ToolTipText = "Saves port parameters to file";
            this.SaveConfigButton.Click += new System.EventHandler(this.SaveConfigButton_Click);
            // 
            // BaudRateComboLabel
            // 
            this.BaudRateComboLabel.Name = "BaudRateComboLabel";
            this.BaudRateComboLabel.Size = new System.Drawing.Size(37, 22);
            this.BaudRateComboLabel.Text = " Baud";
            this.BaudRateComboLabel.ToolTipText = "Baud Rate";
            // 
            // BaudRateCombo
            // 
            this.BaudRateCombo.AutoSize = false;
            this.BaudRateCombo.DropDownWidth = 50;
            this.BaudRateCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BaudRateCombo.Items.AddRange(new object[] {
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "115200"});
            this.BaudRateCombo.Name = "BaudRateCombo";
            this.BaudRateCombo.Size = new System.Drawing.Size(67, 23);
            this.BaudRateCombo.Text = "9600";
            this.BaudRateCombo.ToolTipText = "Baud Rate";
            // 
            // DataBitsComboLabel
            // 
            this.DataBitsComboLabel.AutoToolTip = true;
            this.DataBitsComboLabel.Name = "DataBitsComboLabel";
            this.DataBitsComboLabel.Size = new System.Drawing.Size(34, 22);
            this.DataBitsComboLabel.Text = " Data";
            this.DataBitsComboLabel.ToolTipText = "Data bits";
            // 
            // DataBitsCombo
            // 
            this.DataBitsCombo.AutoSize = false;
            this.DataBitsCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataBitsCombo.Items.AddRange(new object[] {
            "7",
            "8"});
            this.DataBitsCombo.Name = "DataBitsCombo";
            this.DataBitsCombo.Size = new System.Drawing.Size(30, 23);
            this.DataBitsCombo.Text = "8";
            this.DataBitsCombo.ToolTipText = "Data bits";
            // 
            // ParityComboLabel
            // 
            this.ParityComboLabel.Name = "ParityComboLabel";
            this.ParityComboLabel.Size = new System.Drawing.Size(40, 22);
            this.ParityComboLabel.Text = " Parity";
            this.ParityComboLabel.ToolTipText = "Parity";
            // 
            // ParityCombo
            // 
            this.ParityCombo.AutoSize = false;
            this.ParityCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ParityCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ParityCombo.Items.AddRange(new object[] {
            "None",
            "Even",
            "Mark",
            "Odd",
            "Space"});
            this.ParityCombo.Name = "ParityCombo";
            this.ParityCombo.Size = new System.Drawing.Size(50, 23);
            this.ParityCombo.ToolTipText = "Parity";
            // 
            // StopBitsComboLabel
            // 
            this.StopBitsComboLabel.Name = "StopBitsComboLabel";
            this.StopBitsComboLabel.Size = new System.Drawing.Size(34, 22);
            this.StopBitsComboLabel.Text = " Stop";
            this.StopBitsComboLabel.ToolTipText = "Stop bits";
            // 
            // StopBitsCombo
            // 
            this.StopBitsCombo.AutoSize = false;
            this.StopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StopBitsCombo.DropDownWidth = 50;
            this.StopBitsCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StopBitsCombo.Items.AddRange(new object[] {
            "None",
            "One",
            "Two"});
            this.StopBitsCombo.Name = "StopBitsCombo";
            this.StopBitsCombo.Size = new System.Drawing.Size(50, 23);
            this.StopBitsCombo.ToolTipText = "Stop bits";
            // 
            // ReceiveDelayComboLabel
            // 
            this.ReceiveDelayComboLabel.Name = "ReceiveDelayComboLabel";
            this.ReceiveDelayComboLabel.Size = new System.Drawing.Size(36, 22);
            this.ReceiveDelayComboLabel.Text = "Delay";
            this.ReceiveDelayComboLabel.ToolTipText = "Delay in ms";
            // 
            // ReceiveDelayCombo
            // 
            this.ReceiveDelayCombo.AutoSize = false;
            this.ReceiveDelayCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceiveDelayCombo.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10",
            "20",
            "50",
            "100",
            "200",
            "500",
            "1000"});
            this.ReceiveDelayCombo.Name = "ReceiveDelayCombo";
            this.ReceiveDelayCombo.Size = new System.Drawing.Size(45, 23);
            this.ReceiveDelayCombo.Text = "1";
            this.ReceiveDelayCombo.ToolTipText = "Data received handle delay in ms";
            // 
            // ReceiveThresholdComboLabel
            // 
            this.ReceiveThresholdComboLabel.Name = "ReceiveThresholdComboLabel";
            this.ReceiveThresholdComboLabel.Size = new System.Drawing.Size(30, 22);
            this.ReceiveThresholdComboLabel.Text = "Lim.";
            // 
            // ReceiveThresholdCombo
            // 
            this.ReceiveThresholdCombo.AutoSize = false;
            this.ReceiveThresholdCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.ReceiveThresholdCombo.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10",
            "20",
            "50",
            "100",
            "200",
            "500",
            "1000"});
            this.ReceiveThresholdCombo.Name = "ReceiveThresholdCombo";
            this.ReceiveThresholdCombo.Size = new System.Drawing.Size(45, 23);
            this.ReceiveThresholdCombo.Text = "1";
            this.ReceiveThresholdCombo.ToolTipText = "received bytes threshold property";
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.StatusStrip.Location = new System.Drawing.Point(0, 576);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(762, 22);
            this.StatusStrip.TabIndex = 1;
            this.StatusStrip.Text = "Status Strip";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(16, 17);
            this.StatusLabel.Text = "...";
            // 
            // SplitContainer
            // 
            this.SplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer.Location = new System.Drawing.Point(0, 25);
            this.SplitContainer.Name = "SplitContainer";
            this.SplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.Controls.Add(this.ReceiveTextBox);
            this.SplitContainer.Panel1.Controls.Add(this.ReceiveRulerLabel);
            this.SplitContainer.Panel1.Controls.Add(this.ReceiveToolStrip);
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.Controls.Add(this.TransmitTextBox);
            this.SplitContainer.Panel2.Controls.Add(this.TransmitRulerLabel);
            this.SplitContainer.Panel2.Controls.Add(this.SendDataToolStrip);
            this.SplitContainer.Panel2.Controls.Add(this.TransmitToolStrip);
            this.SplitContainer.Size = new System.Drawing.Size(762, 551);
            this.SplitContainer.SplitterDistance = 341;
            this.SplitContainer.TabIndex = 3;
            // 
            // ReceiveTextBox
            // 
            this.ReceiveTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ReceiveTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReceiveTextBox.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceiveTextBox.Location = new System.Drawing.Point(0, 38);
            this.ReceiveTextBox.Name = "ReceiveTextBox";
            this.ReceiveTextBox.ReadOnly = true;
            this.ReceiveTextBox.Size = new System.Drawing.Size(760, 301);
            this.ReceiveTextBox.TabIndex = 3;
            this.ReceiveTextBox.Text = "*";
            this.ReceiveTextBox.WordWrap = false;
            // 
            // ReceiveRulerLabel
            // 
            this.ReceiveRulerLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ReceiveRulerLabel.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceiveRulerLabel.Location = new System.Drawing.Point(0, 25);
            this.ReceiveRulerLabel.Name = "ReceiveRulerLabel";
            this.ReceiveRulerLabel.Size = new System.Drawing.Size(760, 13);
            this.ReceiveRulerLabel.TabIndex = 4;
            this.ReceiveRulerLabel.Text = "Ruler";
            this.ReceiveRulerLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // ReceiveToolStrip
            // 
            this.ReceiveToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.ReceiveToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ReceiveToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ReceiveTextBoxLabel,
            this.Separator2,
            this.ClearReceiveBoxButton,
            this.Separator,
            this.SaveReceiveTextBoxButton,
            this.Separator7,
            this.ReceiveCountLabel,
            this.Separator8,
            this.ReceiveStatusLabel,
            this.Separator9,
            this.ReceiveDisplayOptionComboBox,
            this.FontSizeComboBox});
            this.ReceiveToolStrip.Location = new System.Drawing.Point(0, 0);
            this.ReceiveToolStrip.Name = "ReceiveToolStrip";
            this.ReceiveToolStrip.Size = new System.Drawing.Size(760, 25);
            this.ReceiveToolStrip.TabIndex = 0;
            this.ReceiveToolStrip.Text = "Receive Tool Strip";
            // 
            // ReceiveTextBoxLabel
            // 
            this.ReceiveTextBoxLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReceiveTextBoxLabel.Name = "ReceiveTextBoxLabel";
            this.ReceiveTextBoxLabel.Size = new System.Drawing.Size(51, 22);
            this.ReceiveTextBoxLabel.Text = "RECEIVE";
            // 
            // Separator2
            // 
            this.Separator2.Name = "Separator2";
            this.Separator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ClearReceiveBoxButton
            // 
            this.ClearReceiveBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ClearReceiveBoxButton.Image = global::isr.Ports.Serial.Forms.Properties.Resources.DocDelete;
            this.ClearReceiveBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClearReceiveBoxButton.Name = "ClearReceiveBoxButton";
            this.ClearReceiveBoxButton.Size = new System.Drawing.Size(23, 22);
            this.ClearReceiveBoxButton.ToolTipText = "clear receive box";
            this.ClearReceiveBoxButton.Click += new System.EventHandler(this.ClearReceiveBoxButton_Click);
            // 
            // Separator
            // 
            this.Separator.Name = "Separator";
            this.Separator.Size = new System.Drawing.Size(6, 25);
            // 
            // SaveReceiveTextBoxButton
            // 
            this.SaveReceiveTextBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveReceiveTextBoxButton.Image = global::isr.Ports.Serial.Forms.Properties.Resources.DiskDownload;
            this.SaveReceiveTextBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveReceiveTextBoxButton.Name = "SaveReceiveTextBoxButton";
            this.SaveReceiveTextBoxButton.Size = new System.Drawing.Size(23, 22);
            this.SaveReceiveTextBoxButton.ToolTipText = "save received data to text file";
            this.SaveReceiveTextBoxButton.Click += new System.EventHandler(this.SaveReceiveTextBoxButton_Click);
            // 
            // Separator7
            // 
            this.Separator7.Name = "Separator7";
            this.Separator7.Size = new System.Drawing.Size(6, 25);
            // 
            // ReceiveCountLabel
            // 
            this.ReceiveCountLabel.Name = "ReceiveCountLabel";
            this.ReceiveCountLabel.Size = new System.Drawing.Size(37, 22);
            this.ReceiveCountLabel.Text = "00000";
            this.ReceiveCountLabel.ToolTipText = "number of bytes received";
            // 
            // Separator8
            // 
            this.Separator8.Name = "Separator8";
            this.Separator8.Size = new System.Drawing.Size(6, 25);
            // 
            // ReceiveStatusLabel
            // 
            this.ReceiveStatusLabel.Image = global::isr.Ports.Serial.Forms.Properties.Resources.LedCornerGray;
            this.ReceiveStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ReceiveStatusLabel.Name = "ReceiveStatusLabel";
            this.ReceiveStatusLabel.Size = new System.Drawing.Size(16, 22);
            this.ReceiveStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ReceiveStatusLabel.ToolTipText = "receive status";
            // 
            // Separator9
            // 
            this.Separator9.Name = "Separator9";
            this.Separator9.Size = new System.Drawing.Size(6, 25);
            // 
            // ReceiveDisplayOptionComboBox
            // 
            this.ReceiveDisplayOptionComboBox.AutoSize = false;
            this.ReceiveDisplayOptionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ReceiveDisplayOptionComboBox.Items.AddRange(new object[] {
            "HEX",
            "ASCII",
            "BOTH"});
            this.ReceiveDisplayOptionComboBox.Name = "ReceiveDisplayOptionComboBox";
            this.ReceiveDisplayOptionComboBox.Size = new System.Drawing.Size(53, 23);
            this.ReceiveDisplayOptionComboBox.ToolTipText = "Receive display option";
            this.ReceiveDisplayOptionComboBox.SelectedIndexChanged += new System.EventHandler(this.ReceiveDisplayOptionComboBox_SelectedIndexChanged);
            // 
            // FontSizeComboBox
            // 
            this.FontSizeComboBox.AutoSize = false;
            this.FontSizeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FontSizeComboBox.Items.AddRange(new object[] {
            "Small",
            "Medium",
            "Large"});
            this.FontSizeComboBox.Name = "FontSizeComboBox";
            this.FontSizeComboBox.Size = new System.Drawing.Size(61, 23);
            this.FontSizeComboBox.ToolTipText = "Font size";
            this.FontSizeComboBox.SelectedIndexChanged += new System.EventHandler(this.FontSizeComboBox_SelectedIndexChanged);
            // 
            // TransmitTextBox
            // 
            this.TransmitTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.TransmitTextBox.ContextMenuStrip = this.TransmitContextMenu;
            this.TransmitTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransmitTextBox.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmitTextBox.Location = new System.Drawing.Point(0, 63);
            this.TransmitTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TransmitTextBox.Name = "TransmitTextBox";
            this.TransmitTextBox.ReadOnly = true;
            this.TransmitTextBox.Size = new System.Drawing.Size(760, 141);
            this.TransmitTextBox.TabIndex = 4;
            this.TransmitTextBox.Text = "*";
            this._ToolTip.SetToolTip(this.TransmitTextBox, "press right mouse button");
            this.TransmitTextBox.WordWrap = false;
            this.TransmitTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TransmitTextBox_MouseClick);
            // 
            // TransmitContextMenu
            // 
            this.TransmitContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TransmitCopyMenuItem,
            this.TransmitPasteMenuItem,
            this.TransmitCutMenuItem,
            this.TransmitSendMenuItem,
            this.TransmitSendSelectionMenuItem});
            this.TransmitContextMenu.Name = "MenuTxBox";
            this.TransmitContextMenu.Size = new System.Drawing.Size(150, 114);
            // 
            // TransmitCopyMenuItem
            // 
            this.TransmitCopyMenuItem.Image = global::isr.Ports.Serial.Forms.Properties.Resources.Copy;
            this.TransmitCopyMenuItem.Name = "TransmitCopyMenuItem";
            this.TransmitCopyMenuItem.Size = new System.Drawing.Size(149, 22);
            this.TransmitCopyMenuItem.Text = "Copy";
            this.TransmitCopyMenuItem.Click += new System.EventHandler(this.TransmitCopyMenuItem_Click);
            // 
            // TransmitPasteMenuItem
            // 
            this.TransmitPasteMenuItem.Image = global::isr.Ports.Serial.Forms.Properties.Resources.Paste;
            this.TransmitPasteMenuItem.Name = "TransmitPasteMenuItem";
            this.TransmitPasteMenuItem.Size = new System.Drawing.Size(149, 22);
            this.TransmitPasteMenuItem.Text = "Paste";
            this.TransmitPasteMenuItem.Click += new System.EventHandler(this.TransmitPasteMenuItem_Click);
            // 
            // TransmitCutMenuItem
            // 
            this.TransmitCutMenuItem.Image = global::isr.Ports.Serial.Forms.Properties.Resources.ClipboardCut;
            this.TransmitCutMenuItem.Name = "TransmitCutMenuItem";
            this.TransmitCutMenuItem.Size = new System.Drawing.Size(149, 22);
            this.TransmitCutMenuItem.Text = "Cut";
            this.TransmitCutMenuItem.Click += new System.EventHandler(this.TransmitCutMenuItem_Click);
            // 
            // TransmitSendMenuItem
            // 
            this.TransmitSendMenuItem.Image = global::isr.Ports.Serial.Forms.Properties.Resources.Arrow1Right;
            this.TransmitSendMenuItem.Name = "TransmitSendMenuItem";
            this.TransmitSendMenuItem.Size = new System.Drawing.Size(149, 22);
            this.TransmitSendMenuItem.Text = "send line";
            this.TransmitSendMenuItem.Click += new System.EventHandler(this.TransmitSendMenuItem_Click);
            // 
            // TransmitSendSelectionMenuItem
            // 
            this.TransmitSendSelectionMenuItem.Image = global::isr.Ports.Serial.Forms.Properties.Resources.Arrow2Right;
            this.TransmitSendSelectionMenuItem.Name = "TransmitSendSelectionMenuItem";
            this.TransmitSendSelectionMenuItem.Size = new System.Drawing.Size(149, 22);
            this.TransmitSendSelectionMenuItem.Text = "send selection";
            this.TransmitSendSelectionMenuItem.Click += new System.EventHandler(this.TransmitSendSelectionMenuItem_Click);
            // 
            // TransmitRulerLabel
            // 
            this.TransmitRulerLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TransmitRulerLabel.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmitRulerLabel.Location = new System.Drawing.Point(0, 50);
            this.TransmitRulerLabel.Name = "TransmitRulerLabel";
            this.TransmitRulerLabel.Size = new System.Drawing.Size(760, 13);
            this.TransmitRulerLabel.TabIndex = 5;
            this.TransmitRulerLabel.Text = "Ruler";
            this.TransmitRulerLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // SendDataToolStrip
            // 
            this.SendDataToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.SendDataToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.SendDataToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TransmitMessageCombo,
            this.TransmitEnterOptionComboBox});
            this.SendDataToolStrip.Location = new System.Drawing.Point(0, 25);
            this.SendDataToolStrip.Name = "SendDataToolStrip";
            this.SendDataToolStrip.Size = new System.Drawing.Size(760, 25);
            this.SendDataToolStrip.TabIndex = 3;
            this.SendDataToolStrip.Text = "Send Data Tool Strip";
            // 
            // TransmitMessageCombo
            // 
            this.TransmitMessageCombo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmitMessageCombo.Name = "TransmitMessageCombo";
            this.TransmitMessageCombo.Size = new System.Drawing.Size(500, 25);
            this.TransmitMessageCombo.Text = "enter message to send and press<enter>";
            this.TransmitMessageCombo.ToolTipText = "enter message to send and press<enter>";
            this.TransmitMessageCombo.TextUpdate += new System.EventHandler(this.TransmitMessageCombo_TextUpdate);
            this.TransmitMessageCombo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressDingHandler);
            this.TransmitMessageCombo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TransmitMessageCombo_KeyPress);
            this.TransmitMessageCombo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SuppressDingHandler);
            // 
            // TransmitEnterOptionComboBox
            // 
            this.TransmitEnterOptionComboBox.AutoSize = false;
            this.TransmitEnterOptionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransmitEnterOptionComboBox.Items.AddRange(new object[] {
            "HEX",
            "ASCII"});
            this.TransmitEnterOptionComboBox.Name = "TransmitEnterOptionComboBox";
            this.TransmitEnterOptionComboBox.Size = new System.Drawing.Size(53, 23);
            this.TransmitEnterOptionComboBox.ToolTipText = "Enter Hex or ASCII";
            // 
            // TransmitToolStrip
            // 
            this.TransmitToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.TransmitToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.TransmitToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TransmitTextBoxLabel,
            this.TransmitSeparator1,
            this.ClearTransmitBoxButton,
            this.TransmitSeparator2,
            this.LoadTransmitFileButton,
            this.TransmitSeparator3,
            this.TransmitCountLabel,
            this.TransmitSeparator4,
            this.TransmitStatusLabel,
            this.TransmitSeparator5,
            this.TransmitDisplayOptionComboBox,
            this.TransmitSeparator6,
            this.TransmitTerminationComboBox,
            this.TransmitSeparator7,
            this.TransmitRepeatCountTextBox,
            this.TransmitDelayMillisecondsTextBox,
            this.TransmitPlayButton,
            this.TransmitSeparator9});
            this.TransmitToolStrip.Location = new System.Drawing.Point(0, 0);
            this.TransmitToolStrip.Name = "TransmitToolStrip";
            this.TransmitToolStrip.Size = new System.Drawing.Size(760, 25);
            this.TransmitToolStrip.TabIndex = 0;
            this.TransmitToolStrip.Text = "Transmit Tool Strip";
            // 
            // TransmitTextBoxLabel
            // 
            this.TransmitTextBoxLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmitTextBoxLabel.Name = "TransmitTextBoxLabel";
            this.TransmitTextBoxLabel.Size = new System.Drawing.Size(62, 22);
            this.TransmitTextBoxLabel.Text = "TRANSMIT";
            // 
            // TransmitSeparator1
            // 
            this.TransmitSeparator1.Name = "TransmitSeparator1";
            this.TransmitSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ClearTransmitBoxButton
            // 
            this.ClearTransmitBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ClearTransmitBoxButton.Image = global::isr.Ports.Serial.Forms.Properties.Resources.DocDelete;
            this.ClearTransmitBoxButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClearTransmitBoxButton.Name = "ClearTransmitBoxButton";
            this.ClearTransmitBoxButton.Size = new System.Drawing.Size(23, 22);
            this.ClearTransmitBoxButton.ToolTipText = "Clear transmit box";
            this.ClearTransmitBoxButton.Click += new System.EventHandler(this.ClearTransmitBoxButton_Click);
            // 
            // TransmitSeparator2
            // 
            this.TransmitSeparator2.Name = "TransmitSeparator2";
            this.TransmitSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // LoadTransmitFileButton
            // 
            this.LoadTransmitFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LoadTransmitFileButton.Image = global::isr.Ports.Serial.Forms.Properties.Resources.Disk;
            this.LoadTransmitFileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LoadTransmitFileButton.Name = "LoadTransmitFileButton";
            this.LoadTransmitFileButton.Size = new System.Drawing.Size(23, 22);
            this.LoadTransmitFileButton.ToolTipText = "load file into transmit text box";
            this.LoadTransmitFileButton.Click += new System.EventHandler(this.LoadTransmitFileButton_Click);
            // 
            // TransmitSeparator3
            // 
            this.TransmitSeparator3.Name = "TransmitSeparator3";
            this.TransmitSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // TransmitCountLabel
            // 
            this.TransmitCountLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmitCountLabel.Name = "TransmitCountLabel";
            this.TransmitCountLabel.Size = new System.Drawing.Size(37, 22);
            this.TransmitCountLabel.Text = "00000";
            this.TransmitCountLabel.ToolTipText = "number of bytes sent";
            // 
            // TransmitSeparator4
            // 
            this.TransmitSeparator4.Name = "TransmitSeparator4";
            this.TransmitSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // TransmitStatusLabel
            // 
            this.TransmitStatusLabel.Image = global::isr.Ports.Serial.Forms.Properties.Resources.LedCornerGray;
            this.TransmitStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TransmitStatusLabel.Name = "TransmitStatusLabel";
            this.TransmitStatusLabel.Size = new System.Drawing.Size(16, 22);
            this.TransmitStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TransmitStatusLabel.ToolTipText = "send status";
            // 
            // TransmitSeparator5
            // 
            this.TransmitSeparator5.Name = "TransmitSeparator5";
            this.TransmitSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // TransmitDisplayOptionComboBox
            // 
            this.TransmitDisplayOptionComboBox.AutoSize = false;
            this.TransmitDisplayOptionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransmitDisplayOptionComboBox.Items.AddRange(new object[] {
            "HEX",
            "ASCII",
            "BOTH"});
            this.TransmitDisplayOptionComboBox.Name = "TransmitDisplayOptionComboBox";
            this.TransmitDisplayOptionComboBox.Size = new System.Drawing.Size(53, 23);
            this.TransmitDisplayOptionComboBox.ToolTipText = "Transmit display option";
            this.TransmitDisplayOptionComboBox.SelectedIndexChanged += new System.EventHandler(this.TransmitDisplayOptionComboBox_CheckedChanged);
            // 
            // TransmitSeparator6
            // 
            this.TransmitSeparator6.Name = "TransmitSeparator6";
            this.TransmitSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // TransmitTerminationComboBox
            // 
            this.TransmitTerminationComboBox.AutoSize = false;
            this.TransmitTerminationComboBox.Items.AddRange(new object[] {
            "",
            "\\n",
            "\\r",
            "\\r\\n"});
            this.TransmitTerminationComboBox.Name = "TransmitTerminationComboBox";
            this.TransmitTerminationComboBox.Size = new System.Drawing.Size(44, 23);
            this.TransmitTerminationComboBox.ToolTipText = "Append end-of-line characters";
            // 
            // TransmitSeparator7
            // 
            this.TransmitSeparator7.Name = "TransmitSeparator7";
            this.TransmitSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // TransmitRepeatCountTextBox
            // 
            this.TransmitRepeatCountTextBox.AutoSize = false;
            this.TransmitRepeatCountTextBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmitRepeatCountTextBox.Name = "TransmitRepeatCountTextBox";
            this.TransmitRepeatCountTextBox.Size = new System.Drawing.Size(50, 23);
            this.TransmitRepeatCountTextBox.Text = "100";
            this.TransmitRepeatCountTextBox.ToolTipText = "Repeat the transmitted message.";
            this.TransmitRepeatCountTextBox.Visible = false;
            // 
            // TransmitDelayMillisecondsTextBox
            // 
            this.TransmitDelayMillisecondsTextBox.AutoSize = false;
            this.TransmitDelayMillisecondsTextBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmitDelayMillisecondsTextBox.Name = "TransmitDelayMillisecondsTextBox";
            this.TransmitDelayMillisecondsTextBox.Size = new System.Drawing.Size(50, 23);
            this.TransmitDelayMillisecondsTextBox.Text = "5000";
            this.TransmitDelayMillisecondsTextBox.ToolTipText = "Transmit delay time milliseconds. Applies to repeat messages only. ";
            this.TransmitDelayMillisecondsTextBox.Visible = false;
            // 
            // TransmitPlayButton
            // 
            this.TransmitPlayButton.AutoToolTip = false;
            this.TransmitPlayButton.CheckOnClick = true;
            this.TransmitPlayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TransmitPlayButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TransmitPlayButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TransmitPlayButton.Name = "TransmitPlayButton";
            this.TransmitPlayButton.Size = new System.Drawing.Size(38, 22);
            this.TransmitPlayButton.Text = "PLAY";
            this.TransmitPlayButton.ToolTipText = "Send the transmit box messages as many times as identified in the Repeat box.";
            this.TransmitPlayButton.Visible = false;
            this.TransmitPlayButton.CheckedChanged += new System.EventHandler(this.TransmitPlayButton_CheckedChanged);
            // 
            // TransmitSeparator9
            // 
            this.TransmitSeparator9.Name = "TransmitSeparator9";
            this.TransmitSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // PortConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SplitContainer);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.ToolStrip);
            this.MinimumSize = new System.Drawing.Size(716, 250);
            this.Name = "PortConsole";
            this.Size = new System.Drawing.Size(762, 598);
            this.Load += new System.EventHandler(this.Form_Load);
            this.VisibleChanged += new System.EventHandler(this.MessengerConsole_VisibleChanged);
            this.Resize += new System.EventHandler(this.Form_ResizeEnd);
            this.ToolStrip.ResumeLayout(false);
            this.ToolStrip.PerformLayout();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel1.PerformLayout();
            this.SplitContainer.Panel2.ResumeLayout(false);
            this.SplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).EndInit();
            this.SplitContainer.ResumeLayout(false);
            this.ReceiveToolStrip.ResumeLayout(false);
            this.ReceiveToolStrip.PerformLayout();
            this.TransmitContextMenu.ResumeLayout(false);
            this.SendDataToolStrip.ResumeLayout(false);
            this.SendDataToolStrip.PerformLayout();
            this.TransmitToolStrip.ResumeLayout(false);
            this.TransmitToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.ToolStrip ToolStrip;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.FontDialog FontDialog;
        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.ToolStrip ReceiveToolStrip;
        private System.Windows.Forms.ToolStripLabel ReceiveTextBoxLabel;
        private System.Windows.Forms.ToolStrip TransmitToolStrip;
        private System.Windows.Forms.ToolStripLabel TransmitTextBoxLabel;
        private System.Windows.Forms.ToolStripButton ClearTransmitBoxButton;
        private System.Windows.Forms.ToolStripSeparator Separator2;
        private System.Windows.Forms.ToolStripButton ClearReceiveBoxButton;
        private System.Windows.Forms.ToolStripSeparator Separator;
        private System.Windows.Forms.ToolStripSeparator TransmitSeparator1;
        private System.Windows.Forms.ToolStripSeparator TransmitSeparator2;
        private System.Windows.Forms.ToolStripButton LoadTransmitFileButton;
        private System.Windows.Forms.ToolStripSeparator TransmitSeparator3;
        private System.Windows.Forms.ToolStripLabel TransmitCountLabel;
        private System.Windows.Forms.ToolStripSeparator TransmitSeparator4;
        private System.Windows.Forms.ToolStripButton SaveReceiveTextBoxButton;
        private System.Windows.Forms.ToolStripSeparator Separator7;
        private System.Windows.Forms.ToolStripLabel ReceiveCountLabel;
        private System.Windows.Forms.ToolStripSeparator Separator8;
        private System.Windows.Forms.ToolStripLabel ReceiveStatusLabel;
        private System.Windows.Forms.ToolStripSeparator Separator9;
        private System.Windows.Forms.ToolStripLabel TransmitStatusLabel;
        private System.Windows.Forms.ToolStripSeparator TransmitSeparator5;
        private System.Windows.Forms.RichTextBox ReceiveTextBox;
        private System.Windows.Forms.ToolStrip SendDataToolStrip;
        private System.Windows.Forms.ToolStripComboBox TransmitMessageCombo;
        private System.Windows.Forms.RichTextBox TransmitTextBox;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.ToolStripComboBox PortNamesCombo;
        private System.Windows.Forms.ToolStripLabel BaudRateComboLabel;
        private System.Windows.Forms.ToolStripComboBox BaudRateCombo;
        private System.Windows.Forms.ToolStripLabel ParityComboLabel;
        private System.Windows.Forms.ToolStripComboBox ParityCombo;
        private System.Windows.Forms.ToolStripLabel StopBitsComboLabel;
        private System.Windows.Forms.ToolStripComboBox StopBitsCombo;
        private System.Windows.Forms.ToolStripButton ConnectButton;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.ContextMenuStrip TransmitContextMenu;
        private System.Windows.Forms.ToolStripMenuItem TransmitCopyMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TransmitPasteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TransmitCutMenuItem;
        private System.Windows.Forms.ToolStripLabel DataBitsComboLabel;
        private System.Windows.Forms.ToolStripComboBox DataBitsCombo;
        private System.Windows.Forms.ToolStripMenuItem TransmitSendMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TransmitSendSelectionMenuItem;
        private System.Windows.Forms.ToolStripLabel ReceiveDelayComboLabel;
        private System.Windows.Forms.ToolStripComboBox ReceiveDelayCombo;
        private System.Windows.Forms.ToolStripLabel ReceiveThresholdComboLabel;
        private System.Windows.Forms.ToolStripComboBox ReceiveThresholdCombo;
        private System.Windows.Forms.ToolStripTextBox TransmitRepeatCountTextBox;
        private System.Windows.Forms.ToolStripSeparator TransmitSeparator6;
        private System.Windows.Forms.ToolStripSeparator TransmitSeparator7;
        private System.Windows.Forms.ToolStripButton TransmitPlayButton;
        private System.Windows.Forms.ToolStripTextBox TransmitDelayMillisecondsTextBox;
        private System.Windows.Forms.ToolStripSeparator TransmitSeparator9;
        private System.Windows.Forms.ToolStripButton RefreshPortListButton;
        private System.Windows.Forms.ToolStripComboBox TransmitEnterOptionComboBox;
        private System.Windows.Forms.ToolStripComboBox TransmitDisplayOptionComboBox;
        private System.Windows.Forms.ToolStripComboBox TransmitTerminationComboBox;
        private System.Windows.Forms.ToolStripComboBox ReceiveDisplayOptionComboBox;
        private System.Windows.Forms.Label ReceiveRulerLabel;
        private System.Windows.Forms.Label TransmitRulerLabel;
        private System.Windows.Forms.ToolStripButton LoadConfigButton;
        private System.Windows.Forms.ToolStripButton SaveConfigButton;
        private System.Windows.Forms.ToolStripComboBox FontSizeComboBox;
    }
}
