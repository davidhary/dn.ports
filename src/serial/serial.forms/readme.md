# About

isr.Ports.Serial.Forms is a .Net library providing Windows Controls and Forms for serial communication.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Ports.Serial.Forms is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Ports Repository].

[Ports Repository]: https://bitbucket.org/davidhary/dn.ports

