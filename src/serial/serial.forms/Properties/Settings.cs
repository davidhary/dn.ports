using System;
using System.ComponentModel;

namespace isr.Ports.Serial.Forms.Properties
{
    /// <summary>   A settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( Settings ) )]
    public class Settings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public Settings() : base( System.Reflection.Assembly.GetAssembly( typeof( Settings ) ) )
        { }

        /// <summary>   (Immutable) The lazy instance. </summary>
        private static readonly Lazy<Settings> LazyInstance = new( () => new Settings() );

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static Settings Instance
        {
            get {
                if ( !LazyInstance.IsValueCreated )
                {
                    // This ensures that the settings is created if the application context settings file
                    // does not exist.
                    LazyInstance.Value.Initialize( new object[] { LazyInstance.Value } );
                }
                return LazyInstance.Value;
            }
        }

        /// <summary>   Saves this object. </summary>
        /// <remarks>   David, 2021-12-06. </remarks>
        public void Save()
        {
            this.SaveSettings( new object[] { this } );
        }

        private DisplayOptions _EnteredDataEncodingOption = DisplayOptions.Ascii;
        /// <summary>   Gets or sets the enter option. </summary>
        /// <value> The enter option. </value>
        [Description("The encoding options of the entered data, e.g., Hex, ASCII or both")]
        public DisplayOptions EnteredDataEncodingOption
        {
            get => this._EnteredDataEncodingOption;
            set {
                if ( !int.Equals( value, this.EnteredDataEncodingOption ) )
                {
                    this._EnteredDataEncodingOption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private DisplayOptions _ReceiveDisplayOption = DisplayOptions.Ascii;
        /// <summary>   Gets or sets the receive display option. </summary>
        /// <value> The receive display option. </value>
        [Description( "The display options for received data, e.g., Hex, ASCII or both" )]
        public DisplayOptions ReceiveDisplayOption
        {
            get => this._ReceiveDisplayOption;
            set {
                if ( !int.Equals( value, this.ReceiveDisplayOption ) )
                {
                    this._ReceiveDisplayOption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private DisplayOptions _TransmitDisplayOption = DisplayOptions.Ascii;
        /// <summary>   Gets or sets the transmit display option. </summary>
        /// <value> The transmit display option. </value>
        [Description( "The display options for transmitted data, e.g., Hex, ASCII or both" )]
        public DisplayOptions TransmitDisplayOption
        {
            get => this._TransmitDisplayOption;
            set {
                if ( !int.Equals( value, this.TransmitDisplayOption ) )
                {
                    this._TransmitDisplayOption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _PortName = "COM2";
        /// <summary>   Gets or sets the name of the Port. </summary>
        /// <value> The name of the Port. </value>
        [Description( "The name of the serial port" )]
        public string PortName
        {
            get => this._PortName;
            set {
                if ( !string.Equals( value, this.PortName ) )
                {
                    this._PortName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _Termination = @"\r\n";
        /// <summary>   Gets or sets the termination. </summary>
        /// <value> The termination. </value>
        [Description("The termination of transmitted data")]
        public string Termination
        {
            get => this._Termination;
            set {
                if ( !String.Equals( value, this.Termination ) )
                {
                    this._Termination = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _FontName = "Lucida Console";
        /// <summary>   Gets or sets the name of the Font. </summary>
        /// <value> The name of the Font. </value>
        [Description("The font name for display")]
        public string FontName
        {
            get => this._FontName;
            set {
                if ( !string.Equals( value, this.FontName ) )
                {
                    this._FontName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _SmallFontSize = 8;
        /// <summary>   Gets or sets the size of the small font. </summary>
        /// <value> The size of the small font. </value>
        [Description("The font size to select when selecting the small font size option")]
        public int SmallFontSize
        {
            get => this._SmallFontSize;
            set {
                if ( !int.Equals( value, this.SmallFontSize ) )
                {
                    this._SmallFontSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _MediumFontSize = 10;
        /// <summary>   Gets or sets the size of the medium font. </summary>
        /// <value> The size of the medium font. </value>
        [Description( "The font size to select when selecting the medium font size option" )]
        public int MediumFontSize
        {
            get => this._MediumFontSize;
            set {
                if ( !int.Equals( value, this.MediumFontSize ) )
                {
                    this._MediumFontSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _LargeFontSize = 12;
        /// <summary>   Gets or sets the size of the large font. </summary>
        /// <value> The size of the large font. </value>
        [Description( "The font size to select when selecting the large font size option" )]
        public int LargeFontSize
        {
            get => this._LargeFontSize;
            set {
                if ( !int.Equals( value, this.LargeFontSize ) )
                {
                    this._LargeFontSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private FontSizeOption _FontSize = FontSizeOption.Small;
        /// <summary>   Gets or sets the font size. </summary>
        /// <value> The size of the font. </value>
        [Description("Selects the font size option for display")]
        public FontSizeOption FontSize
        {
            get => this._FontSize;
            set {
                if ( !int.Equals( value, this.FontSize ) )
                {
                    this._FontSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

    }

}
