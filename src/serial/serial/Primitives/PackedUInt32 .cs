using System;

namespace isr.Ports.Serial
{

    /// <summary>
    /// Packs or unpacks two unsigned <see cref="T:UInt32">short</see> values into an unsigned
    /// <see cref="T:UInt32">integer</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    [CLSCompliant( false )]
    public sealed class PackedUInt32
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PackedUInt32() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="PackedUInt32" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public PackedUInt32( uint value ) : this()
        {
            this.Value = value;
            this.FromValueThis();
        }

        /// <summary> Initializes a new instance of the <see cref="PackedUInt32" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public PackedUInt32( int value ) : this()
        {
            this.Value = ( uint ) (value);
            this.FromValueThis();
        }

        /// <summary> Initializes a new instance of the <see cref="PackedUInt32" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highPart"> The high part. </param>
        /// <param name="lowPart">  The low part. </param>
        public PackedUInt32( PackedUInt16 highPart, PackedUInt16 lowPart ) : this()
        {
            this.HighPart = highPart;
            this.LowPart = lowPart;
            this.ToValueThis();
        }

        #endregion

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public uint Value { get; set; }

        /// <summary> Gets or sets the high part. </summary>
        /// <value> The high part. </value>
        public PackedUInt16 HighPart { get; set; }

        /// <summary> Gets or sets the low part. </summary>
        /// <value> The low part. </value>
        public PackedUInt16 LowPart { get; set; }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void FromValueThis()
        {
            this.LowPart = ToLowUnsignedPart( this.Value );
            this.HighPart = ToHighUnsignedPart( this.Value );
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void FromValue()
        {
            this.FromValueThis();
        }

        /// <summary> Parses the value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public void FromValue( uint value )
        {
            this.Value = value;
            this.FromValue();
        }

        /// <summary> Builds the <see cref="T:UInt32">unsigned integer</see> value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ToValueThis()
        {
            this.Value = ( uint ) (this.HighPart.Value & 0xFFFF);
            this.Value = ( uint ) (this.LowPart.Value | 0xFFFF0000L & this.Value << 16);
        }

        /// <summary> Builds the <see cref="T:UInt32">unsigned integer</see> value. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void ToValue()
        {
            this.ToValueThis();
        }

        /// <summary> Parses the high unsigned part. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a PackedUInt16. </returns>
        public static PackedUInt16 ToHighUnsignedPart( uint value )
        {
            return new PackedUInt16( ( ushort ) ((value & 0xFFFF0000L) >> 16) );
        }

        /// <summary> Parses the low unsigned part. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a PackedUInt16. </returns>
        public static PackedUInt16 ToLowUnsignedPart( uint value )
        {
            return new PackedUInt16( ( ushort ) (value & 0xFFFFL) );
        }

        /// <summary>
        /// Combines the unsigned parts into <see cref="T:UInt32">unsigned integer</see>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="highPart"> The high part. </param>
        /// <param name="lowPart">  The low part. </param>
        /// <returns> The given data converted to an UInt32. </returns>
        public static uint ToValue( PackedUInt16 highPart, PackedUInt16 lowPart )
        {
            if ( highPart is null )
                throw new ArgumentNullException( nameof( highPart ) );
            if ( lowPart is null )
                throw new ArgumentNullException( nameof( lowPart ) );
            uint result = ( uint ) (highPart.Value & 0xFFFF);
            result = ( uint ) (lowPart.Value | 0xFFFF0000L & result << 16);
            return result;
        }
    }
}
