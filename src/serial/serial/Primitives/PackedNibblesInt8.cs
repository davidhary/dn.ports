using System;
using System.Collections.Generic;
using System.Linq;

using isr.Ports.Serial.PayloadExtensions;

namespace isr.Ports.Serial
{

    /// <summary>
    /// Packs or unpacks two <see cref="T:Byte">bytes, each of nibble value (0-15), into a single
    /// byte</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public sealed class PackedNibblesInt8
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public PackedNibblesInt8() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="PackedNibblesInt8" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="value"> The value. </param>
        public PackedNibblesInt8( byte value ) : this()
        {
            this.PackedValue = new PackedInt8( value );
        }

        /// <summary> Initializes a new instance of the <see cref="PackedNibblesInt8" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highNibble"> The high nibble. </param>
        /// <param name="lowNibble">  The low nibble. </param>
        public PackedNibblesInt8( byte highNibble, byte lowNibble ) : this()
        {
            this.PackedValue = new PackedInt8( highNibble, lowNibble );
        }

        /// <summary> Initializes a new instance of the <see cref="PackedNibblesInt8" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        public PackedNibblesInt8( IEnumerable<byte> values ) : this()
        {
            this.PackedValue = new PackedInt8( values );
        }

        #endregion

        #region " MEMBERS "

        /// <summary> Gets or sets the packed hexadecimal value. </summary>
        /// <value> The packed hexadecimal value. </value>
        public string PackedHexValue
        {
            get => ToHex( this.PackedValue );

            set => this.PackedValue = new PackedInt8( FromHex( value ) );
        }

        /// <summary> Gets or sets the packed value. </summary>
        /// <value> The packed value. </value>
        public PackedInt8 PackedValue { get; set; }

        #endregion

        #region " PARSERS "

        /// <summary> Converts this object to a hexadecimal. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="highNibble"> The high nibble. </param>
        /// <param name="lowNibble">  The low nibble. </param>
        /// <returns> The given data converted to a String. </returns>
        public static string ToHex( byte highNibble, byte lowNibble )
        {
            return highNibble >= 0x10
                ? throw new ArgumentException( $"value {highNibble} must be lower than {0x10}", nameof( highNibble ) )
                : lowNibble >= 0x10
                ? throw new ArgumentException( $"value {lowNibble} must be lower than {0x10}", nameof( lowNibble ) )
                : $"{highNibble.ToHex()}{lowNibble.ToHex()}";
        }

        /// <summary> Converts this object to a hexadecimal. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> The given data converted to a String. </returns>
        public static string ToHex( PackedInt8 value )
        {
            return value is null ? throw new ArgumentNullException( nameof( value ) ) : $"{value.HighNibble.ToHex()}{value.LowNibble.ToHex()}";
        }

        /// <summary> From hexadecimal. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="highNibble"> The high nibble. </param>
        /// <param name="lowNibble">  The low nibble. </param>
        /// <returns> A Byte. </returns>
        public static byte FromHex( string highNibble, string lowNibble )
        {
            return PackedInt8.ToValue( byte.Parse( highNibble ), byte.Parse( lowNibble ) );
        }

        /// <summary> From hexadecimal. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> A Byte. </returns>
        public static byte FromHex( string value )
        {
            return string.IsNullOrWhiteSpace( value )
                ? throw new ArgumentNullException( nameof( value ) )
                : value.Count() != 2
                ? throw new ArgumentException( $"Invalid value length {value.Count()}; must be 2", nameof( value ) )
                : FromHex( value.Substring( 0, 1 ), value.Substring( 1, 1 ) );
        }
        #endregion

    }
}
