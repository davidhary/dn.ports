using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace isr.Ports.Serial
{

    /// <summary> Defines an event arguments class for <see cref="Port">Sent messages</see>. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class SentDataEventArgs : EventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SentDataEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public SentDataEventArgs() : this( SendDataStatus.None, Array.Empty<byte>() )
        {
        }

        /// <summary>   Initializes a new instance of the <see cref="SentDataEventArgs" /> class. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="sendDataStatus">   The send data status. </param>
        public SentDataEventArgs( SendDataStatus sendDataStatus ) : this( sendDataStatus, Array.Empty<byte>() )
        {
        }

        /// <summary>   Initializes a new instance of the <see cref="SentDataEventArgs" /> class. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="e">    Port event information. </param>
        public SentDataEventArgs( SentDataEventArgs e ) : this( Validated( e ).SendDataStatus, e.DataBuffer )
        {
        }

        /// <summary>   Validated the given e. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="e">    Port event information. </param>
        /// <returns>   The <see cref="SentDataEventArgs"/>. </returns>
        public static SentDataEventArgs Validated( SentDataEventArgs e )
        {
            return e is null ? throw new ArgumentNullException( nameof( e ) ) : e;
        }

        /// <summary>   Initializes a new instance of the <see cref="SentDataEventArgs" /> class. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="sendDataStatus">   The send data status. </param>
        /// <param name="data">             The data. </param>
        public SentDataEventArgs( SendDataStatus sendDataStatus, IEnumerable<byte> data ) : base()
        {
            this.SendDataStatus = sendDataStatus;
            this.DataBuffer = data;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>   Gets or sets the send data status. </summary>
        /// <value> The send data status. </value>
        public SendDataStatus SendDataStatus { get; private set; }

        /// <summary> Gets or sets the sent data buffer. </summary>
        /// <value> A buffer for sent data. </value>
        public IEnumerable<byte> DataBuffer { get; private set; }

        /// <summary>   Gets the data. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="encoding"> The encoding. </param>
        /// <returns>   The string. </returns>
        public string GetData( System.Text.Encoding encoding )
        {
            return encoding is null
                ? throw new ArgumentNullException( nameof( encoding ) )
                : encoding.GetString( this.DataBuffer.ToArray(), 0, this.DataBuffer.Count() );
        }

        #endregion

    }

    /// <summary>   Values that represent Send data status. </summary>
    /// <remarks>   David, 2021-08-05. </remarks>
    public enum SendDataStatus
    {

        /// <summary> An enum constant representing the none option.
        /// Indicates that the message was not yet parsed</summary>
        [Description( "Value not set" )]
        None,

        /// <summary>   An enum constant representing the Sent option. </summary>
        [Description( "Message Sent" )]
        Sent,

        /// <summary>   An enum constant representing the Send failed. </summary>
        [Description( "Send Failed" )]
        SendFailed

    }

}
