using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

using isr.Ports.Serial.PayloadExtensions;

namespace isr.Ports.Serial
{

    /// <summary>
    /// Defines a wrapper around the Visual Studio <see cref="SerialPort">serial port</see>
    /// </summary>
    /// <remarks>
    /// Based on Extended Serial Port Windows Forms Sample
    /// http://code.MSDN.microsoft.com/Extended-SerialPort-10107e37 <para>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class Port : IPort
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Port" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="serialPort"> The <see cref="SerialPort">serial port.</see> </param>
        public Port( SerialPort serialPort ) : base()
        {
            this.SerialPortInternal = new SerialPort();
            this.TimeoutTimer = new System.Timers.Timer();

            // create a new instance of the serial port.
            this.SerialPortInternal = serialPort;

            // initialize the default port parameters.
            this.PortParametersInternal = new PortParametersDictionary( this.SerialPort, 1 ) { PortName = serialPort.PortName };
            this.PortParametersFileName = Port.BuildProductDataPortParametersFileName( serialPort.PortName );
            this._InputBufferingOption = DataBufferingOption.LinearBuffer;
            this.ReceivedBytes = new List<byte>();
            this.SentBytes = new List<byte>();
        }

        /// <summary> initialize a new instance. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public Port() : this( new SerialPort() )
        {
        }

        /// <summary> Gets the is port that owns this item. </summary>
        /// <value> The is port owner. </value>
        public bool IsPortOwner { get; set; }

        /// <summary> Creates a new Port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A Port. </returns>
        public static Port Create()
        {
            Port port = null;
            try
            {
                port = new Port();
            }
            catch
            {
                port?.Dispose();
                throw;
            }

            return port;
        }

        #region " Disposable Support "

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets the is disposed. </summary>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases unmanaged resources and, optionally, releases
        /// the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    if ( this.TimeoutTimer is object )
                    {
                        this.TimeoutTimer.Stop();
                        this.TimeoutTimer.Dispose();
                        this.TimeoutTimer = null;
                    }

                    if ( this.IsPortOwner && this.SerialPortInternal is object )
                        this.SerialPortInternal.Dispose();
                    this.SerialPortInternal = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        ~Port()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " TIME SPAN "

        /// <summary>   Gets or sets the do events action. </summary>
        /// <value> The do events action. </value>
        public static Action DoEventsAction { get; set; }

        /// <summary>   Waits the given delay time. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan Wait( TimeSpan delayTime )
        {
            Stopwatch sw = Stopwatch.StartNew();
            while ( sw.Elapsed < delayTime )
            {
                Thread.SpinWait( 1 );
            }
            return sw.Elapsed;
        }

        /// <summary>   Wait until. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="timeout">      The timeout. </param>
        /// <param name="loopDelay">    The internal loop delay. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   A Tuple. </returns>
        public static (bool Success, TimeSpan Elapsed) WaitUntil( TimeSpan timeout, TimeSpan loopDelay, Func<bool> predicate )
        {
            Stopwatch sw = Stopwatch.StartNew();
            return Port.LetElapseUntil( sw, timeout, loopDelay, predicate );
        }

        /// <summary>   Let elapse until. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="stopwatch">    The stopwatch. </param>
        /// <param name="timeout">      The timeout. </param>
        /// <param name="loopDelay">    The poll interval. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   A Tuple. </returns>
        public static (bool Success, TimeSpan Elapsed) LetElapseUntil( Stopwatch stopwatch, TimeSpan timeout, TimeSpan loopDelay, Func<bool> predicate )
        {
            while ( stopwatch.Elapsed <= timeout && !predicate() )
            {
                if ( loopDelay > TimeSpan.Zero )
                    _ = Port.Wait( loopDelay );
            }
            return (stopwatch.Elapsed < timeout, stopwatch.Elapsed);
        }

        /// <summary>   Wait until. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="loopDelay">        The internal loop delay. </param>
        /// <param name="predicate">        The predicate. </param>
        /// <param name="doEventsAction">   The do events action. </param>
        /// <returns>   A Tuple. </returns>
        public static (bool Success, TimeSpan Elapsed) WaitUntil( TimeSpan timeout, TimeSpan loopDelay, Func<bool> predicate, Action doEventsAction )
        {
            Stopwatch sw = Stopwatch.StartNew();
            return Port.LetElapseUntil( sw, timeout, loopDelay, predicate, doEventsAction );
        }

        /// <summary>   Let elapse until. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="stopwatch">        The stopwatch. </param>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="loopDelay">        The poll interval. </param>
        /// <param name="predicate">        The predicate. </param>
        /// <param name="doEventsAction">   The do events action. </param>
        /// <returns>   A Tuple. </returns>
        public static (bool Success, TimeSpan Elapsed) LetElapseUntil( Stopwatch stopwatch, TimeSpan timeout, TimeSpan loopDelay, Func<bool> predicate, Action doEventsAction )
        {
            while ( stopwatch.Elapsed <= timeout && !predicate() )
            {
                if ( loopDelay > TimeSpan.Zero )
                    _ = Port.Wait( loopDelay );
                doEventsAction?.Invoke();
            }
            return (stopwatch.Elapsed < timeout, stopwatch.Elapsed);
        }

        /// <summary>   Waits the given delay time. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="loopDelay">        The internal loop delay. </param>
        /// <param name="doEventsAction">   The do events action. </param>
        /// <returns>   A TimeSpan. </returns>
        public static (bool Success, TimeSpan Elapsed) Wait( TimeSpan timeout, TimeSpan loopDealy, Action doEventsAction )
        {
            Stopwatch sw = Stopwatch.StartNew();
            return Port.LetElapse( sw, timeout, loopDealy, doEventsAction );
        }

        /// <summary>   Let elapse. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="stopwatch">        The stopwatch. </param>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="loopDelay">        The internal loop delay. </param>
        /// <param name="doEventsAction">   The do events action. </param>
        /// <returns>   A Tuple. </returns>
        public static (bool Success, TimeSpan Elapsed) LetElapse( Stopwatch stopwatch, TimeSpan timeout, TimeSpan loopDelay, Action doEventsAction )
        {
            while ( stopwatch.Elapsed <= timeout )
            {
                if ( loopDelay > TimeSpan.Zero )
                    _ = Port.Wait( loopDelay );
                doEventsAction?.Invoke();
            }
            return (stopwatch.Elapsed < timeout, stopwatch.Elapsed);
        }

        #endregion

        #region " FILE NAME BUILDERS "

        /// <summary>   Builds product data file name. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="assembly"> The assembly. </param>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns>   A string. </returns>
        public static string BuildProductDataFileName( System.Reflection.Assembly assembly, string fileName )
        {
            return System.IO.Path.Combine( BuildProductDataFolderName( assembly ), fileName );
        }

        /// <summary>   Builds product data folder name. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="assembly"> The assembly. </param>
        /// <returns>   A string. </returns>
        public static string BuildProductDataFolderName( System.Reflection.Assembly assembly )
        {
            return System.IO.Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), Port.BuildAssemblyFolderName( assembly ) );
        }

        /// <summary>   Builds assembly folder name. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="assembly"> The assembly. </param>
        /// <returns>   A string. </returns>
        public static string BuildAssemblyFolderName( System.Reflection.Assembly assembly )
        {
            var versionInfo = FileVersionInfo.GetVersionInfo( assembly.Location );
            return System.IO.Path.Combine( versionInfo.CompanyName, versionInfo.ProductName, versionInfo.ProductVersion );
        }

        private const string _DefaultPortName = "COM1";

        /// <summary> Default port name. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> A String. </returns>
        public static string DefaultPortName()
        {
            var portNames = SerialPort.GetPortNames();
            return portNames.Any() && !portNames.Contains( Port._DefaultPortName, StringComparer.OrdinalIgnoreCase ) ? portNames[0] : Port._DefaultPortName;
        }

        /// <summary> Queries if a given port exists. </summary>
        /// <param name="portName"> Name of the port. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool PortExists( string portName )
        {
            return PortExists( SerialPort.GetPortNames(), portName );
        }

        /// <summary> Queries if a given port exists. </summary>
        /// <param name="portNames"> List of names of the ports. </param>
        /// <param name="portName">  Name of the port. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool PortExists( string[] portNames, string portName )
        {
            return portNames is object && portNames.Contains( portName, StringComparer.CurrentCultureIgnoreCase );
        }

        #endregion

        #region " PORT PARAMETERS "

        private PortParametersDictionary _PortParametersInternal;

        /// <summary>   Gets or sets the port parameters internal. </summary>
        /// <value> The port parameters internal. </value>
        private PortParametersDictionary PortParametersInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._PortParametersInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._PortParametersInternal != null )
                {
                    this._PortParametersInternal.PropertyChanged -= this.PortProperties_PropertyChanged;
                }

                this._PortParametersInternal = value;
                if ( this._PortParametersInternal != null )
                {
                    this._PortParametersInternal.PropertyChanged += this.PortProperties_PropertyChanged;
                }
            }
        }

        /// <summary> Gets the port parameters. </summary>
        /// <value> The port properties. </value>
        public PortParametersDictionary PortParameters => this.PortParametersInternal;

        /// <summary> Port properties property changed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PortProperties_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                this.NotifyPropertyChanged( $"{nameof( this.PortParameters )}.{e.PropertyName}" );
            }
            catch ( Exception ex )
            {
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary> Returns the port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portName"> Name of the port. </param>
        /// <returns>
        /// A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see>
        /// </returns>
        public PortParametersDictionary ToPortParameters( string portName )
        {
            var p = PortParametersDictionary.ToPortParameters( this.SerialPort, this.PortParameters.ReceiveDelay );
            p.PortName = portName;
            return p;
        }

        /// <summary> Returns the port parameters. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns>
        /// A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see>
        /// </returns>
        public PortParametersDictionary ToPortParameters()
        {
            return PortParametersDictionary.ToPortParameters( this.SerialPort, this.PortParameters.ReceiveDelay );
        }

        /// <summary> Assign port parameters to the port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
        /// parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see> </param>
        public void FromPortParameters( PortParametersDictionary portParameters )
        {
            if ( this.SerialPortInternal is object && portParameters is object )
            {
                PortParametersDictionary.FromPortParameters( this.SerialPort, portParameters );
            }
        }

        /// <summary>   (Immutable) the port parameters file extension. </summary>
        public const string PortParametersFileExtension = "Port.ini";

        /// <summary>   Builds product data port parameters file name. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="portName"> Name of the port. </param>
        /// <returns>   A string. </returns>
        public static string BuildProductDataPortParametersFileName( string portName )
        {
            return BuildProductDataPortParametersFileName( System.Reflection.Assembly.GetEntryAssembly(), portName );
        }

        /// <summary>   Builds product data port parameters file name. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <param name="assembly"> The assembly. </param>
        /// <param name="portName"> Name of the port. </param>
        /// <returns>   A string. </returns>
        public static string BuildProductDataPortParametersFileName( System.Reflection.Assembly assembly, string portName )
        {
            return System.IO.Path.Combine( BuildProductDataFolderName( assembly ), $"{portName}{Port.PortParametersFileExtension}" );
        }

        private string _PortParametersFileName;

        /// <summary> Gets or sets the filename of the port parameters file. </summary>
        /// <value> The filename of the port parameters file. </value>
        public string PortParametersFileName
        {
            get => this._PortParametersFileName;

            private set {
                if ( !string.Equals( value, this.PortParametersFileName ) )
                {
                    this._PortParametersFileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Attempts to store port parameters from the given data. </summary>
        /// <returns>
        /// a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed.
        /// </returns>
        public (bool Success, string Details) TryStorePortParameters()
        {
            string activity = string.Empty;
            try
            {
                activity = $"storing port parameters to {this.PortParametersFileName}";
                _ = TraceMethods.TraceInformation( activity );
                this.PortParameters.Store( this.PortParametersFileName );
                return (true, string.Empty);
            }
            catch ( System.IO.IOException ex )
            {
                return (false, $"Exception {activity};. {ex}");
            }
        }

        /// <summary>   Attempts to restore port parameters from the given data. </summary>
        /// <returns>
        /// a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed.
        /// </returns>
        public (bool Success, string Details) TryRestorePortParameters()
        {
            string activity = string.Empty;
            try
            {
                activity = $"restoring port parameters from {this.PortParametersFileName}";
                _ = TraceMethods.TraceInformation( activity );
                this.PortParameters.Restore( this.PortParametersFileName );

                activity = $"applying port parameters to port {this.SerialPort.PortName}";
                _ = TraceMethods.TraceInformation( activity );
                PortParametersDictionary.FromPortParameters( this.SerialPort, this.PortParameters );
                return (true, string.Empty);
            }
            catch ( System.IO.IOException ex )
            {
                return (false, $"Exception {activity};. {ex}");
            }
        }

        /// <summary> The supported baud rates. </summary>
        private List<int> _SupportedBaudRates;

        /// <summary> Gets the supported baud rates. </summary>
        /// <value> The supported baud rates. </value>
        public IEnumerable<int> SupportedBaudRates
        {
            get {
                if ( this._SupportedBaudRates is null )
                {
                    this._SupportedBaudRates = new List<int>() { 300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600 };
                }

                return this._SupportedBaudRates;
            }
        }

        #endregion

        #region " CONNECTION MANAGEMENT "

        /// <summary> Gets the is open. </summary>
        /// <value> The is open. </value>
        public bool IsOpen => this.SerialPort is object && this.SerialPort.IsOpen;

        private SerialPort _SerialPortInternal;

        /// <summary>   Gets or sets the serial port internal. </summary>
        /// <value> The serial port. </value>
        private SerialPort SerialPortInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._SerialPortInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._SerialPortInternal != null )
                {

                    this._SerialPortInternal.DataReceived -= this.SerialPort_DataReceived;
                    this._SerialPortInternal.ErrorReceived -= this.SerialPort_ErrorReceived;

                    this._SerialPortInternal.Disposed -= this.SerialPort_Disposed;
                }

                this._SerialPortInternal = value;
                if ( this._SerialPortInternal != null )
                {
                    this._SerialPortInternal.DataReceived += this.SerialPort_DataReceived;
                    this._SerialPortInternal.ErrorReceived += this.SerialPort_ErrorReceived;
                    this._SerialPortInternal.Disposed += this.SerialPort_Disposed;
                }
            }
        }

        /// <summary> Gets or sets the <see cref="SerialPort">Serial Port</see>. </summary>
        /// <value> The serial port. </value>
        public SerialPort SerialPort
        {
            get => this.SerialPortInternal;

            set {
                this.SerialPortInternal = value;
                if ( value is null )
                {
                }
                else
                {
                    this.NotifyConnectionChanged();
                }
            }
        }

        /// <summary>   Opens the port using current port parameters. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <returns>
        /// a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed.
        /// </returns>
        public (bool Success, string Details) TryOpen()
        {
            return this.TryOpen( this.PortParameters );
        }

        /// <summary>   Opens the port using current port parameters. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="portName"> Name of the port. </param>
        /// <returns>
        /// a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed.
        /// </returns>
        public (bool Success, string Details) TryOpen( string portName )
        {
            return string.IsNullOrWhiteSpace( portName )
                ? throw new ArgumentNullException( nameof( portName ) )
                : this.TryOpen( this.ToPortParameters( portName ) );
        }

        /// <summary>   Opens the port using specified port parameters. </summary>
        /// <remarks>   David, 2021-08-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="portParameters">   A <see cref="PortParametersDictionary">collection</see> of
        ///                                 parameters keyed by the
        ///                                 <see cref="PortParameterKey">parameter key</see> </param>
        /// <returns>
        /// a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryOpen( PortParametersDictionary portParameters )
        {
            if ( portParameters is null )
                throw new ArgumentNullException( nameof( portParameters ) );
            string activity = string.Empty;
            bool wasOpen = this.IsOpen;
            try
            {
                activity = $"setting {portParameters.PortName} port parameters";
                _ = TraceMethods.TraceInformation( activity );
                this.FromPortParameters( portParameters );

                // open and check device if is available?
                activity = $"opening {this.SerialPort.PortName}...";
                _ = TraceMethods.TraceInformation( activity );
                this.SerialPort.Open();
                if ( this.IsOpen )
                {
                    this.PortParameters.Populate( this.SerialPort, portParameters.ReceiveDelay );
                    return (true, string.Empty);
                }
                else
                {
                    _ = TraceMethods.TraceWarning( $"failed {activity}; not open" );
                    return (false, $"failed {activity}; not open");
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                return (false, $"Exception {activity};. {ex}");
            }
            finally
            {
                // if port was closed and is now open
                if ( wasOpen != this.IsOpen )
                    this.NotifyConnectionChanged();
            }
        }

        /// <summary> Closes the port. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> True if success; otherwise, false. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryClose()
        {
            string activity = string.Empty;
            bool isOpen = this.IsOpen;
            try
            {
                activity = $"discarding {this.SerialPort.PortName} input buffer";
                _ = TraceMethods.TraceInformation( activity );
                this.SerialPort.DiscardInBuffer();

                activity = $"closing {this.SerialPort.PortName}";
                _ = TraceMethods.TraceInformation( activity );
                this.SerialPort.Close();
                if ( this.IsOpen )
                {
                    _ = TraceMethods.TraceWarning( $"failed {activity}; still open" );
                    return (false, $"failed {activity}; still open");
                }
                else
                {
                    return (true, string.Empty);
                }
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                return (false, $"Exception {activity};. {ex}");
            }
            finally
            {
                if ( isOpen != this.IsOpen )
                    this.NotifyConnectionChanged();
            }
        }

        #endregion

        #region " BUFFERS "

        /// <summary> Gets the received bytes. </summary>
        /// <value> The received bytes. </value>
        private List<byte> ReceivedBytes { get; set; }

        /// <summary> Gets the received values. </summary>
        /// <value> The received values. </value>
        public byte[] ReceivedValues()
        {
            return this.ReceivedBytes is null ? Array.Empty<byte>() : this.ReceivedBytes.ToArray();
        }

        /// <summary> Gets the number of bytes received. </summary>
        /// <value> The number of bytes received. </value>
        public int ReceivedBytesCount => this.ReceivedBytes.Count;

        /// <summary> Discard input buffers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void DiscardInputBuffers()
        {
            this.ReceivedBytes.Clear();
            this.SerialPort?.DiscardInBuffer();
        }

        /// <summary> Attempts to wait for the receive count from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="count">      Number of. </param>
        /// <param name="trialCount"> Number of trials. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryWaitReceiveCount( int count, int trialCount )
        {
            // wait for the data to come in: 
            var minimumTransitTime = this.PortParameters.MinimumTransitTimespan( count );
            // timeout after number of trial of transit time
            var timeout = this.PortParameters.MinimumTransitTimespan( trialCount * count );
            return this.TryWaitReceiveCount( count, minimumTransitTime, timeout );
        }

        /// <summary> Attempts to wait for the receive count from the given data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="count">        Number of. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <param name="timeout">      The timeout. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryWaitReceiveCount( int count, TimeSpan pollInterval, TimeSpan timeout )
        {
            _ = Port.WaitUntil( pollInterval, timeout, () => this.ReceivedBytesCount >= count, Port.DoEventsAction );
            // failed if timeout before reaching the desired count
            return this.ReceivedBytesCount < count;
        }

        /// <summary> Returns the encoded string from the byte values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> A String. </returns>
        public string Decode( IEnumerable<byte> values )
        {
            return Decode( values, this.SerialPort.Encoding );
        }

        /// <summary> Returns the encoded string from the byte values. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values">   The values. </param>
        /// <param name="encoding"> The encoding. </param>
        /// <returns> A String. </returns>
        public static string Decode( IEnumerable<byte> values, System.Text.Encoding encoding )
        {
            return encoding is null
                ? throw new ArgumentNullException( nameof( encoding ) )
                : values is null
                ? throw new ArgumentNullException( nameof( values ) )
                : values.Any() ? encoding.GetString( values.ToArray(), 0, values.Count() ) : string.Empty;
        }

        /// <summary> Gets the sent bytes. </summary>
        /// <value> The sent bytes. </value>
        private List<byte> SentBytes { get; set; }

        /// <summary> Holds the sent values. </summary>
        /// <value> The sent values. </value>
        public byte[] SentValues()
        {
            return this.SentBytes is null ? Array.Empty<byte>() : this.SentBytes.ToArray();
        }

        /// <summary> Discard output buffers. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void DiscardOutputBuffers()
        {
            this.SentBytes.Clear();
            this.SerialPort?.DiscardOutBuffer();
        }

        /// <summary> Resynchronizes the circular buffer or clears the receive buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Resync()
        {
            if ( this.InputBufferingOption == DataBufferingOption.CircularBuffer )
            {
                this.ReceiveBuffer.Resync();
            }
            else
            {
                _ = this.SerialPort.ReadExisting();
            }
        }

        #endregion

        #region " CIRCULAR BUFFER "

        private CircularCollection<byte> _ReceiveBufferInternal;

        /// <summary>   Gets or sets the receive buffer internal. </summary>
        /// <value> The receive buffer internal. </value>
        private CircularCollection<byte> ReceiveBufferInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ReceiveBufferInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ReceiveBufferInternal != null )
                {

                    this._ReceiveBufferInternal.WatermarkNotify -= this.Buffer_WaterMarkNotify;
                }

                this._ReceiveBufferInternal = value;
                if ( this._ReceiveBufferInternal != null )
                {
                    this._ReceiveBufferInternal.WatermarkNotify += this.Buffer_WaterMarkNotify;
                }
            }
        }

        /// <summary> Gets the buffer for receive data. </summary>
        /// <value> A Buffer for receive data. </value>
        public CircularCollection<byte> ReceiveBuffer => this.ReceiveBufferInternal;

        /// <summary> Returns the data count in the circular buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> An Integer. </returns>
        public int DataCount()
        {
            return this.ReceiveBuffer.Count;
        }

        /// <summary> Reads the next byte from the circular buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The next. </returns>
        public byte ReadNext()
        {
            return this.ReceiveBuffer.Dequeue();
        }

        /// <summary> Notify of buffer overflow. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Buffer_WaterMarkNotify( object sender, EventArgs e )
        {
        }

        /// <summary> The input buffering option. </summary>
        private DataBufferingOption _InputBufferingOption;

        /// <summary> Gets or sets the data buffering option. </summary>
        /// <value> The data buffering option. </value>
        public DataBufferingOption InputBufferingOption
        {
            get => this._InputBufferingOption;

            set {
                this._InputBufferingOption = value;
                if ( value == DataBufferingOption.CircularBuffer && (this.ReceiveBufferInternal is null || 0 > this.ReceiveBufferInternal.Capacity) )
                {
                    this.ReceiveBufferInternal = new CircularCollection<byte>( 1024 );
                    this.ReceivedBytes.Clear();
                }
            }
        }

        #endregion

        #region " SEND and RECEIVE "

        /// <summary> The send lock. </summary>
        private object _SendLock;

        /// <summary> Sends data. </summary>
        /// <remarks> Clears the input buffer. </remarks>
        /// <param name="data"> byte array data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void SendData( IEnumerable<byte> data )
        {
            if ( this._SendLock is null )
                this._SendLock = new object();
            string activity = string.Empty;
            SentDataEventArgs sentDataEventArgs = null;
            try
            {
                activity = "setting the send lock";
                lock ( this._SendLock )
                {
                    activity = "clearing input buffers";
                    _ = TraceMethods.TraceInformation( activity );
                    // clear the input buffers in preparation from the returned message.
                    this.DiscardInputBuffers();

                    // clear the output buffers.
                    activity = "clearing output buffers";
                    _ = TraceMethods.TraceInformation( activity );
                    this.DiscardOutputBuffers();
                    if ( data?.Any() == true )
                    {
                        activity = "setting output buffer to new data";
                        _ = TraceMethods.TraceInformation( activity );

                        this.SentBytes.AddRange( data );
                        activity = $"{this.SerialPort.PortName} sending {data.Count()} byes; {data.ToHex( 0, 10 )}... ";
                        _ = TraceMethods.TraceInformation( activity );

                        this.SerialPort.Write( data.ToArray(), 0, data.Count() );
                        activity = $"{this.SerialPort.PortName} sent {data.Count()} byes";
                        _ = TraceMethods.TraceInformation( activity );

                        activity = "setting new send data event arguments";
                        _ = TraceMethods.TraceInformation( activity );
                        sentDataEventArgs = new SentDataEventArgs( SendDataStatus.Sent, data );
                    }
                    else
                    {
                        activity = "attempt at sending data ignored -- nothing to send";
                        _ = TraceMethods.TraceInformation( activity );

                        activity = "setting send data event arguments with empty data";
                        _ = TraceMethods.TraceInformation( activity );
                        sentDataEventArgs = new SentDataEventArgs( SendDataStatus.None, Array.Empty<byte>() );
                    }
                }
            }
            catch ( Exception ex )
            {
                sentDataEventArgs = new SentDataEventArgs( SendDataStatus.SendFailed, data );
                _ = TraceMethods.TraceError( activity, ex );
            }
            finally
            {
                this.TryNotifyDataSent( sentDataEventArgs );
            }
        }

        /// <summary> Sends a hexadecimal data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> The port. </param>
        /// <param name="data"> byte array data. </param>
        public static void SendHexData( IPort port, string data )
        {
            if ( port is null || string.IsNullOrWhiteSpace( data ) )
                return;
            if ( !string.IsNullOrWhiteSpace( data ) )
            {
                var bytes = data.ToHexBytes();
                if ( bytes.ElementAtOrDefault( 0 ) == 255 )
                    throw new InvalidOperationException( $"failed converting '{data}'; '{( char ) bytes.ElementAtOrDefault( 1 )}' is not a valid HEX character" );
                port.SendData( bytes );
            }
        }

        /// <summary> Sends a hexadecimal data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data"> byte array data. </param>
        public void SendHexData( string data )
        {
            SendHexData( this, data );
        }

        /// <summary> Sends an ASCII data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="port"> The port. </param>
        /// <param name="data"> byte array data. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process send ASCII data in this collection.
        /// </returns>
        public static IEnumerable<byte> SendAsciiData( IPort port, string data )
        {
            IEnumerable<byte> bytes = Array.Empty<byte>();
            if ( port is null || string.IsNullOrWhiteSpace( data ) )
                return bytes;
            if ( !string.IsNullOrWhiteSpace( data ) )
            {
                bytes = System.Text.Encoding.ASCII.GetBytes( data );
                port.SendData( bytes );
            }

            return bytes;
        }

        /// <summary> Sends an ASCII data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="data"> byte array data. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process send ASCII data in this collection.
        /// </returns>
        public IEnumerable<byte> SendAsciiData( string data )
        {
            return SendAsciiData( this, data );
        }

        /// <summary> Try receive data. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="serialPort"> The <see cref="SerialPort">serial port.</see> </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryReceiveData( SerialPort serialPort )
        {
            string activity = string.Empty;
            ReceiveDataEventArgs receiveDataEventArgs = null;
            var newValues = Array.Empty<byte>();
            byte[] receivedValues = this.ReceivedValues();
            try
            {
                if ( serialPort.IsOpen )
                {
                    activity = $"awaiting {serialPort.PortName} receive delay {this.PortParameters.ReceiveDelay} ms";
                    _ = TraceMethods.TraceInformation( activity );

                    var actualDelay = Port.Wait( TimeSpan.FromMilliseconds( this.PortParameters.ReceiveDelay ) );
                    activity = $"{serialPort.PortName} receive delay {actualDelay.TotalMilliseconds:0.0} ms";
                    _ = TraceMethods.TraceInformation( activity );

                    int len = serialPort.BytesToRead;
                    if ( len > 0 )
                    {
                        activity = $"reading {len} bytes from {serialPort.PortName}...";
                        _ = TraceMethods.TraceInformation( activity );
                        Array.Resize( ref newValues, len );
                        _ = serialPort.Read( newValues, 0, len );
                        this.ReceivedBytes.AddRange( newValues );
                        receivedValues = this.ReceivedValues();
                        activity = $"received {newValues.Length} bytes {newValues.ToHex( 0, 10 )}... from {serialPort.PortName}";
                        _ = TraceMethods.TraceInformation( activity );

                        if ( this.MessageParser is object && this.MessageParser.ParseEnabled )
                        {
                            // if the parser is defined, use it to parse the message. 
                            var outcome = this.MessageParser.Parse( receivedValues );
                            if ( outcome == MessageParserOutcome.Complete )
                            {
                                receiveDataEventArgs = new ReceiveDataEventArgs( ReceiveDataStatuses.Received | ReceiveDataStatuses.MessageComplete, receivedValues, newValues );
                                activity = $"{serialPort.PortName} message completed";
                                _ = TraceMethods.TraceInformation( activity );
                            }
                            else if ( outcome == MessageParserOutcome.Invalid )
                            {
                                receiveDataEventArgs = new ReceiveDataEventArgs( ReceiveDataStatuses.Received | ReceiveDataStatuses.MessageInvalid, receivedValues, newValues );
                                activity = $"{serialPort.PortName} failed parsing the input message -- message is invalid";
                                _ = TraceMethods.TraceWarning( activity );
                            }
                            else
                            {
                                // if not complete, wait for more characters.
                                activity = $"{serialPort.PortName} message incomplete; awaiting more characters";
                                receiveDataEventArgs = new ReceiveDataEventArgs( ReceiveDataStatuses.Received | ReceiveDataStatuses.MessageIncomplete, receivedValues, newValues );
                                _ = TraceMethods.TraceInformation( activity );
                            }
                        }
                        else
                        {
                            activity = $"{serialPort.PortName} message received";
                            _ = TraceMethods.TraceInformation( activity );
                            receiveDataEventArgs = new ReceiveDataEventArgs( ReceiveDataStatuses.Received, receivedValues, newValues );
                        }
                    }
                }
                else
                {
                    activity = $"{serialPort.PortName} data received event aborted; port no longer open";
                    _ = TraceMethods.TraceWarning( activity );
                    receiveDataEventArgs = new ReceiveDataEventArgs( ReceiveDataStatuses.PortClosed, receivedValues, newValues );
                }
            }
            catch ( Exception ex )
            {
                receiveDataEventArgs = new ReceiveDataEventArgs( ReceiveDataStatuses.ReceiveFailed, receivedValues, newValues );
                _ = TraceMethods.TraceError( activity, ex );
            }
            finally
            {
                if ( receiveDataEventArgs is object )
                    this.TryNotifyDataReceived( receiveDataEventArgs );
            }
        }

        /// <summary> Handles the DataReceived event of the _serialPort control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.IO.Ports.SerialDataReceivedEventArgs" /> instance
        /// containing the event data. </param>
        private void SerialPort_DataReceived( object sender, SerialDataReceivedEventArgs e )
        {
            if ( sender is SerialPort port && e is object )
            {
                _ = TraceMethods.TraceInformation( $"{e.EventType} received" );
                this.TryReceiveData( port );
            }
        }

        #endregion

        #region " MESSAGE PARSER "

        /// <summary> Gets or sets the message parser. </summary>
        /// <value> The message parser. </value>
        public IMessageParser MessageParser { get; set; }

        #endregion

        #region " EVENT: SERIAL PORT ERROR RECEIVED "

        /// <summary> Event queue for all listeners interested in
        /// <see cref="SerialPortErrorReceived">Serial port error receivd </see>> events.
        /// Serial Port Error Received status is reported with the
        /// <see cref="SerialErrorReceivedEventArgs">Serial Port Error Received event arguments.</see>
        /// </summary>
        public event EventHandler<SerialErrorReceivedEventArgs> SerialPortErrorReceived;

        /// <summary>   Raises the serial error received event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnSerialPortErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            this.SerialPortErrorReceived?.Invoke( sender, e );
        }

        /// <summary> Raises the serial port error received event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void NotifySerialPortErrorReceived( SerialErrorReceivedEventArgs e )
        {
            this.OnSerialPortErrorReceived( this, e );
        }

        /// <summary> Handles the Error Received event of the _serialPort control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="SerialErrorReceivedEventArgs" /> instance containing the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SerialPort_ErrorReceived( object sender, SerialErrorReceivedEventArgs e )
        {
            if ( sender is SerialPort port )
            {
                string activity = $"notifying {port.PortName} {nameof( this.SerialPort.ErrorReceived )} event";
                try
                {
                    _ = TraceMethods.TraceInformation( activity );
                    this.NotifySerialPortErrorReceived( e );
                }
                catch ( Exception ex )
                {
                    _ = TraceMethods.TraceError( activity, ex );
                    this.OnEventHandlerError( ex );
                }
            }
        }

        #endregion

        #region " EVENT: SERIAL PORT DISPOSED "

        /// <summary>
        /// Event queue for all listeners interested in the <see cref="SerialPortDisposed">Serial Port
        /// Disposed</see> events.
        /// </summary>
        public event EventHandler<EventArgs> SerialPortDisposed;

        /// <summary>   Raises the serial port disposed event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnSerialPortDisposed( object sender, EventArgs e )
        {
            this.SerialPortDisposed?.Invoke( sender, e );
        }

        /// <summary>
        /// Notifies the <see cref="SerialPortDisposed">Serial Port Disposed</see> event.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifySerialPortDisposed( EventArgs e )
        {
            this.OnSerialPortDisposed( this, e );
        }

        /// <summary> Serial port disposed. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void SerialPort_Disposed( object sender, EventArgs e )
        {
            string activity = "notifying serial port disposed";
            try
            {
                _ = TraceMethods.TraceInformation( activity );
                this.NotifySerialPortDisposed( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        #endregion

        #region " EVENT: CONNECTION CHANGED "

        /// <summary>
        /// Event queue for all listeners interested in Connection Changed events. Connection status is
        /// reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
        /// </summary>
        public event EventHandler<ConnectionEventArgs> ConnectionChanged;

        /// <summary>   Raises the connection changed event. </summary>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnConnectionChanged( object sender, ConnectionEventArgs e )
        {
            this.PortParametersFileName = Port.BuildProductDataPortParametersFileName( this.PortParameters.PortName );
            ConnectionChanged?.Invoke( sender, e );
        }

        /// <summary>   Raises the connection changed event. </summary>
        private void NotifyConnectionChanged()
        {
            this.OnConnectionChanged( this, new ConnectionEventArgs( this.IsOpen ) );
        }

        #endregion

        #region " EVENT: DATA RECEIVED "

        /// <summary>
        /// Event queue for all listeners interested in <see cref="DataReceived">data received</see>/&gt;
        /// events. Receipt status is reported along with the received data in the receive buffer using
        /// the <see cref="ReceiveDataEventArgs">receive data event arguments.</see>
        /// </summary>
        public event EventHandler<ReceiveDataEventArgs> DataReceived;

        /// <summary>   Raises the <see cref="DataReceived"/> event. </summary>
        /// <remarks>   David, 2021-08-03. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDataReceived( object sender, ReceiveDataEventArgs e )
        {
            this.TimeoutTimer?.Stop();
            this.DataReceived?.Invoke( sender, e );
        }

        /// <summary>   Notifies a <see cref="DataReceived"/> event. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        private void NotifyDataReceived( ReceiveDataEventArgs e )
        {
            this.OnDataReceived( this, e );
        }

        /// <summary> Try notify a <see cref="DataReceived"/> event. </summary>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyDataReceived( ReceiveDataEventArgs e )
        {
            string activity = "notifying data received";
            try
            {
                this.NotifyDataReceived( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        #endregion

        #region " EVENT: DATA SENT "

        /// <summary> Event queue for all listeners interested in <see cref="DataSent">Data Sent events</see>.
        /// Sent status is reported along with the Sent data in the buffer
        /// using the <see cref="SentDataEventArgs">send data event arguments.</see> </summary>
        public event EventHandler<SentDataEventArgs> DataSent;

        /// <summary>   Raises the <see cref="DataSent"/> event. </summary>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDataSent( object sender, SentDataEventArgs e )
        {
            if ( (e.SendDataStatus == SendDataStatus.Sent) && this.SerialPort.ReadTimeout > 0 )
            {
                // notify only once
                this.TimeoutTimer.AutoReset = true;
                this.TimeoutTimer.Interval = this.SerialPort.ReadTimeout;
                this.TimeoutTimer.Start();
            }
            _ = TraceMethods.TraceInformation( $"notifying {this.SerialPort?.PortName} data sent" );
            this.DataSent?.Invoke( sender, e );
        }

        /// <summary>   Notifies a <see cref="DataSent"/> event. </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        private void NotifyDataSent( SentDataEventArgs e )
        {
            this.OnDataSent( this, e );
        }

        /// <summary> Try notify a <see cref="DataSent"/> event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyDataSent( SentDataEventArgs e )
        {
            string activity = "notifying data sent";
            try
            {
                this.NotifyDataSent( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        #endregion

        #region " EVENT: TIMEOUT "

        /// <summary>   Event queue for all listeners interested in <see cref="Timeout"/> events. </summary>
        public event EventHandler<EventArgs> Timeout;

        /// <summary>   Raises the <see cref="Timeout"/> event. </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="sender">   The source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnTimeout( object sender, EventArgs e )
        {
            this.Timeout?.Invoke( sender, e );
        }

        /// <summary> Notifies a <see cref="Timeout"/> event. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="e"> Event information. </param>
        private void NotifyTimeout( EventArgs e )
        {
            this.OnTimeout( this, e );
        }

        /// <summary> Try notify a <see cref="Timeout"/> event. </summary>
        /// <param name="e"> Port event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryNotifyTimeout( EventArgs e )
        {
            string activity = $"notifying {this.SerialPort.PortName} timeout";
            try
            {
                _ = TraceMethods.TraceInformation( activity );
                this.NotifyTimeout( e );
            }
            catch ( Exception ex )
            {
                _ = TraceMethods.TraceError( activity, ex );
                this.OnEventHandlerError( ex );
            }
        }

        private System.Timers.Timer _TimeoutTimer;

        /// <summary>   Gets or sets the timeout timer. </summary>
        /// <value> The timeout timer. </value>
        private System.Timers.Timer TimeoutTimer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._TimeoutTimer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._TimeoutTimer is object )
                {
                    this._TimeoutTimer.Elapsed -= this.TimeoutTimer_Elapsed;
                }

                this._TimeoutTimer = value;
                if ( this._TimeoutTimer is object )
                {
                    this._TimeoutTimer.Elapsed += this.TimeoutTimer_Elapsed;
                }
            }
        }

        /// <summary> Handles the Elapsed event of the _timeoutTimer control. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.Timers.ElapsedEventArgs" /> instance containing the
        /// event data. </param>
        private void TimeoutTimer_Elapsed( object sender, System.Timers.ElapsedEventArgs e )
        {
            this.TryNotifyTimeout( EventArgs.Empty );
        }

        #endregion

        #region " EVENT: EVENT HANDLER ERROR "

        /// <summary>   Event queue for all listeners interested in <see cref="ExceptionEventHandler"/> events. </summary>
        public event System.Threading.ThreadExceptionEventHandler ExceptionEventHandler;

        /// <summary>
        /// Raises the  <see cref="System.Threading.ThreadExceptionEventHandler"/> event.
        /// </summary>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnEventHandlerError( System.Threading.ThreadExceptionEventArgs e )
        {
            this.ExceptionEventHandler?.Invoke( this, e );
        }

        /// <summary>
        /// Raises the  <see cref="System.Threading.ThreadExceptionEventHandler"/> event.
        /// </summary>
        /// <param name="exception">    The exception. </param>
        protected virtual void OnEventHandlerError( System.Exception exception )
        {
            this.OnEventHandlerError( new System.Threading.ThreadExceptionEventArgs( exception ) );
        }

        #endregion

        #region " EVENT: PROPERTY CHANGED "

        /// <summary> Event queue for all listeners interested in property changed events. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Raises the property changed event . </summary>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            PropertyChanged?.Invoke( sender, e );
        }

        /// <summary>   Notifies a property changed. </summary>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

    }
}
