using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace isr.Ports.Serial
{

    /// <summary> Defines an event arguments class for <see cref="Port">received messages</see>. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class ReceiveDataEventArgs : EventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ReceiveDataEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ReceiveDataEventArgs() : this( ReceiveDataStatuses.None, Array.Empty<byte>(), Array.Empty<byte>() )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiveDataEventArgs" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="receiveDataStatus">    The receive data status. </param>
        public ReceiveDataEventArgs( ReceiveDataStatuses receiveDataStatus ) : this( receiveDataStatus, Array.Empty<byte>(), Array.Empty<byte>() )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiveDataEventArgs" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="e">    Port event information. </param>
        public ReceiveDataEventArgs( ReceiveDataEventArgs e ) : this( Validated( e ).ReceiveDataStatus, e.DataBuffer, e.NewData )
        {
        }

        /// <summary>   Validated the given e. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="e">    Port event information. </param>
        /// <returns>   The ReceiveDataEventArgs. </returns>
        public static ReceiveDataEventArgs Validated( ReceiveDataEventArgs e )
        {
            return e is null ? throw new ArgumentNullException( nameof( e ) ) : e;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiveDataEventArgs" /> class.
        /// </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <param name="receiveDataStatus">    The receive data status. </param>
        /// <param name="data">                 The data. </param>
        /// <param name="newData">              The new data. </param>
        public ReceiveDataEventArgs( ReceiveDataStatuses receiveDataStatus, IEnumerable<byte> data, IEnumerable<byte> newData ) : base()
        {
            this.ReceiveDataStatus = receiveDataStatus;
            this.DataBuffer = data;
            this.NewData = newData;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>   Gets or sets the receive data status. </summary>
        /// <value> The receive data status. </value>
        public ReceiveDataStatuses ReceiveDataStatus { get; private set; }

        /// <summary> Gets or sets the data buffer. </summary>
        /// <value> A buffer for data. </value>
        public IEnumerable<byte> DataBuffer { get; private set; }

        /// <summary>   Gets or sets the new data. </summary>
        /// <value> The new data. </value>
        public IEnumerable<byte> NewData { get; private set; }

        /// <summary>   Gets the data. </summary>
        /// <remarks>   David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="encoding"> The encoding. </param>
        /// <returns>   The string. </returns>
        public string GetData( System.Text.Encoding encoding )
        {
            return encoding is null
                ? throw new ArgumentNullException( nameof( encoding ) )
                : encoding.GetString( this.DataBuffer.ToArray(), 0, this.DataBuffer.Count() );
        }

        /// <summary>   Gets new data. </summary>
        /// <remarks>   David, 2021-08-05. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="encoding"> The encoding. </param>
        /// <returns>   The new data. </returns>
        public string GetNewData( System.Text.Encoding encoding )
        {
            return encoding is null
                ? throw new ArgumentNullException( nameof( encoding ) )
                : encoding.GetString( this.NewData.ToArray(), 0, this.DataBuffer.Count() );
        }

        #endregion

    }

    /// <summary>   Values that represent receive data status. </summary>
    /// <remarks>   David, 2021-08-05. </remarks>
    [Flags]
    public enum ReceiveDataStatuses
    {

        /// <summary> An enum constant representing the none option.
        /// Indicates that the message was not yet parsed</summary>
        [Description( "Value not set" )]
        None,

        /// <summary>   An enum constant representing the received option. </summary>
        [Description( "Message received" )]
        Received = 1,

        /// <summary> An enum constant representing the message complete option.
        /// Indicates that the message is complete</summary>
        [Description( "Message Complete" )]
        MessageComplete = 2,

        /// <summary> An enum constant representing the invalid option.
        /// Indicates that the message is invalid. This means that the provided buffer
        /// has invalid structure and needs to be cleared.</summary>
        [Description( "Message Invalid" )]
        MessageInvalid = 4,

        /// <summary> An enum constant representing the incomplete option.
        /// Indicates that the message is incomplete.
        /// This means that the message parser expects additional information.
        /// </summary>
        [Description( "Message incomplete" )]
        MessageIncomplete = 8,

        /// <summary>   An enum constant representing the port closed option. </summary>
        [Description( "Port Closed" )]
        PortClosed = 16,

        /// <summary>   An enum constant representing the receive failed. </summary>
        [Description( "Receive Failed" )]
        ReceiveFailed = 32

    }

}
