using System;

namespace isr.Ports.Serial
{

    /// <summary>
    /// Defines an event arguments class for <see cref="Port">port connection messages</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class ConnectionEventArgs : EventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ConnectionEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public ConnectionEventArgs() : this( false )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionEventArgs" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="isPortOpen"> if set to <c>True</c> [is connected]. </param>
        public ConnectionEventArgs( bool isPortOpen ) : base()
        {
            this.IsPortOpen = isPortOpen;
        }

        #endregion

        /// <summary>
        /// Gets or sets a value indicating whether the port is open, true; Otherwise, False.
        /// </summary>
        /// <value> The is port open. </value>
        public bool IsPortOpen { get; private set; }
    }
}
