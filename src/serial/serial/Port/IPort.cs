using System;
using System.Collections.Generic;

namespace isr.Ports.Serial
{

    /// <summary> Defines the interface for the <see cref="Port">serial port</see> </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public interface IPort : IDisposable, System.ComponentModel.INotifyPropertyChanged
    {

        #region " PORT PARAMETERS "

        /// <summary> Gets or sets the port parameters. </summary>
        /// <value> The port properties. </value>
        PortParametersDictionary PortParameters { get; }

        /// <summary> Returns the port parameters. </summary>
        /// <returns>
        /// A <see cref="PortParametersDictionary">collection</see> of parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see>
        /// Includes, in addition to the standard parameters, the Threshold count, Delay time in
        /// milliseconds and timeout in milliseconds.
        /// </returns>
        PortParametersDictionary ToPortParameters();

        /// <summary> Assign port parameters to the port. </summary>
        /// <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
        /// parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see> </param>
        void FromPortParameters( PortParametersDictionary portParameters );

        /// <summary> Gets or sets the filename of the port parameters file. </summary>
        /// <value> The filename of the port parameters file. </value>
        string PortParametersFileName { get; }

        /// <summary> Attempts to store port parameters from the given data. </summary>
        /// <param name="e"> Action event information. </param>
        /// <returns> a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed. </returns>
        (bool Success, string Details) TryStorePortParameters();

        /// <summary> Attempts to restore port parameters from the given data. </summary>
        /// <returns> a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed. </returns>
        (bool Success, string Details) TryRestorePortParameters();

        /// <summary> Gets or sets the supported baud rates. </summary>
        /// <value> The supported baud rates. </value>
        IEnumerable<int> SupportedBaudRates { get; }

        #endregion

        #region " PORT OPEN STATUS "

        /// <summary> Gets or sets the is open. </summary>
        /// <value> The is open. </value>
        bool IsOpen { get; }

        #endregion

        #region " CONNECTION MANAGEMENT "

        /// <summary> Gets or sets the <see cref="System.IO.Ports.SerialPort">Serial Port</see>. </summary>
        /// <value> The serial port. </value>
        System.IO.Ports.SerialPort SerialPort { get; set; }

        /// <summary> Opens the port using specified port parameters. </summary>
        /// <param name="portParameters"> A <see cref="PortParametersDictionary">collection</see> of
        /// parameters keyed by the
        /// <see cref="PortParameterKey">parameter key</see> </param>
        /// <param name="e">              Action event information. </param>
        /// <returns> a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed. </returns>
        (bool Success, string Details) TryOpen( PortParametersDictionary portParameters );

        /// <summary> Opens the port using current port parameters. </summary>
        /// <param name="portName"> Name of the port. </param>
        /// <param name="e">        Action event information. </param>
        /// <returns> a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed. </returns>
        (bool Success, string Details) TryOpen( string portName );

        /// <summary> Opens the port using current port parameters. </summary>
        /// <param name="e"> Action event information. </param>
        /// <returns> a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed. </returns>
        (bool Success, string Details) TryOpen();

        /// <summary> Closes the port. </summary>
        /// <param name="e"> Action event information. </param>
        /// <returns> a Tuple{bool, string}; <c>true</c> if it succeeds; otherwise <c>false</c>; Details if failed. </returns>
        (bool Success, string Details) TryClose();

        #endregion

        #region " DATA MANAGEMENT "

        /// <summary> Returns the data count in the circular buffer. </summary>
        /// <returns> An Integer. </returns>
        int DataCount();

        /// <summary> Reads the next byte from the circular buffer. </summary>
        /// <returns> The next. </returns>
        byte ReadNext();

        /// <summary> Resynchronizes the circular buffer. </summary>
        void Resync();

        /// <summary> Sends data. </summary>
        /// <param name="data"> byte array data. </param>
        void SendData( IEnumerable<byte> data );

        /// <summary> Gets or sets the input (receive) data buffering option. </summary>
        /// <value> The data buffering option. </value>
        DataBufferingOption InputBufferingOption { get; set; }

        /// <summary> Gets the received values. </summary>
        /// <value> The received values. </value>
        Byte[] ReceivedValues();

        /// <summary> Gets the sent values. </summary>
        /// <value> The sent values. </value>
        Byte[] SentValues();

        /// <summary> Attempts to wait for the receive count from the given data. </summary>
        /// <param name="count">      Number of. </param>
        /// <param name="trialCount"> Number of trials. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        bool TryWaitReceiveCount( int count, int trialCount );

        /// <summary> Attempts to wait for the receive count from the given data. </summary>
        /// <param name="count">        Number of. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <param name="timeout">      The timeout. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        bool TryWaitReceiveCount( int count, TimeSpan pollInterval, TimeSpan timeout );

        #endregion

        #region " MESSAGE PARSER "

        /// <summary> Gets or sets the message parser. </summary>
        /// <value> The message parser. </value>
        IMessageParser MessageParser { get; set; }

        #endregion

        #region " EVENT MANAGEMENT "

        /// <summary>
        /// Event queue for all listeners interested in ConnectionChanged events. Connection status is
        /// reported with the <see cref="ConnectionEventArgs">connection event arguments.</see>
        /// </summary>
        event EventHandler<ConnectionEventArgs> ConnectionChanged;

        /// <summary>
        /// Event queue for all listeners interested in DataReceived events. Reception status is report
        /// along with the received data in the receive buffer using the
        /// <see cref="ReceiveDataEventArgs"> receive data event arguments.</see>
        /// </summary>
        event EventHandler<ReceiveDataEventArgs> DataReceived;

        /// <summary>
        /// Event queue for all listeners interested in DataSent events. Status is reported along with
        /// the Sent data in the buffer using the <see cref="SentDataEventArgs">send data event
        /// arguments.</see>
        /// </summary>
        event EventHandler<SentDataEventArgs> DataSent;

        /// <summary>
        /// Event queue for all listeners interested in SerialPortErrorReceived events.
        /// </summary>
        event EventHandler<System.IO.Ports.SerialErrorReceivedEventArgs> SerialPortErrorReceived;

        /// <summary> Event queue for all listeners interested in SerialPortDisposed events. </summary>
        event EventHandler<EventArgs> SerialPortDisposed;

        /// <summary>   Event queue for all listeners interested in Timeout events. </summary>
        event EventHandler<EventArgs> Timeout;

        /// <summary>   Event queue for all listeners interested in ExceptionEventHandler events. </summary>
        event System.Threading.ThreadExceptionEventHandler ExceptionEventHandler;

        #endregion

    }
}
