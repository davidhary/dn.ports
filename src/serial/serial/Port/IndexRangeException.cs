using System;
using System.Runtime.Serialization;

namespace isr.Ports.Serial
{

    /// <summary> Reports index exception for the Circular buffer. </summary>
    public class IndexRangeException : Exception
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public IndexRangeException() : this( "Index exception occurred" )
        {
        }

        /// <summary>
        /// Constructs the class specifying a <paramref name="message">message</paramref>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="message"> Specifies the exception message. </param>
        public IndexRangeException( string message ) : base( message )
        {
        }

        /// <summary>
        /// Constructs the class specifying a <paramref name="message">message</paramref>
        /// and <paramref name="innerException"/>.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="message">        Specifies the exception message. </param>
        /// <param name="innerException"> Specifies the InnerException. </param>
        public IndexRangeException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>
        /// Constructs the class using serialization <paramref name="info"/> and
        /// <paramref name="context"/>
        /// information.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="info">    Specifies <see cref="SerializationInfo">serialization
        /// information</see>. </param>
        /// <param name="context"> Specifies <see cref="StreamingContext">streaming context</see> for
        /// the exception. </param>
        protected IndexRangeException( SerializationInfo info, StreamingContext context ) : base( info, context )
        {
        }
    }
}
