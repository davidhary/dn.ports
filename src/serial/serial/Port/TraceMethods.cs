using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Ports.Serial
{
    /// <summary>   A trace methods. </summary>
    /// <remarks>   David, 2021-08-04. </remarks>
    public static class TraceMethods
    {

        /// <summary>   Converts an eventType to a trace level text. </summary>
        /// <remarks>   David, 2021-08-04. </remarks>
        /// <param name="eventType">    Type of the event. </param>
        /// <returns>   EventType as a string. </returns>
        private static string ToTraceLevelText( System.Diagnostics.TraceEventType eventType )
        {
            return 0 != (eventType & TraceEventType.Critical)
                ? "[CRT]"
                : 0 != (eventType & TraceEventType.Error)
                ? "[ERR]"
                : 0 != (eventType & TraceEventType.Warning)
                ? "[WRN]"
                : 0 != (eventType & TraceEventType.Information)
                ? "[INT]"
                : 0 != (eventType & TraceEventType.Verbose)
                ? "[VRB]"
                : "[INT]";
        }

        /// <summary>   Builds a message. </summary>
        /// <remarks>   David, 2021-08-04. </remarks>
        /// <param name="eventType">        Type of the event. </param>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       Name of the member. </param>
        /// <param name="sourcePath">       Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> Source line number. </param>
        /// <returns>   A string. </returns>
        private static string BuildMessage( System.Diagnostics.TraceEventType eventType, string message, string memberName, string sourcePath, int sourceLineNumber )
        {
            return $"t::{DateTime.Now:HH:mm:ss.fff zzz}, {ToTraceLevelText( eventType )}, [{sourcePath}].{memberName}.Line#{sourceLineNumber}, {message}";
        }

        /// <summary>   Trace message. </summary>
        /// <remarks>   David, 2021-08-04. </remarks>
        /// <param name="eventType">        Type of the event. </param>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       Name of the member. </param>
        /// <param name="sourcePath">       Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> Source line number. </param>
        /// <returns>   A string. </returns>
        public static string TraceMessage( System.Diagnostics.TraceEventType eventType, string message, string memberName, string sourcePath, int sourceLineNumber )
        {
            string output = BuildMessage( eventType, message, memberName, sourcePath, sourceLineNumber );
            if ( 0 != (eventType & TraceEventType.Critical) )
            {
                System.Diagnostics.Trace.TraceError( output );
            }
            else if ( 0 != (eventType & TraceEventType.Error) )
            {
                System.Diagnostics.Trace.TraceError( output );
            }
            else if ( 0 != (eventType & TraceEventType.Warning) )
            {
                System.Diagnostics.Trace.TraceWarning( output );
            }
            else
            {
                System.Diagnostics.Trace.TraceInformation( output );
            }
            return output;
        }


        /// <summary>   Trace information. </summary>
        /// <remarks>   David, 2021-08-04. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   A string. </returns>
        public static string TraceInformation( string message, [CallerMemberName] string memberName = "",
                                                               [CallerFilePath] string sourcePath = "",
                                                               [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceMessage( TraceEventType.Information, message, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>   Trace error. </summary>
        /// <remarks>   David, 2021-08-04. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   A string. </returns>
        public static string TraceError( string message, [CallerMemberName] string memberName = "",
                                                         [CallerFilePath] string sourcePath = "",
                                                         [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceMessage( TraceEventType.Error, message, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>   Trace error. </summary>
        /// <remarks>   David, 2021-08-04. </remarks>
        /// <param name="activity">         The activity. </param>
        /// <param name="ex">               The exception. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   A string. </returns>
        public static string TraceError( string activity, Exception ex,
                                                         [CallerMemberName] string memberName = "",
                                                         [CallerFilePath] string sourcePath = "",
                                                         [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceMessage( TraceEventType.Error, $"Exception {activity}; {ex}", memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>   Trace error. </summary>
        /// <remarks>   David, 2021-08-05. </remarks>
        /// <param name="ex">               The exception. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   A string. </returns>
        public static string TraceError( Exception ex, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceMessage( TraceEventType.Error, ex.ToString(), memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>   Trace warning. </summary>
        /// <remarks>   David, 2021-08-04. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   A string. </returns>
        public static string TraceWarning( string message, [CallerMemberName] string memberName = "",
                                                           [CallerFilePath] string sourcePath = "",
                                                           [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceMessage( TraceEventType.Warning, message, memberName, sourcePath, sourceLineNumber );
        }

    }
}
